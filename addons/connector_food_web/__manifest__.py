# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Api Food Web',
    'version': '15.0.0',
    'category': 'Sales',
    'summary': 'PDT',
    "description": """
       Pham Duy Tuong
    """,
    'author': 'PDT',
    'depends': ['base', 'sale', 'website_sale', 'web', 'product'],
    'data': [
        'views/slide_buy_now.xml',
        'views/slide_deal.xml',
        'views/blog_deal_hot.xml',
        'views/res_partner.xml',
        'views/feed_back.xml',
        'views/shipper_feedback_tag.xml',
        'views/order_feedback_tag.xml',
        'views/order_feedback_image.xml',
        'views/order_feedback_videos.xml',
        'views/branch.xml',
        'views/res_setting.xml',
        'views/like_product.xml',
        'views/product_template.xml',
        'views/sale_order.xml',
        'views/cooking_recipe.xml',
        "security/ir.model.access.csv",
    ],
    'demo': [],
    # 'assets': {
    #     'web.assets_backend': [
    #     ],
    #     'web.assets_qweb': [
    #
    #     ],
    # },
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
