import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = 'res.partner'

    sex = fields.Selection([
        ('male', 'Nam'),
        ('female', 'Nữ'),
        ('is_different', 'Khác'),
    ], string="Giới tính", default='male')
    birthday = fields.Date('Ngày sinh nhật')
    coin_in_ids = fields. Many2many('feed.back')
    coin_out_so_ids = fields.Many2many('sale.order')
    total_coin = fields.Float(compute='_compute_total_coin', store=False)

    @api.depends('coin_in_ids', 'coin_out_so_ids')
    def _compute_total_coin(self):
        for line in self:
            if line.coin_in_ids or line.coin_out_so_ids:
                coin_in = sum(line.coin_in_ids.mapped('coin_in'))
                coin_out = sum(line.coin_out_so_ids.mapped('coin_out'))
                line.total_coin = coin_in - coin_out
            else:
                line.total_coin = 0

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    date_out = fields.Datetime('Date Out')
    coin_out = fields.Float('Coin Out')