import time

from attr import fields

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class CookingRecipe(models.Model):
    _name = 'cooking.recipe'

    name = fields.Char('Tên bài viết')
    image = fields.Binary('Hình ảnh')
    date_content = fields.Datetime('Ngày đăng bài')
    view_content = fields.Float('Lượt xem', compute='_compute_view_user', store=False)
    content_1 = fields.Html('Nội dung bài viết 1')
    content_2 = fields.Html('Nội dung bài viết 2')
    heart = fields.Float('Lượt tim', compute='_compute_record_heart', store=False)
    comment = fields.Float('Lượt bình luận', compute='_compute_record_comment', store=False)
    share = fields.Float('Lượt chia sẻ')
    user_comment_ids = fields.One2many('user.comment.cook.recipe', 'cook_recipe_id')
    user_heart_ids = fields.One2many('user.heart.cook.recipe', 'cook_id')
    viewer_ids = fields.One2many('user.view.cook', 'view_cook_id')

    @api.depends('user_comment_ids')
    def _compute_record_comment(self):
        for line in self:
            if line.user_comment_ids:
                line.comment = len(line.user_comment_ids)
            else:
                line.comment = 0

    @api.depends('user_heart_ids')
    def _compute_record_heart(self):
        for line in self:
            if line.user_heart_ids:
                line.heart = len(line.user_heart_ids)
            else:
                line.heart = 0

    @api.depends('viewer_ids')
    def _compute_view_user(self):
        for line in self:
            if line.viewer_ids:
                line.view_content = len(line.viewer_ids)
            else:
                line.view_content = 0


class UserCommentCookRecipe(models.Model):
    _name = 'user.comment.cook.recipe'

    cook_recipe_id = fields.Many2one('cooking.recipe')
    user_id = fields.Many2one('res.users', 'Người dùng')
    date_comment = fields.Datetime('Ngày bình luận')
    content_1 = fields.Char('Nội dung bài viết 1')
    heart = fields.Float('Lượt tim')
    comment = fields.Float('Lượt bình luận')

class UserListHeartCookRecipe(models.Model):
    _name = 'user.heart.cook.recipe'

    cook_id = fields.Many2one('cooking.recipe')
    user_id = fields.Many2one('res.users', 'Người dùng')
    heart = fields.Boolean()

class UserViewCook(models.Model):
    _name = 'user.view.cook'

    view_cook_id = fields.Many2one('cooking.recipe')

