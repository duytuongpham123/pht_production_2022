from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    distance = fields.Float('Khoảng cách')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            distance=self.env['ir.config_parameter'].sudo().get_param(
                'connector_food_web.distance')
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()
        distance = self.distance
        param.set_param('connector_food_web.distance', distance)








