from odoo import api, fields, models, _
from odoo.http import request
from collections import Counter
import logging
logger = logging.getLogger(__name__)
from odoo.tools.float_utils import float_round

class Http(models.AbstractModel):
    _inherit = 'ir.http'

    def session_info(self):
        res = super(Http, self).session_info()
        return res

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    date_delivery = fields.Datetime('Thời gian giao hàng')
    note_sale = fields.Text('Lời nhắn đến quán')
    branch_id = fields.Many2one('branch.partner', 'Điểm bán')
    name_partner_address = fields.Char('Tên liên hệ')
    street = fields.Char('Địa chỉ')
    street2 = fields.Char('Cụ thể')
    phone = fields.Char('Số điện thoại')
    type_payment = fields.Many2one('payment.type', 'Loại thanh toán')
    check_customer_confirm = fields.Boolean('Khách đã xác nhận?', default=False)
    flag_feedback = fields.Boolean("Khách đã đánh giá", default=False)
    date_cancel = fields.Datetime('Thời gian hủy hàng')

class PaymentType(models.Model):
    _name = 'payment.type'

    name = fields.Char('Tên')

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    flag_like = fields.Boolean(default=False)
    feedback_ids = fields.One2many('product.feed.back', 'product_tmpl_id')
    average_start = fields.Float('Lượt sao', compute="_compute_data_feedback", store=False)
    total_feedback = fields.Float('Tổng lượt đánh giá', compute="_compute_data_feedback", store=False)
    sales_count_1 = fields.Float(related="sales_count", store=True)

    @api.depends('feedback_ids')
    def _compute_data_feedback(self):
        for line in self:
            if line.feedback_ids:
                order_evaluation = [int(tok.order_evaluation.split("_")[1]) for tok in line.feedback_ids]
                line.average_start = sum(order_evaluation) / len([tok.id for tok in line.feedback_ids])
                line.total_feedback = len([tok.id for tok in line.feedback_ids])
            else:
                line.average_start = 0
                line.total_feedback = 0

class ProductFeedBack(models.Model):
    _name = 'product.feed.back'

    product_tmpl_id = fields.Many2one('product.template', 'Partner')
    feedback_id = fields.Many2one('res.partner', 'Partner')
    feedback_date = fields.Datetime('Feedback Date')
    product_feedback = fields.Many2one('product.product')
    order_evaluation = fields.Selection([
        ('start_0', 'sao 0'),
        ('start_1', 'sao 1'),
        ('start_2', 'sao 2'),
        ('start_3', 'sao 3'),
        ('start_4', 'sao 4'),
        ('start_5', 'sao 5'),
    ], default='start_5', string='Order Evaluation')
    order_feedback_content = fields.Char('Order Feedback Content')
    order_feedback_tag_id = fields.Many2many('order.feedback.tag',
                                             string='Order Feedback Tag')
    order_feedback_image = fields.One2many('order.feedback.image',
                                           'product_feed_back_id')
    order_feedback_videos = fields.One2many('order.feedback.videos',
                                            'product_feed_back_id')

