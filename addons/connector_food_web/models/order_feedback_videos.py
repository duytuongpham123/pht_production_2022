import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class OrderFeedBackVideo(models.Model):
    _name = 'order.feedback.videos'

    feed_back_id = fields.Many2one('feed.back')
    product_feed_back_id = fields.Many2one('product.feed.back')
    name = fields.Char('Tên')
    image = fields.Char('Url Video')