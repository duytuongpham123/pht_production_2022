import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging
from geopy.geocoders import Nominatim

# Create a custom logger
logger = logging.getLogger(__name__)

class Branch(models.Model):
    _name = 'branch.partner'

    name = fields.Char('Điểm bán')
    address = fields.Char('Địa chỉ')
    # ward = fields.Many2one('res.country.ward', 'Phường/Xã')
    # district = fields.Many2one('res.country.district', 'Quận/huyện')
    state = fields.Many2one('res.country.state', 'Tỉnh/Thành phố')
    lat = fields.Float('Lat',  digits=(12,12))
    long = fields.Float('Long', digits=(12,12))
    time_buy = fields.Float('Giờ bán')

    def get_address(self):
        try:
            geolocator = Nominatim(user_agent="duytuongpham123@gmail.com")
            location = geolocator.reverse((self.lat, self.long))
            data = location.raw['address']
            if location:
                self.address = data['road'] if 'road' in data else ''
                if 'suburb' in data:
                    self.ward = data['suburb']
                if 'village' in data:
                    self.ward = data['village']
                if 'town' in data:
                    self.district = data['town']
                if 'county' in data:
                    self.district = data['county']
                self.state = data['country'] if 'country' in data else ''
        except NameError:
            raise UserError('{}'.format(NameError))