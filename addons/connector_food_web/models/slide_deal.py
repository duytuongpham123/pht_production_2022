import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class SlideDeal(models.Model):
    _name = 'slide.deal'

    image_128 = fields.Image('Hình ảnh')
    url = fields.Char('Url')
    website_id = fields.Many2one('website')
    company_id = fields.Many2one('res.company', default=1)
    is_published = fields.Boolean(default=True)