import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class LikeProduct(models.Model):
    _name = 'like.product'

    product_id = fields.Many2one('product.template', 'Sản phẩm')
    price_unit = fields.Float('Đơn giá')
    user_id = fields.Many2one('res.users')