import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class PartnerCoinOut(models.Model):
    _name = 'partner.coin.out'

    partner_id = fields.Many2one('res.partner')
    coin_out = fields.Float('Coin out')
    date_out = fields.Datetime('Date out')
    so_id = fields.Many2one('sale.order')