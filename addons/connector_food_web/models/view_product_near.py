import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class ViewProductNear(models.Model):
    _name = 'view.product.near'

    product_id = fields.Many2one('product.template')
    name = fields.Char(related='product_id.name', store=True)