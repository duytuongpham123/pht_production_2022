import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class FeedBack(models.Model):
    _name = 'feed.back'

    name = fields.Char('Tên', readonly=1)
    feedbacker_id = fields.Many2one('res.partner', 'Feedbacker')
    feedback_date = fields.Datetime('Feedback Date')
    shipping_code = fields.Char('Shipping code')
    shipper_evaluation = fields.Selection([
        ('start_0', 'sao 0'),
        ('start_1', 'sao 1'),
        ('start_2', 'sao 2'),
        ('start_3', 'sao 3'),
        ('start_4', 'sao 4'),
        ('start_5', 'sao 5'),
    ], default='start_5', string='Shipper Evaluation')
    shipper_feedback_content = fields.Char('Shipper Feedback Content')
    shipper_feedback_tag_id = fields.Many2many('shipper.feedback.tag', string='Shipper Feedback Tag')
    order_id = fields.Many2one('sale.order', 'Sale order')
    order_evaluation = fields.Selection([
        ('start_0', 'sao 0'),
        ('start_1', 'sao 1'),
        ('start_2', 'sao 2'),
        ('start_3', 'sao 3'),
        ('start_4', 'sao 4'),
        ('start_5', 'sao 5'),
    ], default='start_5', string='Order Evaluation')
    order_feedback_content = fields.Char('Order Feedback Content')
    order_feedback_tag_id = fields.Many2many('order.feedback.tag', string='Order Feedback Tag')
    order_feedback_image = fields.One2many('order.feedback.image', 'feed_back_id')
    order_feedback_videos = fields.One2many('order.feedback.videos', 'feed_back_id')
    coin_in = fields.Float('Coin In')

    @api.model
    def create(self, vals_list):
        if len(self.search([]).mapped('id')) == 0:
            number_str = "1"
        else:
            check = self.search([], order="id asc")[-1]
            conver_data = check.name.split('-')[1]
            number_str = str(int(conver_data) + 1)
        zero_filled_number = "FB-{}".format(number_str.zfill(5))
        res = super(FeedBack, self).create(vals_list)
        res.name = zero_filled_number
        return res


class ShipperFeedbackTag(models.Model):
    _name = 'shipper.feedback.tag'

    feed_back_id = fields.Many2one('feed.back')
    name = fields.Char('Tag name')
    color = fields.Integer('Color')

class OrderFeedbackTag(models.Model):
    _name = 'order.feedback.tag'

    feed_back_order_id = fields.Many2one('feed.back')
    name = fields.Char('Tag name')
    color = fields.Integer('Color')