import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class SlideBuyNow(models.Model):
    _name = 'slide.buy.now'

    image = fields.Image('Hình ảnh')
    url = fields.Char('Url')