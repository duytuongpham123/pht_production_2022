import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class BlogDealHot(models.Model):
    _name = 'blog.deal.hot'

    name = fields.Char('Tên')
    image = fields.Image('Hình ảnh')
    content = fields.Text('Nội dung')