import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class OrderFeedBackImage(models.Model):
    _name = 'order.feedback.image'

    feed_back_id = fields.Many2one('feed.back')
    product_feed_back_id = fields.Many2one('product.feed.back')
    name = fields.Char('Tên')
    image = fields.Image('Hình ảnh')

    @api.model
    def create(self, vals_list):
        number_str = ""
        if len(self.search([]).filtered(lambda x: x.name).mapped('id')) == 0:
            number_str = "1"
        else:
            check = self.search([], order="id asc").filtered(lambda x: x.name)[-1]
            if check:
                convert_data = check.name.split('-')[1]
                number_str = str(int(convert_data) + 1)
        zero_filled_number = "Hình-{}".format(number_str)
        res = super(OrderFeedBackImage, self).create(vals_list)
        res.name = zero_filled_number
        return res