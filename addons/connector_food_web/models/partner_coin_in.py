import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class PartnerCoinIn(models.Model):
    _name = 'partner.coin.in'

    partner_id = fields.Many2one('res.partner')
    coin_in = fields.Float('Coin in')
    date_in = fields.Datetime('Date in')
    feedback_id = fields.Many2one('feed.back')