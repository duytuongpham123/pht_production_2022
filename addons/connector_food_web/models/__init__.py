
from . import slide_buy_now
from . import slide_deal
from . import product
from . import blog_deal_hot
from . import res_partner
from . import feed_back
from . import order_feedback_image
from . import order_feedback_videos
from . import partner_coin_in
from . import partner_coin_out
from . import branch
from . import res_setting
from . import like_product
from . import view_product_near
from . import cooking_recipe