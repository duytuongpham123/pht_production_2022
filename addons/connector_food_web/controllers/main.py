# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
from odoo import http, _
from odoo.http import request
from datetime import datetime, date, timedelta
from odoo.addons.web.controllers.main import Binary
import requests
import json
import random
import urllib
import haversine as hs
from odoo.addons.sale.controllers.variant import VariantController
import logging
import json
from collections import Counter


# Create a custom logger
logger = logging.getLogger(__name__)


class FoodWeb(http.Controller):

    def get_base_url(self):
        # base_url = request.httprequest.host_url
        base_url = 'http://localhost:8882/'
        return base_url

    def check_current_partner(self):
        user = request.env['res.users'].sudo().search([
            ('id', '=', request.session.uid)
        ])
        return user.partner_id.id

    @http.route('/slide_buy_now', type='json', auth='public', methods=['POST'], website=False)
    def slide_buy_now(self):
        get_slide = request.env['slide.buy.now'].sudo().search([])
        arr = []
        base_url = self.get_base_url()
        for line in get_slide:
            value = {
                'id': line.id,
                'image': "{}web/image1?model=slide.buy.now&id={}&field=image".format(base_url, line.id),
                'url': line.url
            }
            arr.append(value)
        return arr

    @http.route('/slide_deal', type='json', auth='public', methods=['POST'])
    def slide_deal(self):
        get_slide = request.env['slide.deal'].sudo().search([])
        arr = []
        base_url = self.get_base_url()
        for line in get_slide:
            value = {
                'id': line.id,
                'image': "{}web/image1?model=slide.deal&id={}&field=image_128".format(base_url, line.id),
                'url': line.url
            }
            arr.append(value)
        return arr

    @http.route('/category/product', type='json', auth='public', method=['POST'])
    def get_category(self):
        category = request.env['product.public.category'].sudo().search([])
        arr = []
        base_url = self.get_base_url()
        for line in category:
            value = {
                'id': line.id,
                'name': line.name,
                'parent_id': {
                    'id': line.parent_id.id,
                    'name': line.parent_id.name
                } if line.parent_id else False,
                'image': "{}web/image?model=product.public.category&id={}&field=image_1920".format(
                    base_url, line.id),
            }
            arr.append(value)
        return arr

    @http.route('/list/product', type='json', auth='public', method=['POST'])
    def get_list_product(self):
        page = int(request.jsonrequest.get('page')) - 1
        cate_id = request.jsonrequest.get('categ_id')
        limit_record = int(request.jsonrequest.get('limit_record'))
        product_template = request.env['product.template'].sudo().search([
            ('public_categ_ids', 'in', [cate_id]),
            ('is_published', '=', True)
        ], limit=limit_record, offset=page*limit_record)
        product_template_all = request.env['product.template'].sudo().search([
            ('public_categ_ids', 'in', [cate_id]),
            ('is_published', '=', True)
        ])
        like_product = request.env['like.product'].sudo().search([
            ('user_id', '=', request.session.uid)
        ]).mapped('product_id.id')
        arr = []
        base_url = self.get_base_url()
        for line in product_template:
            value = {
                'id': line.id,
                'name': line.name,
                'sale_price': line.list_price,
                'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                    base_url, line.id),
                'categ_id': [
                    [{
                        'id': child.id,
                        'name': child.name,
                    }]
                    for child in line.public_categ_ids],
                'qty_available': line.qty_available,
                'sales_count': line.sales_count,
                'star': round(line.average_start, 1),
                'flag_like': True if line.id in like_product else False,
                'description': line.description,
                'accessory_product_ids': [
                    {
                        'id': child.id,
                        'name': child.name,
                        'image': "{}web/image?model=product.product&id={}&field=image_128".format(
                            base_url, child.id),
                    }
                    for child in line.accessory_product_ids]
            }
            if not line.attribute_line_ids:
                product_product = request.env['product.product'].sudo().search([
                    ('product_tmpl_id', '=', line.id)
                ])
                value.update({'product_id': product_product.id})
            arr.append(value)
        return {
            'total_record': len(product_template_all.mapped('id')),
            'list_product': arr
        }


    @http.route('/featured/products', type='json', auth='public', method=['POST'])
    def featured_products(self):
        featured_products = request.env['product.template'].sudo().search([
            ('is_published', '=', True)
        ], order="sales_count_1 desc", limit=5)
        arr = []
        base_url = self.get_base_url()
        for line in featured_products:
            value = {
                'id': line.id,
                'name': line.name,
                'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                    base_url, line.id),
                'categ_id': [
                    [{
                        'id': child.id,
                        'name': child.name,
                    }]
                    for child in line.public_categ_ids],
                'qty_available': line.qty_available,
                'sales_count': line.sales_count_1,
                'sale_price': line.list_price,
                'star': round(line.average_start, 1),
                'description': line.description,
                'accessory_product_ids': [
                    [{
                        'id': child.id,
                        'name': child.name,
                        'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                            base_url, child.id),
                    }]
                    for child in line.accessory_product_ids]
            }
            arr.append(value)
        return arr

    @http.route('/blog/deal/hot', type='json', auth='public', method=['POST'])
    def blog_deal_hot(self):
        arr = []
        base_url = self.get_base_url()
        deal_hot = request.env['blog.deal.hot'].sudo().search(
            [], order="create_date desc", limit=10)
        for line in deal_hot:
            value = {
                'id': line.id,
                'name': line.name,
                'image': "{}web/image?model=blog.deal.hot&id={}&field=image".format(
                    base_url, line.id),
                'content': line.content
            }
            arr.append(value)
        return arr

    @http.route('/detail/user', type='json', auth='public', method=['POST'])
    def detail_user(self):
        id_partner = self.check_current_partner()
        partner = request.env['res.partner'].sudo().search([
            ('id', '=', id_partner)
        ])
        user = request.env['res.users'].search([
            ('id', '=', request.uid)
        ])
        if partner:
            value = {
                'id': partner.id,
                'name': partner.name,
                'sex': partner.sex,
                'mobile': partner.phone,
                'email': partner.email,
                'birthday': partner.birthday,
                'user': user.login
            }
            return value

    @http.route('/history/sale/order', type='json', auth='public', methods=['POST'])
    def history_sale_order(self):
        id_partner = self.check_current_partner()
        models = request.env['sale.order']
        base_url = self.get_base_url()
        arr1 = []
        arr2 = []
        arr3 = []

        # so coming and history
        so = models.sudo().search([
            ('state', '=', 'sale'),
            ('partner_id', '=', id_partner)
        ])
        so_cancel = models.sudo().search([
            ('state', '=', 'cancel'),
            ('partner_id', '=', id_partner)
        ])
        for line in so:
            value_comming = {}
            value_history = {}
            value_all = {
                'id': line.id,
                'name': line.name,
                'date_time_delivery': line.date_delivery or '',
                'total_amount': json.loads(line.tax_totals_json)['amount_total'],
                'flag_feedback': line.flag_feedback
            }
            for order_line in line.order_line:
                if order_line.qty_delivered == 0 and line.check_customer_confirm == False:
                    key = (order_line.id)
                    if key not in value_comming:
                        value_comming.update({key: {
                            'product_id': order_line.product_id.id,
                            'product_name': order_line.product_id.display_name,
                            'qty': order_line.product_uom_qty,
                            'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                                base_url, order_line.product_id.product_tmpl_id.id),
                        }})

                if line.check_customer_confirm == True:
                    key = (order_line.id)
                    if key not in value_comming:
                        value_history.update({key: {
                            'product_id': order_line.product_id.id,
                            'product_name': order_line.product_id.display_name,
                            'qty': order_line.product_uom_qty,
                            'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                                base_url,
                                order_line.product_id.product_tmpl_id.id),
                        }})
            arr_so_coming = []
            arr_so_history = []
            for i in value_comming.values():
                arr_so_coming.append(i)
            for z in value_history.values():
                arr_so_history.append(z)
            if arr_so_coming:
                value_all['order_line'] = arr_so_coming
                value_all['image'] = arr_so_coming[0]['image']
                arr1.append(value_all)
            if arr_so_history:
                value_all['order_line'] = arr_so_history
                value_all['image'] = arr_so_history[0]['image']
                arr2.append(value_all)

        for line in so_cancel:
            value_cancel = {}
            value_all = {
                'id': line.id,
                'name': line.name,
                'date_time_delivery': line.date_delivery or '',
                'total_amount': json.loads(line.tax_totals_json)[
                    'amount_total']
            }
            for order_line in line.order_line:
                key = (order_line.id)
                value_cancel.update({key: {
                    'product_id': order_line.product_id.id,
                    'product_name': order_line.product_id.display_name,
                    'qty': order_line.product_uom_qty,
                    'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                        base_url, order_line.product_id.product_tmpl_id.id),
                }})
            arr_so_cancel = []
            for i in value_cancel.values():
                arr_so_cancel.append(i)
            if arr_so_cancel:
                value_all['order_line'] = arr_so_cancel
                value_all['image'] = arr_so_cancel[0]['image']
                arr3.append(value_all)

        return {
            'so_coming': arr1,
            'so_history': arr2,
            'so_cancel': arr3
        }

    # action cancel sale order
    @http.route('/action_cancel_so', type='json', auth='public',  methods=['POST'])
    def action_cancel_so(self):
        order_id = request.jsonrequest.get('order_id')
        check_so = request.env['sale.order'].sudo().browse(order_id)
        try:
            if check_so:
                check_so.sudo().update({
                    'state': 'cancel',
                    'date_cancel': datetime.now()
                })
                return {
                    'status': 'Success',
                    'message': 'Bạn đã hủy đơn thành công',
                }
        except NameError:
            return {
                'status': 'Error',
                'message': 'Lỗi hệ thống {}'.format(NameError),
            }

    # action confirm so on customer not odoo
    @http.route('/action_confirm_customer', type='json', auth='public', methods=['POST'])
    def action_confirm_customer(self):
        order_id = request.jsonrequest.get('order_id')
        check_so = request.env['sale.order'].sudo().browse(order_id)
        try:
            if check_so:
                check_so.sudo().update({
                    'check_customer_confirm': True
                })
                return {
                    'status': 'Success',
                    'message': 'Bạn đã xác nhận thành công',
                }
        except NameError:
            return {
                'status': 'Error',
                'message': 'Lỗi hệ thống {}'.format(NameError),
            }

    @http.route('/create_so_quotation', type='json', auth='public', methods=['POST'])
    def create_so_quotation(self):
        arr_order_line = []
        arr_order_line_discount = []
        arr_order_line_free_product = []
        arr_free_product = []
        partner_id = self.check_current_partner()
        order_line = request.jsonrequest.get('order_line')
        model_coupon_program = request.env['coupon.program']
        flag_coin = request.jsonrequest.get('flag_coin')
        branch_id = request.jsonrequest.get('branch_id')
        date_delivery = request.jsonrequest.get('date_delivery')
        time_delivery = request.jsonrequest.get('time_delivery')
        note_sale = request.jsonrequest.get('note_sale')
        id_address = request.jsonrequest.get('address')
        payment_type_id = request.jsonrequest.get('payment_type_id')
        str_date_delivery = "{} {}:00".format(date_delivery, time_delivery)
        result_date_delivery = datetime.strptime(str_date_delivery, '%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
        for line in order_line:
            c_p_discount = model_coupon_program.sudo().search([
                ('reward_id.reward_type', '=', 'discount')
            ])
            c_p_free_product = model_coupon_program.sudo().search([
                ('reward_id.reward_type', '=', 'product')
            ])
            price_product = request.env['product.product'].sudo().browse(line['product_id'])
            # lấy giá product miễn phí
            if line['price_unit'] == 0:
                price_unit_so = 0
            else:
                price_unit_so = line['product_uom_qty'] * price_product.lst_price
            value_order_line = {
                'product_id': line['product_id'],
                'product_uom_qty': line['product_uom_qty'],
                'price_unit': price_unit_so,
                'tax_id': [(6, 0, [])]
            }
            for discount in c_p_discount:
                del_first_last = discount.rule_products_domain[1:-1]
                product = request.env['product.product'].sudo().search(
                    [tuple(json.loads(del_first_last.strip('"')))]
                )
                if line['product_id'] in product.ids:
                    # arr_order_line_discount.append((0, 0, {
                    #     'product_id': discount.reward_id.discount_line_product_id.id,
                    #     'tax_id': [(6, 0, [])],
                    #     'price_unit': -price_unit_so * discount.reward_id.discount_percentage / 100
                    # }))
                    value_order_line.update({
                        'price_unit': price_unit_so - (price_unit_so * discount.reward_id.discount_percentage / 100)
                    })
            for free_product in c_p_free_product:
                del_first_last = free_product.rule_products_domain[1:-1]
                product = request.env['product.product'].sudo().search(
                    [tuple(json.loads(del_first_last.strip('"')))]
                )
                if line['product_id'] in product.ids:
                    if line['product_id'] == free_product.reward_id.reward_product_id.id:
                        value_order_line.update({
                            'price_unit': 0
                        })
                    # arr_order_line_free_product.append((0, 0,
                    #     {
                    #         'product_id': free_product.reward_id.reward_product_id.id,
                    #         'tax_id': [(6, 0, [])],
                    #         'price_unit': 0
                    #     },
                    # ))
                    # arr_free_product.append((0, 0,
                    #     {
                    #         'product_id': free_product.reward_id.discount_line_product_id.id,
                    #         'price_unit': -free_product.reward_id.reward_product_id.list_price,
                    #         'tax_id': [(6, 0, [])]
                    #     },
                    # ))
            arr_order_line.append((0, 0, value_order_line))
        arr_result = arr_order_line
        try:
            partner_address = request.env['res.partner'].sudo().browse(id_address)
            product_pricelist = request.env['product.pricelist'].sudo().search([
                ('currency_id.name', '=', 'VND'),
                ('selectable', '=', True)
            ])
            so = request.env['sale.order'].sudo().create({
                'partner_id': partner_id,
                'order_line': arr_result,
                'state': 'sale',
                'pricelist_id': product_pricelist.id if product_pricelist else 1,
                'date_delivery': result_date_delivery,
                'note_sale': note_sale,
                'branch_id': branch_id,
                'user_id': False,
                'type_payment': payment_type_id,
                'name_partner_address': partner_address.name,
                'street': partner_address.street,
                'street2': partner_address.street2,
                'phone': partner_address.phone
            })
            if so:
                amount_total = sum(so.mapped('order_line.price_subtotal'))
                product_coin = request.env['product.product'].sudo().search([
                    ('categ_id.name', '=', 'coin')
                ])
                value_coin = {
                    'product_id': product_coin.id,
                    'price_unit': 0,
                    'tax_id': [(6, 0, [])],
                    'order_id': so.id
                }
                partner = request.env['res.partner'].sudo().search([
                    ('id', '=', self.check_current_partner())
                ])
                so_line = request.env['sale.order.line']
                # kiểm tra xem đã sài coin chưa
                if flag_coin:
                    if partner.total_coin >= amount_total:
                        so.sudo().update({
                            'coin_out': amount_total,
                            'date_out': datetime.now()
                        })
                        value_coin.update({'price_unit': -so.coin_out})
                        so_line.sudo().create(value_coin)
                    if partner.total_coin < amount_total:
                        so.sudo().update({
                            'coin_out': partner.total_coin,
                            'date_out': datetime.now()
                        })
                        value_coin.update({'price_unit': -so.coin_out})
                        so_line.sudo().create(value_coin)
                partner.coin_out_so_ids = [(4, so.id)]
                if so.state == 'sale':
                    return {
                        'status': 'Success',
                        'message': 'Bạn đã tạo thành công đơn hàng',
                        'result_so': {
                            'id': so.id,
                            'name': so.name,
                            'order_line': [
                                [{
                                    'id': child.id,
                                    'product': {
                                        'id': child.product_id.id,
                                        'name': child.product_id.name
                                    },
                                }]
                                for child in so.order_line
                            ]
                        }
                    }
                else:
                    return {
                        'status': 'Failed',
                        'message': 'Bạn đặt hàng thất bại {}'.format(NameError),
                    }
        except NameError:
            return {
                'status': 'Error',
                'message': 'lỗi hệ thống {}'.format(NameError),
            }

    @http.route('/click_buy_so', type='json', auth='public', methods=['POST'])
    def click_buy_so(self):
        date_out = request.jsonrequest.get('date_out')
        order = request.env['sale.order'].sudo().search([
            ('id', '=', request.jsonrequest.get('order_id')),
            ('partner_id', '=', self.check_current_partner()),
            ('state', '=', 'draft')
        ])
        product_coin = request.env['product.product'].sudo().search([
            ('categ_id.name', '=', 'coin')
        ])
        so_line = request.env['sale.order.line']
        value_coin = {
            'product_id': product_coin.id,
            'price_unit': 0,
            'order_id': order.id,
            'tax_id': [(6, 0, [])]
        }
        if order:
            order.update({
                'state': 'sale',
                'date_delivery': request.jsonrequest.get('date_delivery'),
                'note_sale': request.jsonrequest.get('note_sale')
            })
            amount_total = sum(order.mapped('order_line.price_subtotal'))
            # update coin out to partner
            partner = request.env['res.partner'].search([
                ('id', '=', self.check_current_partner())
            ])
            if partner.total_coin >= amount_total:
                order.update({
                    'coin_out': amount_total,
                    'date_out': date_out
                })
                value_coin.update({'price_unit': -order.coin_out})
                so_line.sudo().create(value_coin)
            if partner.total_coin < amount_total:
                order.update({
                    'coin_out': partner.total_coin,
                    'date_out': date_out
                })
                value_coin.update({'price_unit': -order.coin_out})
                so_line.sudo().create(value_coin)
            partner.coin_out_so_ids = [(4, order.id)]
            if order.state == 'sale':
                return {
                    'status': 'Success',
                    'message': 'Bạn đã đặt hàng thành công',
                    'result': {
                        'order_id': order.id,
                        'order_name': order.name
                    }
                }
            else:
                return {
                    'status': 'Failed',
                    'message': 'Bạn đã đặt thất bại'
                }
        else:
            return {
                'status': 'Error',
                'message': 'Không tìm thấy đơn hàng cần đặt'
            }

    @http.route('/create_user_login', type='json', auth='public', methods=['POST'], save_session=False)
    def create_user_login(self):
        name = request.jsonrequest.get('name')
        user_name = request.jsonrequest.get('user_name')
        password = request.jsonrequest.get('user_name')
        model_partner = request.env['res.partner']
        try:
            partner = model_partner.search([
                ('email', '=', user_name)
            ])
            if not partner:
                partner_data = model_partner.sudo().create({
                    'name': name,
                    'email': user_name
                })
                user = request.env['res.users'].sudo().create({
                    'login': user_name,
                    'password': password,
                    'partner_id': partner_data.id,
                    'active': True,
                    'company_id': 1,
                    'notification_type': 'email'
                })
                if user:
                    # only admin change  rule odoo
                    request.session.authenticate(request.env.cr.dbname, 'admin', 'admin')
                    # add group portal user
                    group_user = request.env['res.groups'].sudo().search(
                        [('id', '=', request.env.ref(
                            'base.group_user').id)])
                    # del permission user internal
                    group_user.users = [(3, user.id)]
                    # add permission user portal
                    group_user_portal = request.env['res.groups'].sudo().search(
                        [('id', '=', request.env.ref(
                            'base.group_portal').id)])
                    group_user_portal.sudo().write({'users': [(4, user.id)]})
                    return {
                        'status': 'Success',
                        'message': 'Bạn đã tạo tài khoản thành công'
                    }
                else:
                    return {
                        'status': 'Failed',
                        'message': 'Tài khoản đã được tạo'
                    }
        except NameError:
            return {
                'status': 'Error',
                'message': 'Error {}'.format(NameError)
            }

    @http.route('/login_user', type='json', auth='public', methods=['POST'])
    def login_user(self):
        user_name = request.jsonrequest.get('user_name')
        # login user odoo
        request.session.authenticate(request.env.cr.dbname, user_name, user_name)
        return request.env['ir.http'].session_info()

    @http.route('/logout_user', type='json', methods=['POST'],  auth="public", website=True)
    def logout(self):
        return {
            'status': 'Success',
            'message': 'Bạn đã đăng xuất thành công',
            'result': request.session.logout()
        }

    @http.route('/get_feedback_coin_in_partner', type='json', auth='public', methods=['POST'])
    def get_feedback_coin_in_partner(self):
        feedbacker = self.check_current_partner()
        shipping_code = request.jsonrequest.get('shipping_code')
        shipping_evaluation = request.jsonrequest.get('shipping_evaluation')
        shipper_feedback_content = request.jsonrequest.get('shipper_feedback_content')
        sale_order = request.jsonrequest.get('sale_order')
        order_evaluation = request.jsonrequest.get('order_evaluation')
        order_feedback_content = request.jsonrequest.get('order_feedback_content')
        order_feedback_tag_id = request.jsonrequest.get('order_feedback_tag_id')
        shipper_feedback_tag_id = request.jsonrequest.get('shipper_feedback_tag_id')
        len_driver_coin = sum(len(x) for x in request.jsonrequest.get('driver_coin').split())
        len_product_feedback_coin = sum(len(x) for x in request.jsonrequest.get('order_coin').split())
        image_feedback = request.jsonrequest.get('image_feedback')
        # video_feedback = request.jsonrequest.get('video_feedback')
        if len_driver_coin >= 50:
            driver_coin = 500
        else:
            driver_coin = 0
        product_feedback_coin = 0
        if len_product_feedback_coin >= 50 and len(image_feedback) >= 1:
            product_feedback_coin = 1000
        elif len_product_feedback_coin >= 50:
            product_feedback_coin = 500
        else:
            product_feedback_coin = 0

        coin_in = driver_coin + product_feedback_coin
        # new_video = request.env['order.feedback.image'].sudo().create(video_feedback)
        try:
            check_so_feedback = request.env['feed.back'].sudo().search([]).mapped('order_id.id')
            if sale_order not in check_so_feedback:
                feedback = request.env['feed.back'].sudo().create({
                    'feedbacker_id': feedbacker,
                    'feedback_date': datetime.now(),
                    'shipping_code': shipping_code,
                    'shipper_evaluation': shipping_evaluation,
                    'shipper_feedback_content': shipper_feedback_content,
                    'order_evaluation': order_evaluation,
                    'order_feedback_content': order_feedback_content,
                    'coin_in': coin_in,
                    'order_id': sale_order,
                    'order_feedback_tag_id': [(6, 0, order_feedback_tag_id)],
                    'shipper_feedback_tag_id': [(6, 0, shipper_feedback_tag_id)]
                    # 'order_feedback_videos': [(6, 0, new_video.ids)]
                })
                if feedback:
                    arr_img = []
                    for line in image_feedback:
                        arr_img.append({
                            'image': line,
                            'feed_back_id': feedback.id
                        })

                    new_image_feedback = request.env['order.feedback.image'].sudo().create(arr_img)
                    feedback.feedbacker_id.coin_in_ids = [(4, feedback.id)]
                    # update confirm feedback in SO
                    feedback.order_id.sudo().update({
                        'flag_feedback': True
                    })
                    for so_line in feedback.order_id.order_line:
                        product_template = request.env['product.product'].sudo().browse(so_line.product_id.id).product_tmpl_id
                        product_feedback = request.env['product.feed.back'].sudo().create({
                            'product_tmpl_id': product_template.id,
                            'feedback_id': feedback.feedbacker_id.id,
                            'feedback_date': feedback.feedback_date,
                            'product_feedback': so_line.product_id.id,
                            'order_evaluation': feedback.order_evaluation,
                            'order_feedback_content': feedback.order_feedback_content,
                            'order_feedback_tag_id': [(6, 0, order_feedback_tag_id)]
                            # 'order_feedback_videos': [(6, 0, new_video.ids)]
                        })
                        arr_img_pr = []
                        for line in image_feedback:
                            arr_img_pr.append({
                                'image': line,
                                'product_feed_back_id': product_feedback.id
                            })
                        new_image_product_feedback = request.env['order.feedback.image'].sudo().create(arr_img_pr)

                    return {
                        'status': 'Success',
                        'message': 'Bạn đã đánh giá thành công'
                    }
            else:
                return {
                    'status': 'Failed',
                    'message': 'Đơn hàng nãy đã được đánh giá'
                }
        except NameError:
            return {
                'status': 'Error',
                'message': '{}'.format(NameError)
            }

    @http.route('/get_nearest_branch', type='json', auth='public', methods=['POST'])
    def get_nominatim_geocode(self):
        partner = request.env['res.partner'].browse(self.check_current_partner())
        branch = request.env['branch.partner'].sudo().search([])
        arr_branch = []
        for line in branch:
            if line.lat and line.long:
                coord_1 = (line.lat, line.long)
                coord_2 = (partner.partner_latitude, partner.partner_longitude)
                distance = hs.haversine(coord_1, coord_2)
                config_parameter = request.env['ir.config_parameter']
                param_distance = config_parameter.sudo().get_param('connector_food_web.distance')
                if distance < float(param_distance):
                    arr_branch.append({
                        'name': line.name,
                        'address': line.address,
                        'ward': line.ward,
                        'district_id': line.district,
                        'state_id': line.state,
                        'distance': distance
                    })
        result = sorted(arr_branch, key=lambda i: i['distance'])
        return {
            'status': 'Success',
            'message': 'Tất cả điểm bán gần nhất',
            'result': result
        }

    @http.route('/list_like_product', type='json', auth='public', methods=['POST'])
    def like_product(self):
        arr = []
        base_url = self.get_base_url()
        like_product = request.env['like.product'].sudo().search([
            ('user_id', '=', request.session.uid)
        ])
        for i in like_product:
            arr.append({
                'product_id': i.product_id.id,
                'product_name': i.product_id.name,
                'price_unit': i.product_id.list_price,
                'image': "{}web/image?model=product.template&id={}&field=image_128".format(base_url, i.product_id.id),
                'user_id': {
                    'id': i.user_id.id,
                    'user_name': i.user_id.login
                }
            })
        if len(arr) > 0:
            return {
                'status': 'Success',
                'message': 'Lấy thành công danh sách sản phẩm yêu thích',
                'result': arr
            }
        else:
            return {
                'status': 'Failed',
                'message': 'Hiện chưa có sản phẩm yêu thích'
            }

    @http.route('/create_like_product', type='json', auth='public', methods=['POST'])
    def create_like_product(self):
        product_id = request.jsonrequest.get('product_id')
        price_unit = request.jsonrequest.get('price_unit')
        flag_like = request.jsonrequest.get('flag_like')
        try:
            product_template = request.env['product.template'].sudo().search([
                ('id', '=', product_id)
            ])
            if product_template:
                product_template.sudo().update({
                    'flag_like': flag_like
                })
            if flag_like == True:
                like_product = request.env['like.product'].sudo().create({
                    'product_id': product_id,
                    'price_unit': price_unit,
                    'user_id': request.session.uid
                })
                if like_product:
                    return {
                        'status': 'Success',
                        'message': 'Thêm sản phẩm yêu thích thành công',
                        'result': {
                            'product': {
                                'id': like_product.product_id.id,
                                'name': like_product.product_id.name,
                            },
                            'price_unit': like_product.price_unit,
                            'user': {
                                'id': like_product.user_id.id,
                                'user_name': like_product.user_id.login,
                            }
                        }
                    }
            else:
                like_product = request.env['like.product'].sudo().search([
                    ('product_id', '=', product_id)
                ]).unlink()
                return {
                    'status': 'Success',
                    'message': 'Bạn đã xóa sản phẩm yêu thích thành công',
                }
        except NameError:
            return {
                'status': 'Error',
                'message': '{}'.format(NameError)
            }

    # unlink like product
    @http.route('/unlink_like_product', type='json', auth='public', methods=['POST'])
    def unlink_product(self):
        product_id = request.jsonrequest.get('product_id')
        product = request.env['like.product'].sudo().search([
            ('product_id', 'in', product_id),
            ('user_id', '=', request.session.uid)
        ])
        if product:
            product.sudo().unlink()
            return {
                'status': 'Success',
                'message': 'Bạn đã xóa thành công'
            }
        else:
            return {
                'status': 'Failed',
                'message': 'Bạn đã xóa thất bại'
            }

    @http.route('/list/partner', type='json', auth='public', methods=['POST'])
    def list_partner(self):
        id_partner = self.check_current_partner()
        res_partner = request.env['res.partner'].sudo().browse(id_partner)
        check_partner = request.jsonrequest.get('partner_id')
        flag_address = request.jsonrequest.get('flag_address')
        get_partner = request.env['res.partner'].sudo().browse(check_partner)
        arr_parent = []
        arr_child = []
        for line in res_partner:
            arr_parent.append({
                'id': line.id,
                'name': line.name,
                'phone': line.phone,
                'address': line.street,
                'street': line.street2,
                'flag_address': line.flag_address
            })
            for child in line.child_ids:
                arr_child.append({
                    'id': child.id,
                    'name': child.name,
                    'phone': child.phone,
                    'address': child.street,
                    'street': child.street2,
                    'flag_address': child.flag_address
                })
        l = arr_parent + arr_child
        arr_partner_default = [d['id'] for d in l]

        if flag_address == True:
            partner_default = request.env['res.partner'].sudo().search([
                ('id', 'in', arr_partner_default)
            ])
            for child in partner_default:
                if child.flag_address == True:
                    value = {
                        'id': child.id,
                        'name': child.name,
                        'phone': child.phone,
                        'address': child.street,
                        'street': child.street2,
                        'flag_address': child.flag_address
                    }
                    return value
        else:
            if check_partner:
                value = {
                    'id': get_partner.id,
                    'name': get_partner.name,
                    'phone': get_partner.phone,
                    'address': get_partner.street,
                    'street': get_partner.street2,
                    'flag_address': get_partner.flag_address
                }
                return value
            else:
                result = arr_parent + arr_child
                return result

    @http.route('/update/partner', type='json', auth='public', methods=['POST'])
    def update_partner(self):
        id_partner = request.jsonrequest.get('id')
        name = request.jsonrequest.get('name')
        phone = request.jsonrequest.get('phone')
        address = request.jsonrequest.get('address')
        street = request.jsonrequest.get('street')
        action = request.jsonrequest.get('flag_address')
        partner_root = request.env['res.partner']
        res_partner = partner_root.sudo().browse(id_partner)
        try:
            if action == True:
                # xét tất cả đều k mặc định
                id_partner_parent = self.check_current_partner()
                partner_parent = partner_root.sudo().browse(id_partner_parent)
                partner_parent.update({
                    'flag_address': False
                })
                child_partner = partner_root.search([('parent_id', '=', partner_parent.id)])
                child_partner.sudo().update({
                    'flag_address': False
                })
                # lấy mặc định địa chỉ cho partner này
                res_partner.sudo().update({
                    'flag_address': True,
                    'name': name,
                    'phone': phone,
                    'street': address,
                    'street2': street
                })
            if action == False:
                res_partner.sudo().update({
                    'name': name,
                    'phone': phone,
                    'street': address,
                    'street2': street,
                    'flag_address': False,
                })
            return {
                'status': 'Success',
                'message': '{}'.format(res_partner.name)
            }
        except NameError:
            return {
                'status': 'Error',
                'message': '{}'.format(NameError)
            }

    @http.route('/create/partner', type='json', auth='public', methods=['POST'])
    def create_partner(self):
        name = request.jsonrequest.get('name')
        phone = request.jsonrequest.get('phone')
        address = request.jsonrequest.get('address')
        street = request.jsonrequest.get('street')
        id_partner_parent = self.check_current_partner()
        partner_root = request.env['res.partner']
        res_partner = partner_root.sudo().browse(id_partner_parent)
        try:
            new_partner = request.env['res.partner'].sudo().create({
                'name': name,
                'phone': phone,
                'street': address,
                'street2': street,
                'parent_id': res_partner.id,
                'type': 'other',
                'flag_address': False
            })
            return {
                'status': 'Success',
                'message': '{}'.format(new_partner.name)
            }
        except NameError:
            return {
                'status': 'Error',
                'message': '{}'.format(NameError)
            }

    # delete address partner child
    @http.route('/delete/partner', type='json', auth='public', methods=['POST'])
    def delete_partner(self):
        id_partner = request.jsonrequest.get('id_partner')
        check = request.env['res.partner'].browse(id_partner)
        if check.parent_id:
            check.sudo().unlink()
            return {
                'status': 'Success',
                'message': 'Bạn đã xóa thành công'
            }
        else:
            return {
                'status': 'Failed',
                'message': 'Đây là địa chỉ cha bắt buộc phải có'
            }

    @http.route('/list/branch', type='json', auth='public', methods=['POST'])
    def list_branch(self):
        arr = []
        state = request.jsonrequest.get('state')
        branch = request.env['branch.partner']
        if state:
            list_branch = branch.sudo().search([('state', '=', state)])
        else:
            list_branch = branch.sudo().search([])
        for line in list_branch:
            arr.append({
                'id': line.id,
                'name': line.name,
                'address': line.address,
                'state': {
                    'id': line.state.id,
                    'name': line.state.name
                },
                'time': line.time_buy
            })
        return arr

    @http.route('/list/state', type='json', auth='public', methods=['POST'])
    def list_state(self):
        arr = []
        check = request.env['feed.back'].sudo().search([])
        for line in check:
            logger.info(line.name)
        list_state = request.env['res.country.state'].sudo().search([
            ('country_id', '=', 241)
        ])
        for line in list_state:
            arr.append({
                'id': line.id,
                'name': line.name
            })
        return arr

    @http.route('/list/partner/default', type='json', auth='public', methods=['POST'])
    def list_partner_default(self):
        id_partner = self.check_current_partner()
        res_partner = request.env['res.partner'].sudo().browse(id_partner)
        arr_parent = []
        arr_child = []
        for line in res_partner:
            if line.flag_address == True:
                arr_parent.append({
                    'id': line.id,
                    'name': line.name,
                    'phone': line.phone,
                    'address': line.street,
                    'street': line.street2,
                    'flag_address': line.flag_address
                })
            for child in line.child_ids:
                if child.flag_address == True:
                    arr_child.append({
                        'id': child.id,
                        'name': child.name,
                        'phone': child.phone,
                        'address': child.street,
                        'street': child.street2,
                        'flag_address': child.flag_address
                    })
        return arr_parent + arr_child

    # list bank
    @http.route('/list/bank', type='json', auth='public', methods=['POST'])
    def list_bank(self):
        id_partner = self.check_current_partner()
        res_partner = request.env['res.partner'].sudo().browse(id_partner)
        if res_partner.bank_ids:
            arr = []
            for line in res_partner.bank_ids:
                arr.append({
                    'id': {
                        'bank_id': line.bank_id.id,
                        'bank_name': line.bank_id.name,
                    },
                    'acc_number': line.acc_number
                })
            return arr
        else:
            return {}

    # create bank
    @http.route('/create/bank', type='json', auth='public', methods=['POST'])
    def create_bank(self):
        id_partner = self.check_current_partner()
        id_bank = request.jsonrequest.get('id_bank')
        acc_number = request.jsonrequest.get('acc_number')
        bank = request.env['res.partner.bank']
        try:
            new_bank = bank.sudo().create({
                'bank_id': id_bank,
                'acc_number': acc_number,
                'partner_id': id_partner

            })
            return {
                'status': 'Success',
                'message': '{}'.format(new_bank.bank_id.name)
            }
        except NameError:
            return {
                'status': 'Error',
                'message': '{}'.format(NameError)
            }

    # total coin
    @http.route('/total/coin', type='json', auth='public', methods=['POST'])
    def total_coin(self):
        id_partner = self.check_current_partner()
        res_partner = request.env['res.partner'].sudo().browse(id_partner)
        if res_partner:
            return res_partner.total_coin

    # history coin
    @http.route('/history/coin', type='json', auth='public', methods=['POST'])
    def history_coin(self):
        id_partner = self.check_current_partner()
        res_partner = request.env['res.partner'].sudo().browse(id_partner)
        arr_c_in = []
        arr_c_out = []
        page = request.jsonrequest.get('page') - 1
        limit_record = request.jsonrequest.get('limit_record')
        feed_back = request.env['feed.back'].sudo().search([
            ('id', 'in', res_partner.coin_in_ids.ids),
            ('feedbacker_id', '=', id_partner)
        ], offset=page*limit_record, limit=limit_record)
        sale_order = request.env['sale.order'].sudo().search([
            ('id', 'in', res_partner.coin_out_so_ids.ids),
            ('partner_id', '=', id_partner)
        ], offset=page*limit_record, limit=limit_record)
        for index, line in enumerate(feed_back):
            # date = datetime.strptime(str(line.feedback_date), '%Y-%m-%d %H:%M:%S') \
            #        + timedelta(hours=7) if line.feedback_date else False
            if line.coin_in > 0:
                arr_c_in.append({
                    'stt': index+1,
                    'id': line.id,
                    'name': line.name,
                    'feedback_date': line.feedback_date,
                    'coin_in': line.coin_in
                })
        for index, line in enumerate(sale_order):
            if line.coin_out > 0:
                # date = datetime.strptime(str(line.date_out), '%Y-%m-%d %H:%M:%S') \
                #        + timedelta(hours=7) if line.date_out else ''
                arr_c_out.append({
                    'stt': index + 1,
                    'id': line.id,
                    'name': line.name,
                    'date_out': line.date_out,
                    'coin_out': line.coin_out
                })
        return {
            'coin_in': {
                'data_coin': arr_c_in,
                'total_coin': len(arr_c_in)
            },
            'coin_out': {
                'data_coin': arr_c_out,
                'total_coin': len(arr_c_out)
            }
        }

    # list payment type
    @http.route('/payment/type', type='json', auth='public', methods=['POST'])
    def payment_type(self):
        arr = []
        payment = request.env['payment.type'].sudo().search([])
        for line in payment:
            arr.append({
                'id': line.id,
                'name': line.name
            })
        return arr

    # detail sale order
    @http.route('/detail/sale/order', type='json', auth='public', methods=['POST'])
    def detail_sale_order(self):
        order_id = request.jsonrequest.get('order_id')
        check_so = request.env['sale.order'].sudo().browse(order_id)
        base_url = self.get_base_url()
        if check_so:
            value = {
                'address': {
                    'name': check_so.name_partner_address,
                    'address': check_so.street,
                    'street': check_so.street2,
                    'phone': check_so.phone
                },
                'branch': {
                    'id': check_so.branch_id.id,
                    'name': check_so.branch_id.name,
                    'address': check_so.branch_id.address,
                    'state': check_so.branch_id.state.name
                }
            }
            value_order_line = {}
            value_coin = {}
            for line in check_so.order_line:
                if line.id not in value_order_line:
                    if not line.product_id.categ_id.name == 'coin':
                        value_order_line.update({line.id: {
                            'id': line.id,
                            'product_id': line.product_id.name,
                            'variant': line.product_id.product_template_variant_value_ids.name if len(line.product_id.product_template_variant_value_ids) == 1 else '',
                            'price_unit': line.price_unit,
                            'product_uom_qty': line.product_uom_qty,
                            'price_subtotal': line.price_subtotal,
                            'image': "{}web/image?model=product.product&id={}&field=image_128".format(base_url, line.product_id.id)
                        }})
                    else:
                        value_coin.update({line.id: {
                            'product_id': line.product_id.name,
                            'price_subtotal': line.price_subtotal
                        }})
            arr_so = []
            arr_coin = []
            for i in value_order_line.values():
                arr_so.append(i)
            for z in value_coin.values():
                arr_coin.append(z)
            date_order = datetime.strptime(str(check_so.date_order), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7) if check_so.date_order else ''
            date_delivery = datetime.strptime(str(check_so.date_delivery), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7) if check_so.date_delivery else ''
            # date_cancel = datetime.strptime(str(check_so.date_cancel), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7) if check_so.date_cancel else ''
            # total_amount = json.loads(check_so.tax_totals_json)['amount_total']
            total_amount = float(sum(x['price_subtotal'] for x in arr_so))
            value['state'] = check_so.state
            value['order_line'] = arr_so
            value['count_order_line'] = len(arr_so)
            value['name'] = check_so.name
            value['date_order'] = date_order.strftime('%d-%m-%Y %H:%M:%S') if date_order else ''
            value['date_delivery'] = date_delivery.strftime('%d-%m-%Y %H:%M:%S') if date_delivery else ''
            value['date_cancel'] = check_so.date_cancel
            value['check_customer_confirm'] = check_so.check_customer_confirm
            value['flag_feedback'] = check_so.flag_feedback
            value['type_payment'] = check_so.type_payment.name
            value['coin'] = sum(x['price_subtotal'] for x in arr_coin) if arr_coin else 0
            value['total_amount'] = total_amount
            value['id'] = check_so.id
            value['result_total_amount'] = float(sum(x['price_subtotal'] for x in arr_coin)) + float(total_amount)
            return value

    # create view product near
    @http.route('/view/product/near', type='json', auth='public', methods=['POST'])
    def view_product_near(self):
        product_tmpl_id = request.jsonrequest.get('product_tmpl_id')
        models = request.env['view.product.near'].sudo()
        check_product = models.search([], order='create_date desc')
        if product_tmpl_id not in check_product.mapped('product_id.id'):
            if len(check_product) < 10:
                models.create({
                    'product_id': product_tmpl_id
                })
            if len(check_product) >= 10:
                # xóa vị trí xem cuối cùng
                check_product[-1].unlink()
                # thêm vị trí xem đầu tiên
                models.create({
                    'product_id': product_tmpl_id
                })
        else:
            check_curent_product = models.search([
                ('product_id', '=', product_tmpl_id)
            ])
            # xóa chính nó
            check_curent_product.unlink()
            # update lại chính nó mới nhất
            models.create({
                'product_id': product_tmpl_id
            })
        return {
            'status': 'Success'
        }

    # list view product near
    @http.route('/list/product/near', type='json', auth='public', methods=['POST'])
    def list_product_near(self):
        arr = []
        models = request.env['view.product.near'].sudo()
        check_product = models.search([], order='create_date desc')
        base_url = self.get_base_url()
        for line in check_product:
            value = {
                'id': line.product_id.id,
                'name': line.product_id.name,
                'sale_price': line.product_id.list_price,
                'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                    base_url, line.product_id.id),
                'star': round(line.product_id.average_start, 1),
                'description': line.product_id.description
            }
            arr.append(value)
        return arr

    # create content cook recipe
    @http.route('/create_data_cooking_recipe', type='json', auth='public', methods=['POST'])
    def create_data_cooking_recipe(self):
        comment = request.jsonrequest.get('comment')
        heart = request.jsonrequest.get('heart')
        id_cook_recipe = request.jsonrequest.get('id_cook_recipe')
        if comment:
            data = request.env['user.comment.cook.recipe'].sudo().create({
                'cook_recipe_id': id_cook_recipe,
                'user_id': request.session.uid,
                'date_comment': datetime.now(),
                'content_1': comment,
            })
        if heart:
            check = request.env['user.heart.cook.recipe'].sudo().search_count([
                ('user_id', '=', request.session.uid),
                ('cook_id', '=', id_cook_recipe)
            ])
            if check == 0:
                data = request.env['user.heart.cook.recipe'].sudo().create({
                    'cook_id': id_cook_recipe,
                    'user_id': request.session.uid,
                    'heart': heart
                })
        else:
            check = request.env['user.heart.cook.recipe'].sudo().search([
                ('user_id', '=', request.session.uid),
                ('heart', '=', True)
            ])
            check.sudo().unlink()

    # list cook recipe
    @http.route('/list_cook_recipe', type='json', auth='public', methods=['POST'])
    def list_cook_recipe(self):
        list_data = request.env['cooking.recipe'].sudo().search([])
        arr = []
        base_url = self.get_base_url()
        for line in list_data:
            arr.append({
                'name': line.name,
                'image': "{}web/image1?model=cooking.recipe&id={}&field=image".format(
                    base_url, line.id),
                'heart': line.heart,
                'comment': line.comment
            })
        return arr

    # add message user
    @http.route('/chat_user', type='json', auth='public', methods=['POST'])
    def chat_user(self):
        models_partner = request.env['res.partner']
        models_user = request.env['res.users']
        models_channel = request.env['mail.channel']
        id_partner = self.check_current_partner()
        partner = models_partner.sudo().browse(id_partner)
        body = request.jsonrequest.get('body')
        check_admin = models_user.sudo().search([('login', '=', 'admin')]).partner_id.name
        str_name_1 = '{}, {}'.format(partner.name, check_admin)
        str_name_2 = '{}, {}'.format(check_admin, partner.name)
        channel = models_channel.sudo().search([
            ('name', 'in', [str_name_1, str_name_2]),
            ('channel_type', '=', 'chat')
        ], limit=1)
        if not channel:
            new_channel = models_channel.sudo().create({
                'name': str_name_1,
                'active': True,
                'channel_type': 'chat'
            })
            new_channel.sudo().message_post(
                subject="Khách hàng",
                body='<p>{}</p>'.format(body),
                message_type='comment',
                subtype_xmlid='mail.mt_comment',
            )
            value = {
                'partner_id': models_user.sudo().search([('login', '=', 'admin')]).partner_id.id,
                'channel_id': new_channel.id
            }
            new_channel.channel_last_seen_partner_ids = [(0, 0, value)]
        else:
            channel.sudo().message_post(
                subject="Khách hàng",
                body='<p>{}</p>'.format(body),
                message_type='comment',
                subtype_xmlid='mail.mt_comment',
               )

    # list chat user
    @http.route('/list_chat_user', type='json', auth='public', methods=['POST'])
    def list_chat_user(self):
        base_url = self.get_base_url()
        models_partner = request.env['res.partner']
        models_user = request.env['res.users']
        models_channel = request.env['mail.channel']
        id_partner = self.check_current_partner()
        check_admin = models_user.sudo().search([('login', '=', 'admin')]).partner_id.name
        partner = models_partner.sudo().browse(id_partner)
        str_name_1 = '{}, {}'.format(partner.name, check_admin)
        str_name_2 = '{}, {}'.format(check_admin, partner.name)
        channel = models_channel.sudo().search([
            ('name', 'in', [str_name_1, str_name_2]),
            ('channel_type', '=', 'chat')
        ], limit=1)
        list_chat = request.env['mail.message'].sudo().search([
            ('res_id', '=', channel.id),
            ('message_type', '=', 'comment')
        ], order="create_date asc")
        arr = []
        for line in list_chat:
            get_partner_chat = models_partner.sudo().browse(line.author_id.id)
            arr.append({
                'date': line.date,
                'body': line.body,
                'user_name': get_partner_chat.name,
                'image': "{}web/image?model=res.partner&id={}&field=image_1920".format(
                    base_url, get_partner_chat.id),
            })
        return arr

    # create view user cook
    @http.route('/create_view_user_cook', type='json', auth='public', methods=['POST'])
    def create_view_user_cook(self):
        id_cook_recipe = request.jsonrequest.get('id_cook_recipe')
        try:
            data = request.env['user.view.cook'].sudo().create({
                'view_cook_id': id_cook_recipe
            })
            return {
                'status': 'Success',
                'message': 'Bạn đã view thành công',
            }
        except NameError:
            return {
                'status': 'Error',
                'message': 'Lỗi hệ thống {}'.format(NameError),
            }

    # search product
    @http.route('/search_product', type='json', auth='public', methods=['POST'])
    def search_product(self):
        product_name = request.jsonrequest.get('name')
        product = request.env['product.template'].sudo().search([
            ('name', 'ilike', product_name)
        ], limit=20)
        arr = []
        for line in product:
            arr.append({
                'id': line.id,
                'name': line.name
            })
        return arr

    @http.route('/feedback_user', type='json', auth='public', methods=['POST'])
    def feedback_user(self):
        so_id = request.jsonrequest.get('so_id')
        partner = self.check_current_partner()
        check = request.env['feed.back'].sudo().search([
            ('order_id', '=', so_id),
            ('feedbacker_id', '=', partner)
        ], limit=1)
        value = {
            'name': check.name,
            'user': check.feedbacker_id.name,
            'feedback_date': check.feedback_date,
            'order_evaluation': check.order_evaluation,
            'shipper_evaluation': check.order_evaluation,
            'shipper_feedback_content': check.shipper_feedback_content,
            'shipper_feedback_tag_id': [
                {
                    'id': child.id,
                    'name': child.name,
                }
                for child in check.shipper_feedback_tag_id
            ],
            'order_id': check.order_id.name,
            'order_feedback_content': check.order_feedback_content,
            'order_feedback_tag_id': [
                {
                    'id': child.id,
                    'name': child.name,
                }
                for child in check.order_feedback_tag_id
            ],
            'image': [
                {
                    'id': child.id,
                    'image': child.image,
                }
                for child in check.order_feedback_image
            ]
        }
        return value

class WebsiteSaleVariantController(VariantController):

    def get_base_url(self):
        # base_url = request.httprequest.host_url
        base_url = 'http://localhost:8882/'
        return base_url

    @http.route('/detail/product', type='json', auth='public', methods=['POST'], website=False)
    def get_product(self):
        product_tmpl_id = request.jsonrequest.get('product_tmpl_id')
        get_product = request.env['product.template'].sudo().search([
            ('id', '=', product_tmpl_id),
            ('is_published', '=', True)
        ])
        like_product = request.env['like.product'].sudo().search([
            ('user_id', '=', request.session.uid)
        ]).mapped('product_id.id')
        arr = []
        base_url = self.get_base_url()
        for line in get_product:
            arr_pr = []
            value = {
                'id': line.id,
                'name': line.name,
                'image': "{}web/image?model=product.template&id={}&field=image_1920".format(base_url, line.id),
                'categ_id': [
                    {
                        'id': child.id,
                        'name': child.name,
                    }
                    for child in line.public_categ_ids],
                'qty_available': line.qty_available,
                'sales_count': line.sales_count_1,
                'accessory_product_ids': [
                    {
                        'id': child.id,
                        'name': child.name,
                        'image': "{}web/image?model=product.template&id={}&field=image_128".format(
                            base_url, child.id),
                    }
                    for child in line.accessory_product_ids],
                'sale_price': line.list_price,
                'average_start': round(line.average_start, 1),
                'total_feedback': line.total_feedback,
                'description': line.description,
                'flag_like': True if line.id in like_product else False
            }

            combination = []
            if line.product_variant_ids:
                # coupon program
                model_coupon_program = request.env['coupon.program']
                c_p_discount = model_coupon_program.sudo().search([
                    ('reward_id.reward_type', '=', 'discount')
                ])
                c_p_free_product = model_coupon_program.sudo().search([
                    ('reward_id.reward_type', '=', 'product')
                ])
                for line_pr in line.product_variant_ids:
                    if len(line_pr.combination_indices) > 1:
                        combination = list(map(int, line_pr.combination_indices.split(',')))
                    elif len(line_pr.combination_indices) == 1:
                        combination = [int(line_pr.combination_indices)]
                    mode_pr_tl_att_value = request.env['product.template.attribute.value']
                    product_tmpl_att_value = mode_pr_tl_att_value.sudo().search([
                        ('id', 'in', combination)
                    ])
                    get_price_extra_variant = self.get_combination_info(
                        line.id, line_pr.id, combination, False, False)
                    for index, i in enumerate(product_tmpl_att_value):
                        value_pr = {
                            'id': i.product_attribute_value_id.id,
                            'name': i.product_attribute_value_id.name,
                            'attribute_id': i.attribute_id.id,
                            'product_template_attribute_value': i.id,
                            'product_id': line_pr.id,
                            'price_extra': i.price_extra,
                            'display_name': get_price_extra_variant['display_name'],
                            'discount': {},
                            'free_product': {}
                        }
                        for discount in c_p_discount:
                            del_first_last = discount.rule_products_domain[1:-1]
                            product = request.env['product.product'].search(
                                [tuple(json.loads(del_first_last.strip('"')))]
                            )
                            if line_pr.id in product.ids:
                                value_pr.update({
                                    'discount': {
                                        'name': discount.name,
                                        'percentage': discount.reward_id.discount_percentage,
                                        'discount_product': {
                                            'product_id': discount.reward_id.discount_line_product_id.id,
                                            'product_name': discount.reward_id.discount_line_product_id.name,
                                            'image': "{}web/image?model=product.product&id={}&field=image_1920".format(base_url, discount.reward_id.discount_line_product_id.id),
                                        },
                                        'reward_type': discount.reward_id.reward_type
                                    },
                                })
                        for free_product in c_p_free_product:
                            del_first_last = free_product.rule_products_domain[1:-1]
                            product = request.env['product.product'].search(
                                [tuple(json.loads(del_first_last.strip('"')))]
                            )
                            if line_pr.id in product.ids:
                                value_pr.update({
                                    'free_product': {
                                        'name': free_product.name,
                                        'reward_product': {
                                            'product_id': free_product.reward_id.reward_product_id.id,
                                            'product_name': free_product.reward_id.reward_product_id.name,
                                            'image': "{}web/image?model=product.product&id={}&field=image_1920".format(
                                                base_url, free_product.reward_id.reward_product_id.id),

                                        },
                                        'discount_line_product_id': {
                                            'product_id': free_product.reward_id.discount_line_product_id.id,
                                            'product_name': free_product.reward_id.discount_line_product_id.name,
                                            'image': "{}web/image?model=product.product&id={}&field=image_1920".format(
                                                base_url, free_product.reward_id.discount_line_product_id.id),
                                        },
                                        'reward_type': free_product.reward_id.reward_type
                                    },
                                })
                        arr_pr.append(value_pr)
                value['attribute_line_ids'] = arr_pr
                if not arr_pr:
                    value['product_id'] = line.product_variant_ids.id
            arr.append(value)
        return arr

    # danh sach mon kem
    @http.route('/side/dish', type='json', auth='public', methods=['POST'])
    def list_side_dish(self):
        product_id = request.jsonrequest.get('product_id')
        product_product = request.env['product.product'].sudo().search([
            ('id', 'in', product_id)
        ])
        arr = []
        for child in product_product:
            check_pr_tl = request.env['product.template'].sudo().browse(child.product_tmpl_id.id)
            if check_pr_tl:
                base_url = self.get_base_url()
                for line in check_pr_tl.accessory_product_ids:
                    arr.append({
                        'id': line.id,
                        'name': line.name,
                        'image': "{}web/image?model=product.product&id={}&field=image_128".format(
                            base_url, line.id),
                        'price_list': line.list_price
                })
        return arr

    # danh sách dụng cụ
    @http.route('/product/tool', type='json', auth='public', methods=['POST'])
    def list_product_tool(self):
        product_template = request.env['product.template'].sudo().search([
            ('categ_id.name', '=', 'Dụng cụ')
        ])
        product_product = request.env['product.product'].sudo().search([
            ('product_tmpl_id', 'in', product_template.ids)
        ])
        arr = []
        base_url = self.get_base_url()
        for line in product_product:
            arr.append({
                'id': line.id,
                'name': line.name,
                'image': "{}web/image?model=product.product&id={}&field=image_128".format(
                    base_url, line.id),
                'price_list': line.list_price
            })
        return arr

    # list shipper feed back tag
    @http.route('/list/shipper/feedback', type='json', auth='public', methods=['POST'])
    def list_shipper_feedback(self):
        shipper_feedback =request.env['shipper.feedback.tag'].sudo().search([])
        arr = []
        for line in shipper_feedback:
            arr.append({
                'id': line.id,
                'name': line.name
            })
        return arr

    # list order feed back tag
    @http.route('/list/order/feedback', type='json', auth='public', methods=['POST'])
    def list_order_feedback(self):
        order_feedback = request.env['order.feedback.tag'].sudo().search([])
        arr = []
        for line in order_feedback:
            arr.append({
                'id': line.id,
                'name': line.name
            })
        return arr

    # check sale order
    @http.route('/check/sale/order', type='json', auth='public',  methods=['POST'])
    def check_sale_order(self):
        order_id = request.jsonrequest.get('order_id')
        sale_order = request.env['sale.order'].sudo().browse(order_id)
        if sale_order:
            value = {
                'id': sale_order.id,
                'name': sale_order.name,
                'date_delivery': sale_order.date_delivery
            }
            return value

    @http.route('/detail/product/feedback', type='json', auth='public', methods=['POST'], website=False)
    def get_product_feedback(self):
        arr_feedback = []
        arr_feedback_all = []
        value = {}
        product_tmpl_id = request.jsonrequest.get('product_tmpl_id')
        page = int(request.jsonrequest.get('page')) - 1
        limit_record = int(request.jsonrequest.get('limit_record'))
        start = request.jsonrequest.get('type_feedback')
        pr_feed_back = request.env['product.feed.back']
        base_url = self.get_base_url()
        get_product = request.env['product.template'].sudo().search([
            ('id', '=', product_tmpl_id),
            # ('is_published', '=', True)
        ])
        data = None
        arr_num = []
        for i in range(0,5):
            arr_num.append('start_{}'.format(i+1))
        if start in arr_num:
            operator = [
                ('product_tmpl_id', '=', get_product.id),
                ('order_evaluation', '=', start)
            ]
            data = pr_feed_back.sudo().search(
                operator,
                limit=limit_record,
                offset=page * limit_record,
                order="create_date desc"
            )
        if start == 'all':
            operator = [
                ('product_tmpl_id', '=', get_product.id)
            ]
            data = pr_feed_back.sudo().search(
                operator,
                limit=limit_record,
                offset=page * limit_record,
                order="create_date desc"
            )
        if start == 'has_comment':
            operator = [
                ('product_tmpl_id', '=', get_product.id)
            ]
            data = pr_feed_back.sudo().search(
                operator,
                limit=limit_record,
                offset=page * limit_record,
                order="create_date desc"
            ).filtered(lambda x: x.order_feedback_content)
        if start == 'has_image':
            operator = [
                ('product_tmpl_id', '=', get_product.id)
            ]
            data = pr_feed_back.sudo().search(
                operator,
                limit=limit_record,
                offset=page * limit_record,
                order="create_date asc"
            ).filtered(lambda x: x.order_feedback_image)
        for child in data:
            arr_feedback.append({
                'feedback_id': {
                    'id': child.feedback_id.id,
                    'name': child.feedback_id.name
                },
                'feedback_date': child.feedback_date,
                'product_feedback': {
                    'id': child.product_feedback.id,
                    'name': child.product_feedback.name,
                    'variant': child.product_feedback.product_template_variant_value_ids.name if child.product_feedback.product_template_variant_value_ids.name else child.product_feedback.name
                },
                'order_evaluation': child.order_evaluation.split("_")[1] if child.order_evaluation else 0,
                'order_feedback_content': child.order_feedback_content,
                'image_ids': [
                    {
                        'id': child1.id,
                        'name': child1.name,
                        'image': "{}web/image1?model=order.feedback.image&id={}&field=image".format(
                            base_url, child1.id),
                    }
                    for child1 in child.order_feedback_image],
            })
        for child1 in get_product.feedback_ids:
            arr_feedback_all.append({
                'feedback_id': {
                    'id': child1.feedback_id.id,
                    'name': child1.feedback_id.name
                },
                'feedback_date': child1.feedback_date,
                'product_feedback': {
                    'id': child1.product_feedback.id,
                    'name': child1.product_feedback.name
                },
                'order_evaluation': child1.order_evaluation,
                'order_feedback_content': child1.order_feedback_content,
                'image_ids': [
                    {
                        'id': child2.id,
                        'name': child2.name,
                        'image': "{}web/image1?model=order.feedback.image&id={}&field=image".format(
                            base_url, child2.id),
                    }
                    for child2 in child1.order_feedback_image],
            })
        value['feedback_ids'] = arr_feedback
        start = Counter(tok['order_evaluation'] for tok in arr_feedback_all)
        value['start'] = start if start else 0
        if arr_feedback_all:
            value['average_start'] = round(sum([int(tok['order_evaluation'].split("_")[1]) for tok in arr_feedback_all]) / len(arr_feedback_all), 1) if start else  0
        else:
            value['average_start'] = 0
        arr_tok = []
        arr_img = []
        for index, tok in enumerate(arr_feedback_all):
            if tok['order_feedback_content']:
                arr_tok.append(index)
            if tok['image_ids']:
                arr_img.append(index)
        value['comment'] = len(arr_tok)
        value['total_data'] = len(arr_feedback_all)
        value['image_video'] = len(arr_img)

        return value

class Binary1(Binary):

    @http.route(['/web/image1',
                 '/web/image1/<string:xmlid>',
                 '/web/image1/<string:xmlid>/<string:filename>',
                 '/web/image1/<string:xmlid>/<int:width>x<int:height>',
                 '/web/image1/<string:xmlid>/<int:width>x<int:height>/<string:filename>',
                 '/web/image1/<string:model>/<int:id>/<string:field>',
                 '/web/image1/<string:model>/<int:id>/<string:field>/<string:filename>',
                 '/web/image1/<string:model>/<int:id>/<string:field>/<int:width>x<int:height>',
                 '/web/image1/<string:model>/<int:id>/<string:field>/<int:width>x<int:height>/<string:filename>',
                 '/web/image1/<int:id>',
                 '/web/image1/<int:id>/<string:filename>',
                 '/web/image1/<int:id>/<int:width>x<int:height>',
                 '/web/image1/<int:id>/<int:width>x<int:height>/<string:filename>',
                 '/web/image1/<int:id>-<string:unique>',
                 '/web/image1/<int:id>-<string:unique>/<string:filename>',
                 '/web/image1/<int:id>-<string:unique>/<int:width>x<int:height>',
                 '/web/image1/<int:id>-<string:unique>/<int:width>x<int:height>/<string:filename>'],
                type='http', auth="none", website=True, save_session=False)
    def content_image1(self, xmlid=None, model='ir.attachment', id=None,
                      field='datas',
                      filename_field='name', unique=None, filename=None,
                      mimetype=None,
                      download=None, width=0, height=0, crop=False,
                      access_token=None,
                      **kwargs):
        request.session.authenticate(request.env.cr.dbname, 'admin',
                                     'admin')
        # other kwargs are ignored on purpose
        return request.env['ir.http']._content_image(xmlid=xmlid, model=model,
                                                     res_id=id, field=field,
                                                     filename_field=filename_field,
                                                     unique=unique,
                                                     filename=filename,
                                                     mimetype=mimetype,
                                                     download=download,
                                                     width=width,
                                                     height=height, crop=crop,
                                                     quality=int(
                                                         kwargs.get('quality',
                                                                    0)),
                                                     access_token=access_token)













