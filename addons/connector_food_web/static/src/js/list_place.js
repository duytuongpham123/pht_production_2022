odoo.define('pdt_work_entry.attendance_work_entry', function (require) {
  "use strict";
  var ListController = require('web.ListController');
  var rpc = require('web.rpc');
  
  ListController.include({
      renderButtons: function ($node) {
          this._super.apply(this, arguments);            
          if (this.modelName == 'list.place') {
            var your_btn = this.$buttons.find('button.o_list_button_custom_import');
            your_btn.on('click', this.proxy('_action_list_place'));
        }
      },
      _action_list_place: function(event){
        var self = this;
        rpc.query({
            model: 'list.place',
            method: 'remove_list_place',
            args: [],
        }).then(function (res) {
            console.log(1);
        });

    },
  });
});