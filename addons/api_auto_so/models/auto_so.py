# -*- coding: utf-8 -*-


from odoo import api, tools, models,fields, _
from odoo.tools import float_round
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare, float_round
from datetime import datetime, timedelta
import math

try:
    import requests
    import json
except ImportError:
    pass


class SaleOrder(models.Model):
    _inherit = 'sale.order'


    code_unique = fields.Char('Mã duy nhất')
    get_company = fields.Char('Company', compute='_get_company_a', store=True, )

    # @api.constrains('code_unique')
    # def _constraints_code_unique(self):
    #     if self.code_unique:
    #         check = self.sudo().search_count([('code_unique', '=', self.code_unique)])
    #         if check > 1:
    #             raise UserError('Bạn đã tạo trùng mã')


    def get_login_company_b(self):
        value_b = {
            "params": {
                "login": "admin",
                "password": "admin",
                "db": "company_b_1"
            }
        }
        login_user = "http://localhost:8069/web/session/authenticate"
        res_login = requests.post(login_user, json=value_b)
        return {'res_login': res_login}

    def aysc_data_so(self):
        order_lines = list()
        for line in self.order_line:
            vals = {
                    'product_name': line.product_id.name,
                    'product_id': line.product_id.id,
                    'bom_id': line.bom_id.id,
                    'description': line.description,
                    'product_uom_qty':line.product_uom_qty,
                    'price_unit': round(line.price_unit, -1),
                    'code': line.code,
                    'cost': line.cost,
                    'product_name_customer': line.product_name_customer.id,
                    'profit_percentage': line.profit_percentage,
                    'discount': line.discount_percentage,
                    'total_cost': line.total_cost,
                    'tax_id':[(6, 0, line.tax_id.ids)],

                }
            order_lines.append(vals)

        value={
            'code_unique': self.code_unique,
            'partner_id': self.env.company.name,
            'pricelist_id': self.pricelist_id.name,
            'x_po': self.x_po,
            'commitment_date': self.commitment_date,
            'order_line': order_lines
        }
        #login qua company B
        value_company_b = self.get_login_company_b()
        error_login = value_company_b.get('res_login').json().get('error')
       

        if error_login:
            error_mess = "{}".format(error_login.get('data').get('message')) 
            raise UserError(error_mess)

        else:
            session_login_id = value_company_b.get('res_login').headers.get('Set-Cookie').split(';')[0].strip()
            url = 'http://localhost:8069/auto/so/company_b'

            res = requests.get(url, session_login_id, json=value)
            error = res.json().get('error')
            
            if error:
                error_mess = "{}".format(error.get('data').get('message')) 
                raise UserError(error_mess)

            else:
                raise UserError('Bạn đã aysc thành công')
                return json.dumps(value)

    def action_create_partner_company_b(self):
        company_id = self.env.company.name
        value_company_b = self.get_login_company_b()
        session_login_id = value_company_b.get('res_login').headers.get('Set-Cookie').split(';')[0].strip()
        url = 'http://localhost:8069/auto/partner/company_b'
        res = requests.get(url, session_login_id, json={'name': company_id})
        error = res.json().get('error')
        if error:
            error_mess = "{}".format(error.get('data').get('message')) 
            raise UserError(error_mess)

        else:
            raise UserError('Bạn đã đồng bộ thành công')
            return json.dumps(value)

    @api.depends('company_id')
    def _get_company_a(self):
        for line in self:
            if line.company_id:
                line.get_company = line.company_id.name

    def get_confirm_company(self, flag):
        # get company B
        order_lines = list()
        company_b = self.env['res.company'].search([]).filtered(
            lambda x: x.name == 'CÔNG TY TNHH MỘT THÀNH VIÊN BAO BÌ PHƯỚC HIỆP THÀNH')
        partner_id = self.env['res.partner'].search([('name', '=', flag)])
        for line in self.order_line:
            tax_company_b = self.env['account.tax'].sudo().search([
                ('company_id', '=', company_b.id),
                ('name', 'in', line.tax_id.mapped('name')),
                ('type_tax_use', '=', 'sale'),
            ])
            order_lines.append((0, 0, {
                'product_id': line.product_id.id,
                'product_uom_qty': line.product_uom_qty,
                'price_unit': math.trunc(line.price_unit * 0.97),
                'tax_id': [(6, 0, tax_company_b.mapped('id'))],

            }))

        value = {
            'company_id': company_b.id,
            'partner_id': partner_id.id,
            'pricelist_id': self.pricelist_id.id,
            'commitment_date': self.commitment_date,
            'order_line': order_lines
        }
        company_a = self.env['res.company'].search([]).filtered(lambda x: x.name == self.company_id.name)
        if company_a.name == flag:
            sale_order_line = self.order_line
            sale_order = self.create(value)
            self.state = 'sale'
            partner = self.env['res.partner'].search(
                [('name', '=', 'CÔNG TY TNHH MỘT THÀNH VIÊN BAO BÌ PHƯỚC HIỆP THÀNH')])
            for line in sale_order_line:
                if len(line.product_id.seller_ids) == 0:
                    line.product_id.seller_ids = [(0, 0, {
                        'name': partner.id
                    })]

            if self.state == 'sale':
                result_confirm_so = self.order_line._action_launch_stock_rule_custom()

                if result_confirm_so:
                    # create po into company A
                    id_purchase_order = self.action_view_purchase_orders().get('res_id')
                    if id_purchase_order:
                        po = self.env['purchase.order'].search([('id', '=', id_purchase_order)])
                        # update PO name into company B => SO company A
                        sale_order.update({
                            'x_po': self.x_po
                        })
                        for line1 in sale_order_line:
                            for po_line in po.order_line:
                                # update price po
                                if po_line.product_id.id == line1.product_id.id:
                                    po_line.sudo().update({
                                        'product_id': line1.product_id.id,
                                        'price_unit': math.trunc(line1.price_unit * 0.97),
                                        'product_qty': line1.product_uom_qty,
                                        'taxes_id': [(6, 0, line1.tax_id.ids)],
                                    })
                        po.button_confirm()
            return sale_order

    # multi company not api
    def action_confirm_so_company_b(self):
        self.get_confirm_company('CÔNG TY TNHH THƯƠNG MẠI DỊCH VỤ GIAI PHÚ')
        self.get_confirm_company('CÔNG TY TNHH MTV PHÁT VẠN TƯỜNG')

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    # def _prepare_procurement_values_custom(self, group_id=False):
    #     """ Prepare specific key for moves or other components that will be created from a stock rule
    #     comming from a sale order line. This method could be override in order to add other custom key that could
    #     be used in move/po creation.
    #     """
    #     values = {}
    #     self.ensure_one()
    #     # Use the delivery date if there is else use date_order and lead time
    #     date_deadline = self.order_id.commitment_date or (
    #                 self.order_id.date_order + timedelta(days=self.customer_lead or 0.0))
    #     date_planned = date_deadline - timedelta(days=self.order_id.company_id.security_lead)
    #     route_stock_location = self.env['stock.location.route'].search([('name', '=', 'Buy')])
    #     values.update({
    #         'group_id': group_id,
    #         'sale_line_id': self.id,
    #         # 'date_planned': date_planned,
    #         # 'date_deadline': date_deadline,
    #         'route_ids': route_stock_location,
    #         # 'warehouse_id': self.order_id.warehouse_id or False,
    #         # 'partner_id': self.order_id.partner_shipping_id.id,
    #         # 'product_description_variants': self._get_sale_order_line_multiline_description_variants(),
    #         # 'company_id': self.order_id.company_id,
    #     })
    #     return values

    def _prepare_procurement_values_custom(self, group_id=False):
        """ Prepare specific key for moves or other components that will be created from a stock rule
        comming from a sale order line. This method could be override in order to add other custom key that could
        be used in move/po creation.
        """
        values = super(SaleOrderLine, self)._prepare_procurement_values(group_id)
        self.ensure_one()
        # Use the delivery date if there is else use date_order and lead time
        date_deadline = self.order_id.commitment_date or (self.order_id.date_order + timedelta(days=self.customer_lead or 0.0))
        date_planned = date_deadline - timedelta(days=self.order_id.company_id.security_lead)
        route_stock_location = self.env['stock.location.route'].search(['|', ('name', '=', 'Buy'), ('name', '=', 'Mua')])
        print(route_stock_location)
        values.update({
            'group_id': group_id,
            'sale_line_id': self.id,
            # 'date_planned': date_planned,
            # 'date_deadline': date_deadline,
            'route_ids': route_stock_location,
            # 'warehouse_id': self.order_id.warehouse_id or False,
            # 'partner_id': self.order_id.partner_shipping_id.id,
            # 'product_description_variants': self._get_sale_order_line_multiline_description_variants(),
            # 'company_id': self.order_id.company_id,
            # 'product_packaging_id': self.product_packaging_id,
        })
        return values

    # def _action_launch_stock_rule_custom(self, previous_product_uom_qty=False):
    #     """
    #     Launch procurement group run method with required/custom fields genrated by a
    #     sale order line. procurement group will launch '_run_pull', '_run_buy' or '_run_manufacture'
    #     depending on the sale order line product rule.
    #     """
    #     precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
    #     procurements = []
    #     for line in self:
    #         line = line.with_company(line.company_id)
    #         if line.state != 'sale' or not line.product_id.type in ('consu', 'product'):
    #             continue
    #         qty = line._get_qty_procurement(previous_product_uom_qty)
    #         if float_compare(qty, line.product_uom_qty, precision_digits=precision) >= 0:
    #             continue
    #
    #         group_id = line._get_procurement_group()
    #         if not group_id:
    #             group_id = self.env['procurement.group'].create(line._prepare_procurement_group_vals())
    #             line.order_id.procurement_group_id = group_id
    #         else:
    #             # In case the procurement group is already created and the order was
    #             # cancelled, we need to update certain values of the group.
    #             updated_vals = {}
    #             if group_id.partner_id != line.order_id.partner_shipping_id:
    #                 updated_vals.update({'partner_id': line.order_id.partner_shipping_id.id})
    #             if group_id.move_type != line.order_id.picking_policy:
    #                 updated_vals.update({'move_type': line.order_id.picking_policy})
    #             if updated_vals:
    #                 group_id.write(updated_vals)
    #
    #         values = line._prepare_procurement_values_custom(group_id=group_id)
    #         product_qty = line.product_uom_qty - qty
    #
    #         line_uom = line.product_uom
    #         quant_uom = line.product_id.uom_id
    #         product_qty, procurement_uom = line_uom._adjust_uom_quantities(product_qty, quant_uom)
    #         procurements.append(self.env['procurement.group'].Procurement(
    #             line.product_id, product_qty, procurement_uom,
    #             line.order_id.partner_shipping_id.property_stock_customer,
    #             line.name, line.order_id.name, line.order_id.company_id, values))
    #     if procurements:
    #         self.env['procurement.group'].run(procurements)
    #     return True


    def _action_launch_stock_rule_custom(self, previous_product_uom_qty=False):
        """
        Launch procurement group run method with required/custom fields genrated by a
        sale order line. procurement group will launch '_run_pull', '_run_buy' or '_run_manufacture'
        depending on the sale order line product rule.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        procurements = []
        for line in self:
            line = line.with_company(line.company_id)
            if line.state != 'sale' or not line.product_id.type in ('consu','product'):
                continue
            qty = line._get_qty_procurement(previous_product_uom_qty)
            if float_compare(qty, line.product_uom_qty, precision_digits=precision) == 0:
                continue

            group_id = line._get_procurement_group()
            if not group_id:
                group_id = self.env['procurement.group'].create(line._prepare_procurement_group_vals())
                line.order_id.procurement_group_id = group_id
            else:
                # In case the procurement group is already created and the order was
                # cancelled, we need to update certain values of the group.
                updated_vals = {}
                if group_id.partner_id != line.order_id.partner_shipping_id:
                    updated_vals.update({'partner_id': line.order_id.partner_shipping_id.id})
                if group_id.move_type != line.order_id.picking_policy:
                    updated_vals.update({'move_type': line.order_id.picking_policy})
                if updated_vals:
                    group_id.write(updated_vals)

            values = line._prepare_procurement_values_custom(group_id=group_id)
            product_qty = line.product_uom_qty - qty

            line_uom = line.product_uom
            quant_uom = line.product_id.uom_id
            product_qty, procurement_uom = line_uom._adjust_uom_quantities(product_qty, quant_uom)
            procurements.append(self.env['procurement.group'].Procurement(
                line.product_id, product_qty, procurement_uom,
                line.order_id.partner_shipping_id.property_stock_customer,
                line.name, line.order_id.name, line.order_id.company_id, values))
        if procurements:
            self.env['procurement.group'].run(procurements)

        # This next block is currently needed only because the scheduler trigger is done by picking confirmation rather than stock.move confirmation
        orders = self.mapped('order_id')
        for order in orders:
            pickings_to_confirm = order.picking_ids.filtered(lambda p: p.state not in ['cancel', 'done'])
            if pickings_to_confirm:
                # Trigger the Scheduler for Pickings
                pickings_to_confirm.action_confirm()
        return True



