# -*- coding: utf-8 -*-


from odoo import api, tools, models,fields, _
from odoo.tools import float_round
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

try:
    import requests
    import json
except ImportError:
    pass


class SoCompanyB(models.Model):
    _name = 'so.company.b'


    name = fields.Char('Name')
    partner_id = fields.Integer('Partner')
    pricelist_id = fields.Integer('pricelist_id')




    def action_create_data(self, a , b):
        query = """INSERT INTO so_company_b (name, partner_id, pricelist_id) VALUES ('test123', {}, {}) """.format(a, b)
        result = self.env.cr.execute(query)


    def action_multi_company_b(self):
        data = self.search([])
        sale_order = self.env['sale.order'].create({
            'partner_id': data.partner_id,
            'pricelist_id': data.pricelist_id
        })
