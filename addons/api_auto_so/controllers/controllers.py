from odoo import http
import json
from odoo.http import request
import requests
from odoo.exceptions import ValidationError

class AutoSO(http.Controller):

    @http.route(['/auto/sale/order'], type='json', auth="public", website=True, methods=['GET'])
    def auto_so(self, **kw):
        partner_id = request.jsonrequest.get('partner_id')
        pricelist_id = request.jsonrequest.get('pricelist_id')
        order_line = request.jsonrequest.get('order_line')
        code_unique = request.jsonrequest.get('code_unique')
        value = {
                    'partner_id': partner_id,
                    'pricelist_id': pricelist_id,
                    'code_unique': code_unique,
                    'order_line': order_line
                }
        # url = 'http://localhost:8069/auto/so/company_b'
        # res = requests.get(url, json=value)
        # print(44444, res.status_code)
        # if res.status_code != 200:
        #     return json.dumps({'error_fields': 'asdsadsad'})
        # else:
        #     return json.dumps(value)
