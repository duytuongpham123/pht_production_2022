{
    "name": "Set Cookie attributes from odoo config file",
    "summary": "Set Cookie attributes from odoo config file",    
    'category': 'Extra Tools',
    "description": """
        Secure Cookie
         
    """,
    
    "author": "Openinside",
    "license": "OPL-1",
    'website': "https://www.open-inside.com",
    "version": "14.0.14.0",
    "price" : 0,
    "currency": 'EUR',
    "installable": True,
    "depends": [
        'web'
    ],
    "data": [
        
    ],
    'images': [
    ],
    'odoo-apps' : True      
}

