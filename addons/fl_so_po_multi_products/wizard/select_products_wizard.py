# -*- coding: utf-8 -*-
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import models, fields, api


class SelectProducts(models.TransientModel):

    _name = 'select.products'
    _description = 'Select Products'

    product_ids = fields.Many2many('product.product', string='Products')
    flag_order = fields.Char('Flag Order')

    @api.model
    def create(self, vals):
        res = super(SelectProducts, self).create(vals)
        for index, product in enumerate(res.product_ids):
            product.stt_line = index + 1
        return res

    def select_products(self):
        if self.flag_order == 'so':
            order_id = self.env['sale.order'].browse(self._context.get('active_id', False))
            result = self.product_ids.mapped('id')
            product_test = self.env['product.product'].sudo().search([('id', 'in', result)], order="stt_line asc")
            for line in product_test:
                value = self.env['sale.order.line'].create({
                    'product_id': line.id,
                    'product_uom': line.uom_id.id,
                    'price_unit': line.price_custom,
                    'product_uom_qty': line.qty,
                    'order_id': order_id.id,
                })

        elif self.flag_order == 'po':
            order_id = self.env['purchase.order'].browse(self._context.get('active_id', False))
            for product in self.product_ids:
                self.env['purchase.order.line'].create({
                    'product_id': product.id,
                    'name': product.name,
                    'date_planned': order_id.date_planned or datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'product_uom': product.uom_id.id,
                    'price_unit': product.lst_price,
                    'product_qty': 1.0,
                    'display_type': False,
                    'order_id': order_id.id
                })
class ProductProduct(models.Model):
    _inherit = "product.product"

    stt_line = fields.Integer("STT LIne")
    qty = fields.Float("Qty")
    price_custom = fields.Float("Price")
