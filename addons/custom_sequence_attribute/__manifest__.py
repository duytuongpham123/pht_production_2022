# -*- encoding: utf-8 -*-
{
	"name": "Custom Sequence Attribute",
	"version": "14.0",
	"author": "Vo Thai Loc.",
	"website": "",
	"sequence": 5,
	"depends": ['product', 'sale', 'purchase', 'account'],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	This module allow to add products on opportunity and create quote with that. 
	""",
	"data": [
		'views/custom_sequence_attribute.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
