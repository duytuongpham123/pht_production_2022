# © 2017 Akretion (Alexis de Lattre <alexis.delattre@akretion.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


from odoo import _, api, models, fields


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    x_po = fields.Char('PO')

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    number_vote = fields.Char('Số phiếu')

    def action_create_invoice(self):
        res = super(PurchaseOrder, self).action_create_invoice()
        move = self.env['account.move'].search([
            ('id', '=', res['res_id'])
        ])
        if move:
            move.update({
                'number_vote': self.number_vote
            })
        return res

class AccountMove(models.Model):
    _inherit = 'account.move'

    number_vote = fields.Char('Số phiếu')