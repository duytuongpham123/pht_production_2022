odoo.define('import_hr_attandance.Import', function (require) {
  "use strict";
  var ListController = require('web.ListController');
  var rpc = require('web.rpc');
  
  ListController.include({
      renderButtons: function ($node) {
          this._super.apply(this, arguments);            
          if (this.modelName == 'hr.attendance') {
            var your_btn = this.$buttons.find('button.o_list_button_custom_import');
            your_btn.on('click', this.proxy('_action_import_attandance'));
        }
      },
      _action_import_attandance: function(event){
        var self = this;
        rpc.query({
            model: 'hr.attendance',
            method: 'action_call_import',
            args: [],
        }).then(function (res) {
            self.do_action(
                {
                    name: ("Import chấm công"),
                    type: 'ir.actions.act_window',
                    res_model: 'import.hr.attandance',
                    view_mode: 'form',
                    view_type: 'form',
                    target: 'new',
                    binding_type: 'action',
                    views: [[false, 'form']]
                },
            )
        });

    },
  });
});