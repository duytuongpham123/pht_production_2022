# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError

import os
from xlrd import open_workbook
import base64
import openpyxl
from io import BytesIO
from datetime import datetime, timedelta

class FormImport(models.TransientModel):
    _name = 'import.hr.attandance'

    file = fields.Binary("File")


    def mess_notification(self):
        view = self.env.ref('sh_message.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context['message'] = "Bạn đã import thành công"
        return {
            'name': _('Successfull'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'context': context,
            'nodestroy' : True,
        }


    @api.onchange('file')
    def _onchange_file(self):
        if self.file != False:
            return {'file': self.file}

    def button_import(self):
        result = self._onchange_file().get('file')
        if result:
            arrResult = []
            arrSort = []
            arr = []
            try:
                wb = openpyxl.load_workbook(filename=BytesIO(base64.b64decode(result)), read_only=True)
            except Exception as e:
                raise ValidationError(e)

            ws = wb.active

            for record in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                listRecord = list(record)
                check = listRecord.copy()
                del check[:3]
                number = len(check) - 2
                for i in range(len(check)):
                    if i % 2 == 0 and i < int(number):
                        str_record = str(record[2]).split(' 00:00:00')[0]
                        if check[i] != None:
                            hr_employee = self.env['hr.employee'].search([('code', '=', record[0])])

                            try:
                                hr_attandance = self.env['hr.attendance'].search([('check_in', '=', str_record + ' ' + str(check[i]))])
                            except Exception as e:
                                raise ValidationError(e)

                            if not hr_attandance and hr_employee:
                                # format date
                                date_check_in = str_record + ' ' + str(check[i])
                                date_check_out = str_record + ' ' + str(check[i+1])
                                # validate date
                                try:
                                    check_in = datetime.strptime(date_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)         
                                    check_out = datetime.strptime(date_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0) if check[i+1] != None else False
                                except Exception as e:
                                    raise ValidationError(e)

                                values = {
                                    'employee_id': hr_employee.id,
                                    'check_in': check_in,
                                    'check_out': check_out
                                }
                                arr.append(values)
                            else:
                                raise UserError('Nhân viên {} chưa tồn tại trong hệ thống'.format(record[1]))

            arrResult = [dict(t) for t in {tuple(d.items()) for d in arr}]
            arrSort = sorted(arrResult, key=lambda x: x['check_in'])
            try:
                result = self.env['hr.attendance'].create(arrSort)
                if result:
                    return True
                else:
                    raise UserError('Bạn vui lòng chọn đúng mẫu excel')
            except Exception as e:
                raise ValidationError(e)
            
