import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime, timedelta
from odoo.exceptions import UserError
import logging
from collections import defaultdict

# Create a custom logger
logger = logging.getLogger(__name__)

class HrAttendance(models.Model):
    _inherit = 'hr.attendance'

    # @api.model
    # def action_import_attendance(self):
    #     arr = []
    #     out = []
    #     # logger.info('3333333')
    #     # a = self.env['hr.employee'].search([])
    #     # logger.info(2222)
    #     sql = """
    #         select * from hr_attendance
    #     """
    #     self.env.cr.execute(sql)
    #     currentDateTime = datetime.now()
    #     date = currentDateTime.date()
    #     res_all = self.env.cr.fetchall()
    #     for i in self.env['list.place'].search([]):
    #         data = {
    #             'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIwMTc0NDEwNjgzMTc4MTA2ODgiLCJlbWFpbCI6ImR1eXR1b25ncGhhbTEyM0BnbWFpbC5jb20iLCJjbGllbnRfaWQiOiI0ODAzNmUxOWJiZmU3OTIyM2Q3YTAyMTAzYWYwMzI5NCIsInR5cGUiOiJyZWZyZXNoX3Rva2VuIiwiaWF0IjoxNjU5MDc4MDUwLCJleHAiOjE3MjIxNTAwNTB9.8S71cYgsnsp8zjrVi3vuM5qGlVTT-ke5dyvE_l_qVAM',
    #             'placeID': int(i.place_id),
    #             'from': int(datetime.strptime('1/{}/{} 00:00:00'.format(date.month, date.year), '%d/%m/%Y %H:%M:%S').timestamp() * 1000),
    #             'to': int(datetime.today().timestamp() * 1000),
    #             'devices': '<C21284M140, C21281M945>',
    #             'type': 0
    #         }
    #         list_person = requests.post('https://partner.hanet.ai/person/getCheckinByPlaceIdInTimestamp', data=data)
    #         data_person = list_person.json()['data']
    #         # logger.info(data_person)
    #         arr += data_person
    #     if arr:
    #         for i in arr:
    #             if i.get('aliasID') not in out:
    #                 out.append({
    #                     'personName': i.get('personName'),
    #                     'aliasID': i.get('aliasID'),
    #                     'date': i.get('date')
    #
    #                 })
    #         out_1 = [dict(t) for t in {tuple(d.items()) for d in out}]
    #         for i in arr:
    #             for j in out_1:
    #                 if i.get('aliasID') == j.get('aliasID') and i.get('date') == j.get('date'):
    #                     if 'checkinTime' in j:
    #                         j.update({
    #                             'checkinTime': str(j.get('checkinTime')) + "/" + str(i.get('checkinTime'))
    #                         })
    #                     else:
    #                         j.update({
    #                             'checkinTime': str(i.get('checkinTime'))
    #                         })
    #
    #         # logger.info(out_1)
    #         # get arr camera return
    #         value = {}
    #         for line in out_1:
    #             employee_id = self.env['hr.employee'].search([('alias_id', '=', line['aliasID'])])
    #             convert_arr = []
    #             if '/' in line['checkinTime']:
    #                 convert_arr = line['checkinTime'].split('/')
    #             # else:
    #             #     convert_arr = [int(line['checkinTime'])]
    #             if employee_id:
    #                 for index, child in enumerate(convert_arr):
    #                     num = index + 1
    #                     if num % 2 == 0:
    #                         value['check_out'] = datetime.fromtimestamp(int(child) / 1000)
    #                     else:
    #                         value = {
    #                             'employee_id': employee_id.id,
    #                             'check_in': datetime.fromtimestamp(int(child) / 1000),
    #                         }
    #                     if 'check_out' in value and value:
    #                         last_attendance_before_check_in = self.env['hr.attendance'].search([
    #                             ('employee_id', '=', value['employee_id']),
    #                             ('check_in', '=', value['check_in'])
    #                         ], order='check_in asc')
    #                         if not last_attendance_before_check_in:
    #                             self.env['hr.attendance'].sudo().create(value)
    #                         value = {}
    #                     if num == len(convert_arr) and num %2 != 0:
    #                         last_attendance_before_check_in = self.env['hr.attendance'].search([
    #                             ('employee_id', '=', value['employee_id']),
    #                             ('check_in', '=', value['check_in'])
    #                         ], order='check_in asc')
    #                         if not last_attendance_before_check_in:
    #                             self.env['hr.attendance'].sudo().create(value)
        #

class AttendanceDateWizard(models.TransientModel):
    _name = 'attendance.date.wizard'

    date_from = fields.Date(string='Từ ngày')
    date_to = fields.Date(string='Đến ngày')
    place_id = fields.Many2one('list.place', string='Kho')
    employee_ids = fields.Many2many('hr.employee', string='Nhân viên', domain="[('place_id', '=', place_id)]")

    def btn_ok(self):
        if not self.date_from and not self.date_to:
            raise UserError('Bạn vui lòng chọn ngày')
        date_fr = datetime.strptime(str(self.date_from), '%Y-%m-%d') - timedelta(days=1)
        date_t = datetime.strptime(str(self.date_to), '%Y-%m-%d') - timedelta(days=1)
        date_from = str(date_fr.date()).split('-')
        date_to = str(date_t.date()).split('-')
        multi_alias = ','.join([str(int(x)) for x in self.employee_ids.mapped('alias_id')])
        data = {
            # 'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIwMTc0NDEwNjgzMTc4MTA2ODgiLCJlbWFpbCI6ImR1eXR1b25ncGhhbTEyM0BnbWFpbC5jb20iLCJjbGllbnRfaWQiOiI0ODAzNmUxOWJiZmU3OTIyM2Q3YTAyMTAzYWYwMzI5NCIsInR5cGUiOiJyZWZyZXNoX3Rva2VuIiwiaWF0IjoxNjU5MDc4MDUwLCJleHAiOjE3MjIxNTAwNTB9.8S71cYgsnsp8zjrVi3vuM5qGlVTT-ke5dyvE_l_qVAM",
            'placeID': int(self.place_id.place_id),
            'from': int(
                datetime.strptime('{}/{}/{} 17:00:00'.format(int(date_from[2]), date_from[1], date_from[0]), '%d/%m/%Y %H:%M:%S').timestamp() * 1000),
            'to': int(
                datetime.strptime('{}/{}/{} 17:00:00'.format(date_to[2], date_to[1], date_to[0]), '%d/%m/%Y %H:%M:%S').timestamp() * 1000),
            'devices': '<C21284M140, C21281M945>',
            'aliasIDs': multi_alias,
            'type': 0
        }
        print(data)
        list_person = requests.post('https://partner.hanet.ai/person/getCheckinByPlaceIdInTimestamp', data=data)
        data_person = list_person.json()['data']
        if not data_person:
            raise UserError(_('Hiện tại chưa có dữ liệu chấm công'))
        roles = defaultdict(list)
        arr_new = []
        for i in data_person:
            bwl = self.env['booking.worker.line'].search([
                ('start_date', '<=', i['date']),
                ('end_date', '>=', i['date'])
            ]).filtered(lambda x: x.types_resource_calendar.code == 'night_shift' and x.employee_id.alias_id == i['aliasID'])
            if bwl:
                for t in bwl:
                    array = [str(t.start_date), str(t.end_date)]
                    if i['date'] in array:
                        key = (i['personName'], i['aliasID'], ", ".join(array))
                        str_date = str(datetime.fromtimestamp(int(i['checkinTime']) / 1000))
                        check_in = datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                        fl_check = check_in.hour + check_in.minute / 60.0
                        if i['date'] == array[0] and fl_check >= 12:
                            roles[key].append({
                                'checkinTime': datetime.fromtimestamp(int(i['checkinTime']) / 1000)
                            })
                        if i['date'] == array[1] and i['checkinTime'] <= 12:
                            roles[key].append({
                                'checkinTime': datetime.fromtimestamp(int(i['checkinTime']) / 1000)
                            })
            else:
                key = (i['personName'], i['aliasID'], i['date'])
                roles[key].append({
                    'checkinTime': datetime.fromtimestamp(int(i['checkinTime']) / 1000)
                })
        for (personName, aliasID, date), data_check in roles.items():
            arr_ce = []
            for item in data_check:
                arr_ce.append(item['checkinTime'])
            arr_new.append({
                'personName': personName,
                'aliasID': aliasID,
                'date': date,
                'checkinTime': arr_ce
            })
        value = {}
        for line in arr_new:
            employee_id = self.env['hr.employee'].search([
                ('alias_id', '=', line['aliasID']),
                ('name', '=', line['personName'])
            ])
            print(3333, employee_id)
            convert_arr = line['checkinTime']
            # else:
            #     convert_arr = [int(line['checkinTime'])]
            if employee_id:
                for index, child in enumerate(convert_arr):
                    num = index + 1
                    if num % 2 == 0:
                        value['check_out'] = child
                    else:
                        value = {
                            'employee_id': employee_id.id,
                            'check_in': child,
                        }
                    if 'check_out' in value and value:
                        last_attendance_before_check_in = self.env['hr.attendance'].search([
                            ('employee_id', '=', value['employee_id']),
                            ('check_in', '=', value['check_in'])
                        ], order='check_in asc')
                        if not last_attendance_before_check_in:
                            self.env['hr.attendance'].sudo().create(value)
                        value = {}
                    if num == len(convert_arr) and num % 2 != 0:
                        last_attendance_before_check_in = self.env['hr.attendance'].search([
                            ('employee_id', '=', value['employee_id']),
                            ('check_in', '=', value['check_in'])
                        ], order='check_in asc')
                        if not last_attendance_before_check_in:
                            self.env['hr.attendance'].sudo().create(value)

