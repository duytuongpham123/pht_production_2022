# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
import base64
import requests, json
import subprocess
import sys
from odoo.exceptions import UserError

class ListPlace(models.Model):
    _name = 'list.place'

    user_id = fields.Float('User')
    name = fields.Char('Name')
    permission = fields.Char('permission')
    place_id = fields.Float('place_id')
    address = fields.Char('Address')

    @api.model
    def create(self, vals_list):
        res = super(ListPlace, self).create(vals_list)
        data = {
            'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
            'name': res.name,
            'address': res.address
        }
        create_place = requests.post('https://partner.hanet.ai/place/addPlace', data=data)
        if create_place.status_code == 200:
            if create_place.json()['returnCode'] == 1:
                res.place_id = create_place.json()['data']['id']
                res.user_id = create_place.json()['data']['userID']
        return res

    def unlink(self):
        data_list = {'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token')}
        data_remove = {
            'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
            'placeID': int(self.place_id)
        }
        remove_place = requests.post('https://partner.hanet.ai/place/removePlace', data=data_remove)
        return super(ListPlace, self).unlink()
    
    def write(self, vals):
        res = super(ListPlace, self).write(vals)
        data_list = {'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token')}
        data_update = {
            'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
            'placeID': int(self.place_id),
            'name': self.name,
            'address': self.address
        }
        update_place = requests.post('https://partner.hanet.ai/place/updatePlace', data=data_update)
        return res









