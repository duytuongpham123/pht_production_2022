from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError
import requests, json

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    code = fields.Char('Code')
    token = fields.Char('Token')
    client_id = fields.Char('Client Id')
    client_secret = fields.Char('Client Secret')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            code=self.env['ir.config_parameter'].sudo().get_param(
                'camera_api.code'),
            token=self.env['ir.config_parameter'].sudo().get_param(
                'camera_api.token'),
            client_id=self.env['ir.config_parameter'].sudo().get_param(
                'camera_api.client_id'),
            client_secret=self.env['ir.config_parameter'].sudo().get_param(
                'camera_api.client_secret'),
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()
        code = self.code
        token = self.token
        client_id = self.client_id
        client_secret = self.client_secret
        param.set_param('camera_api.code', code)
        param.set_param('camera_api.token', token)
        param.set_param('camera_api.client_id', client_id)
        param.set_param('camera_api.client_secret', client_secret)

    def get_data_oauth(self):
        authorize_url = "https://oauth.hanet.com/oauth2/authorize"
        token_url = "https://oauth.hanet.com/token"
        callback_uri = "https://developers.hanet.ai"
        client_id = self.env['ir.config_parameter'].sudo().get_param('camera_api.client_id')
        client_secret = self.env['ir.config_parameter'].sudo().get_param('camera_api.client_secret')
        authorization_redirect_url = authorize_url + '?response_type=code&client_id=' + client_id + '&redirect_uri=' + callback_uri + '&scope=full'
        authorization_code = self.env['ir.config_parameter'].sudo().get_param('camera_api.code')
        # step I, J - turn the authorization code into a access token, etc
        data = {'grant_type': 'authorization_code', 'code': authorization_code, 'redirect_uri': callback_uri, 'client_id': client_id, 'client_secret': client_secret}
        access_token_response = requests.post(token_url, data=data)
        if access_token_response.status_code == 200:
            tokens = json.loads(access_token_response.text)
            param = self.env['ir.config_parameter'].sudo()
            param.set_param('camera_api.token', tokens['refresh_token'])
        else:
            return False

    def go_to_url_oauth(self):
        client_id = self.env['ir.config_parameter'].sudo().get_param('camera_api.client_id')
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        oauth_url = 'https://oauth.hanet.com/oauth2/authorize'
        url = '{}?response_type=code&client_id={}&redirect_uri={}&scope=full'.format(oauth_url, client_id, base_url)
        return {
            'name': "go_to_url_oauth",
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new'
        }











