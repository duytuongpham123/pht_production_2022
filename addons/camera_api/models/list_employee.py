from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from odoo.exceptions import UserError
import logging

# Create a custom logger
logger = logging.getLogger(__name__)

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    alias_id = fields.Float('alias_id')
    person_id = fields.Float('Person')
    place_id = fields.Many2one('list.place')
    place = fields.Float('Place')
    check_api = fields.Boolean('Check Api')

    _sql_constraints = [('alias_id','UNIQUE (alias_id)', 'aliasId all already exists')]

    @api.model
    def refesh_list_employee(self):
        arr = []
        data = {
            'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
            'placeID': int(self.env['list.place'].search([])[-1].place_id)
        }
        list_person = requests.post('https://partner.hanet.ai/person/getListByPlace', data=data)
        data_person = list_person.json()['data']
        for line in data_person:
            value = {
                'alias_id': line['aliasID'],
                'name': line['name'],
                'person_id': line['personID'],
                'image_1920': base64.b64encode(requests.get(line['avatar']).content),
                'place': self.env['list.place'].search([])[-1].place_id
            }
            arr.append(value)
        self.env['hr.employee'].create(arr)

    @api.model
    def create_list_employee(self, employee_id):
        employee = self.env['hr.employee'].sudo().search([('id', '=', int(employee_id))])
        if employee.check_api == False:
            value = {
                'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
                'name': employee.name,
                'aliasID': employee.alias_id,
                'title': employee.job_title,
                'type': 0,
                'placeID': int(employee.place_id.place_id),
                'file': employee.image_1920.decode()
            }
            return [value]
        else:
            return []

    @api.model
    def create_all_employee(self):
        employee = self.env['hr.employee'].sudo().search([('check_api', '=', False)]).filtered(lambda x: x.image_1920)
        logger.info(employee)
        array = []
        for line in employee:
            value = {
                'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
                'name': line.name,
                'aliasID': line.alias_id,
                'title': line.job_title,
                'type': 0,
                'placeID': int(line.place_id.place_id),
                'file': line.image_1920.decode()
            }
            array.append(value)
        return array

    @api.model
    def create_list_employee1(self, employee):
        mess = ""
        for line in employee:
            employee_id = self.env['hr.employee'].search([('alias_id', '=', line['alias'])])
            mess += "- Error at {} {} \n".format(employee_id.name, line['returnMessage'])
        if mess:
            raise UserError(_(mess))

    @api.model
    def del_data_fail(self):
        self.env['data.employee.fail'].search([]).unlink()

    @api.model
    def data_employee_fail(self, arr):
        arr1 = []
        self.del_data_fail()
        for line in arr:
            employee_id = self.env['hr.employee'].sudo().search([('alias_id', '=', line.get('aliasID'))])
            value = {
                'employee_id': employee_id.id,
                'alias_id': line['aliasID'],
                'message': line['errorMessage']
            }
            arr1.append(value)
        self.env['data.employee.fail'].create(arr1)

    @api.model
    def update_check_api_camera(self, aliasID):
        employee_id = self.env['hr.employee'].search([('alias_id', '=', float(aliasID))])
        if employee_id:
            employee_id.check_api = True

    def get_alias_employee(self):
        data = {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIwMTc0NDEwNjgzMTc4MTA2ODgiLCJlbWFpbCI6ImR1eXR1b25ncGhhbTEyM0BnbWFpbC5jb20iLCJjbGllbnRfaWQiOiI0ODAzNmUxOWJiZmU3OTIyM2Q3YTAyMTAzYWYwMzI5NCIsInR5cGUiOiJyZWZyZXNoX3Rva2VuIiwiaWF0IjoxNjU5MDc4MDUwLCJleHAiOjE3MjIxNTAwNTB9.8S71cYgsnsp8zjrVi3vuM5qGlVTT-ke5dyvE_l_qVAM",
            "placeID": "7588"
        }
        list_person = requests.post('https://partner.hanet.ai/person/get-list-by-place', data=data)
        data_person = list_person.json()['data']
        test = self.env['hr.employee'].search([])
        for index, t in enumerate(test):
            t.alias_id = index + 88888899

        # for item in data_person:
        #     if item['aliasID']:
        #         employee_id = self.env['hr.employee'].search([]).filtered(lambda x: x.name.lower() == item['name'].lower() and x.alias_id and not x.check_api and x.place_id.place_id == 7588)
        #         # print(employee_id)
        #         if employee_id:
        #             employee_id.check_api = True
        #             employee_id.alias_id = item['aliasID']


class DataEmployeeFail(models.Model):
    _name = 'data.employee.fail'

    employee_id = fields.Many2one('hr.employee')
    alias_id = fields.Float('Alias')
    message = fields.Text('Lỗi')





