# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import http, _
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
from odoo.http import request
from datetime import datetime, date, timedelta
import math

class BranchList(http.Controller):

    @http.route('/test', type='json', auth="public", methods=['POST'], website=True)
    def test(self):
        return {'a': 1}

