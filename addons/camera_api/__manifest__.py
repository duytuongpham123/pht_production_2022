# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Camera Api',
    'version': '15.0.0',
    'category': 'Sales',
    'summary': 'PDT',
    "description": """
       Pham Duy Tuong
    """,
    'author': 'PDT',
    "price": 149.00,
    "currency": 'EUR',
    'depends': ['base', 'contacts', 'web', 'hr', 'hr_attendance', 'mrp_subcontracting'],
    'data': [
        'views/list_place.xml',
        # 'views/res_setting.xml',
        'views/hr_employee.xml',
        # 'security/branch_security.xml',
        'security/ir.model.access.csv',
        'views/data_employee_fail.xml',
        'views/template.xml',
        # 'views/visit_branch.xml',
    ],
    'demo': [],
    'assets': {
        'web.assets_backend': [
            '/camera_api/static/src/js/list_place.js',
            '/camera_api/static/src/js/hr_employee.js',
            '/camera_api/static/src/js/hr_attendance.js',
            '/camera_api/static/src/css/style.css',
            '/camera_api/static/src/js/form_create_employee.js',
        ],
        'web.assets_qweb': [
            '/camera_api/static/src/xml/list_place.xml',
            '/camera_api/static/src/xml/hr_employee.xml',
            '/camera_api/static/src/xml/hr_attendance.xml',
        ],
    },
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
