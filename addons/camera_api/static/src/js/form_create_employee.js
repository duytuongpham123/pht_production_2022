odoo.define('camera_api.CreateFormEmployee', function (require) {
    'use strict';

    const core = require('web.core');
    const FormController = require('web.FormController');
    const qweb = core.qweb;
    const _t = core._t;
    var ajax = require('web.ajax');
    var rpc = require('web.rpc');

    FormController.include({
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
        },
        renderButtons: function ($node) {
            this._super.apply(this, arguments);
            if (this.modelName == 'hr.employee') {
                var your_btn_2 = this.$buttons.find('button.create_employee_v1');
                your_btn_2.on('click', this.proxy('_action_create_employee'));
            }
        },
        _action_create_employee: function(event){
            var self = this;
            const employee_id = self.initialState.res_id;
            var currentLocation = window.location.hash
            const convertLocation = currentLocation.split('&');
            rpc.query({
                model: 'hr.employee',
                method: 'create_list_employee',
                args: [convertLocation[0].replace(/[^0-9]/g, '')],
                }).then(function (res) {
                    console.log(res);
                    if (res.length > 0){
                        const arr = [];
                        const allPromise = [];

                        const createLoadingElelement = () => {
                            var blurBackground = document.createElement('div');
                            blurBackground.classList.add('loading-background');
                            var loadingElement = document.createElement('div');

                            loadingElement.classList.add('lds-ring');
                            blurBackground.appendChild(loadingElement);
                            loadingElement.appendChild(document.createElement('div'))
                            document.body.appendChild(blurBackground);
                        }

                        const removeLoadingElement = () => {
                            const element = document.querySelector('.loading-background');
                            if (element) element.remove();
                        }
                        createLoadingElelement();

                        $.each(res, function(i, item) {
                            let promiseSubmitForm = new Promise(resolve => {
                                let formData = new FormData()
                                formData.append("token", item.token)
                                formData.append("name", item.name)
                                formData.append("aliasID", item.aliasID)
                                formData.append("title", item.title)
                                formData.append("type", item.type)
                                formData.append("placeID", item.placeID)
                                let base64=item.file;
                                let buffer=Uint8Array.from(atob(base64), c => c.charCodeAt(0));
                                let blob=new Blob([buffer], { type: "image/jpg" });
                                let url=URL.createObjectURL(blob);
                                fetch(url)
                                  .then(res => res.blob())
                                  .then(blob => {
                                    const file = new File([blob], "File name",{ type: "image/jpg" })
                                    formData.append('file', file, `${item.name}.jpg`);
                                    fetch('https://partner.hanet.ai/person/register', {
                                       method: "POST",
                                       body: formData,
                                    }).then(res=>res.json()).then(respone=> {
                                        console.log(respone)
                                        if (respone.returnMessage === 'SUCCESS'){
                                            const alias_id = respone.data.aliasID
                                            rpc.query({
                                                model: 'hr.employee',
                                                method: 'update_check_api_camera',
                                                args: [alias_id],
                                            }).then(function (res) {
                                                resolve(true);
                                            });
                                        }
                                        else{
                                            arr.push({'alias': item.aliasID, 'returnMessage': respone.returnMessage});
                                            resolve(true);
                                        }
                                    });
                                  })
                            })
                            allPromise.push(promiseSubmitForm);
                        });
                        Promise.all(allPromise).then((values) => {
                          rpc.query({
                                model: 'hr.employee',
                                method: 'create_list_employee1',
                                args: [arr],
                            }).then(function (res) {
                                removeLoadingElement();
                            }).catch((error) => {
                                removeLoadingElement();
                            });
                        });

                    }
                    if(res.length == 0){
                        alert('Nhân viên này đã được đồng bộ')
                    }


                });
                console.log(222222);
//          });
        },
    });
});