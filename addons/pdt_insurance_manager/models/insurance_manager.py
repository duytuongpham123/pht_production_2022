from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class InsuranceManager(models.Model):
    _name = "hr.insurance"

    employee_id = fields.Many2one('hr.employee')
    social_insurance_no = fields.Char('Social Insurance No')
    date_of_issue = fields.Date('Date of Issue')
    status = fields.Selection([
        ('hr', 'Hr'),
        ('employee', 'Employee'),
        ('insurance', 'Insurance'),
    ], default='hr')

    handover_date = fields.Date('Handover Date')
    company_care = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No'),
    ], default='yes', string='Not under the company’s care')

    reason = fields.Text('Reason')
    health_insurance_no = fields.Char('Health Insurance No')
    social_insurance_office = fields.Char('Social Insurance Office')
    company_code = fields.Char('Company Code')
    household_code = fields.Char('Household Code')


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    hr_insurance_ids = fields.One2many('hr.insurance', 'employee_id')






