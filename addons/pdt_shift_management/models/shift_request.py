from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta

class ShiftRequest(models.Model):
    _name = "shift.request"

    name = fields.Char("Number")
    shift_type_id = fields.Many2one('shift.type', 'Shift Type', required=True)
    employee_id = fields.Many2one('hr.employee', 'Employee', required=True)
    employee_manager_id = fields.Many2one('hr.employee', 'Employee Manager')
    start_date = fields.Date('Start Date', required=True)
    shift_responsible_id = fields.Many2one('hr.employee', 'Shift Responsible')
    department_id = fields.Many2one('hr.department', 'Department')
    end_date = fields.Date('End Date', required=True)
    company_id = fields.Many2one('res.company', 'Company')
    description = fields.Text('Description')
    internal_note = fields.Text('Internal Note')
    state = fields.Selection([
        ('new', 'New'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('cancel', 'Cancel'),
    ], default='new')
    shift_request_line_ids = fields.One2many('shift.request.line', 'shift_request_id')


    @api.onchange('employee_id')
    def _onchange_shift_responsible(self):
        if self.employee_id:
            self.employee_manager_id = self.employee_id.parent_id
            self.department_id = self.employee_id.department_id
            self.company_id = self.employee_id.company_id

    @api.model
    def create(self, vals):
        vals['state'] = 'new'
        if len(self.search([]).mapped('id')) == 0:
            zero_filled_number = "SHIFT/REQ/0000001"
        else:
            check = self.search([], order="id asc")[-1]
            number_str = str(int(check.name.split('/')[2]) + 1)
            zero_filled_number = "SHIFT/REQ/" + number_str.zfill(7)
        vals['name'] = zero_filled_number
        res = super(ShiftRequest, self).create(vals)
        # get_last_data = self.env['shift.request'].search([('employee_id', '=', res.employee_id.id)], order="id asc")[-1]
        # if self.env['shift.request'].search_count([('employee_id', '=', res.employee_id.id)]) > 1:
        #     check_date = get_last_data.end_date - res.start_date
        #     print(111,get_last_data.end_date)
        #     print(222, res.start_date)
        #     if check_date.days > 0:
        #         raise UserError('Bạn bị lỗi ở ngày kết thúc {} >  ngày bắt đầu {}'.format(get_last_data.end_date,
        #                                                                                   res.start_date))
        return res

    def confirm_request(self):
        if self.state == 'new':
            self.state = 'confirmed'
            group = self.env['res.groups'].search(
                [('id', '=', self.env.ref('pdt_shift_management.group_pdt_manager_shift').id)])
            group.write({'users': [(4, self.employee_manager_id.user_id.id)]})


    def approve_request(self):
        if self.state == 'confirmed':
            if self.employee_manager_id.user_id.id == self.env.uid:
                self.state = 'approved'
            else:
                raise UserError('Bạn không có quyền duyệt')


    def cancel_request(self):
        if self.state:
            self.state = 'cancel'

    def reset_to_draft(self):
        if self.state:
            self.state = 'new'

    def generate_request_line(self):
        self.delete_request()
        if self.shift_type_id.id == self.env.ref('pdt_shift_management.morning_shift').id:
            date = self.end_date - self.start_date
            total_date = int(date.days) + 1
            for i in range(total_date):
                get_date = datetime.strptime(str(self.start_date), '%Y-%m-%d') + timedelta(days=i)
                value = {
                    'name': 'Ca {}'.format(i+1),
                    'start_date': get_date.date(),
                    'end_date': get_date.date(),
                }
                self.shift_request_line_ids = [(0, 0, value)]

        if self.shift_type_id.id == self.env.ref('pdt_shift_management.evening_shift').id:
            date = self.end_date - self.start_date
            if int(date.days) % 2 != 0:
                total_date = int(date.days) + 1
                for i in range(total_date):
                    get_start_date = datetime.strptime(str(self.start_date), '%Y-%m-%d') + timedelta(days=i)
                    get_stop_date = get_start_date + timedelta(days=1)
                    value = {
                        'name': 'Ca {}'.format(i + 1),
                        'start_date': get_start_date.date(),
                        'end_date': get_stop_date.date(),
                    }
                    self.shift_request_line_ids = [(0, 0, value)]
            else:
                raise UserError("Vui lòng chọn lại ngày kết thúc")

    def delete_request(self):
        if self.shift_request_line_ids:
            self.shift_request_line_ids.unlink()

class ShiftRequestLine(models.Model):
    _name = 'shift.request.line'

    name = fields.Char("Name")
    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    shift_request_id = fields.Many2one('shift.request')






