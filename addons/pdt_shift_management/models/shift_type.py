from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta

class ShiftType(models.Model):
    _name = "shift.type"

    name = fields.Char("Name")
    code = fields.Char("Code")
    start_time = fields.Float("Start Time")
    end_time = fields.Float("End Time")
    internal_note = fields.Text('Internal Note')
    time_off = fields.Float('Time off')
    time_off_from = fields.Float('Hour time off from')
    time_off_to = fields.Float('Hour time off to')

    @api.onchange('time_off_from', 'time_off_to')
    def get_time_off(self):
        if self.time_off_from and self.time_off_to:
            self.time_off = self.time_off_to - self.time_off_from







