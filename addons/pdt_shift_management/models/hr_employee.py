from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    def call_shift_request(self):
        action = self.env["ir.actions.act_window"]._for_xml_id(
            "pdt_shift_management.view_my_shift_request")
        action['domain'] = [('employee_id', '=', self.id)]
        return action






