# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from . import shift_request
from . import shift_type
from . import hr_employee

