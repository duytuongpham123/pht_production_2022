from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class Allocations(models.Model):
    _inherit = "hr.leave.allocation"

    allocation_period = fields.Char("Kì cấp phát")
    day_balance = fields.Float("Số dư ngày nghỉ trong tháng", compute="_compute_leaves", stored=True)

    @api.depends('employee_id', 'holiday_status_id')
    def _compute_leaves(self):
        res = super(Allocations, self)._compute_leaves()
        for allocation in self:
            leave_type = allocation.holiday_status_id.with_context(employee_id=allocation.employee_id.id)
            allocation.day_balance = leave_type.max_leaves - leave_type.leaves_taken
        return res

    def action_create_allocations(self):
        arr_allocations = []
        year = datetime.datetime.today().strftime('%Y')
        month = datetime.datetime.today().strftime('%m')
        current_date = month + '/' + year
        for line in self.env['hr.contract'].search([('state', '=', 'open')]):
            value = {
                'name': 'Cấp phát ngày nghỉ phép cho nhân viên {}'.format(line.employee_id.name),
                'holiday_status_id': 1,
                'allocation_type': 'regular',
                'number_of_days_display': 1,
                'holiday_type': 'employee',
                'employee_id': line.employee_id.id,
                'allocation_period': current_date,
                'state': 'validate'
            }
            arr_allocations.append(value)
        try:
            allocations = self.env['hr.leave.allocation'].create(arr_allocations)
        except Exception as e:
            raise ValidationError(e)



