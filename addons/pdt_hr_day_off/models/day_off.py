# Copyright 2018-2020 Akretion France
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime

class DayOffYear(models.Model):
    _name = "day.off.year"

    day_fixed_id = fields.Many2one('day.fixed.every.year')
    code = fields.Char("Mã")
    name = fields.Char("Tên ngày nghỉ", required=True)
    date = fields.Date('Ngày', required=True)
    rank = fields.Char("Thứ trong tuần")
    year = fields.Char("Năm")

    @api.onchange('day_fixed_id')
    def _onchange_day_off(self):
        if self.day_fixed_id:
            self.name = self.day_fixed_id.name
            self.code = self.day_fixed_id.code
            self.date = self.day_fixed_id.date
            #format date
            if self.date:
                date = "{}".format(self.date)
                day_str = datetime.datetime.strptime(date, '%Y-%m-%d')
                day_of_weeken = day_str.strftime('%A')
                year = day_str.strftime('%Y')
                self.rank = day_of_weeken
                self.year = year


class DayFixedEveryYear(models.Model):
    _name = "day.fixed.every.year"

    code = fields.Char("Mã")
    name = fields.Char("Tên ngày nghỉ", required=True)
    date = fields.Date('Ngày', required=True)
    permanent = fields.Boolean("Cố định")


    def action_update_day_off_check(self):
        day_off = self.search([])
        # auto update date every year
        for line in day_off:
            line.update({
                'date': line.date + relativedelta(years=1)
            })

            #update date of year
            day_off_year = self.env['day.off.year'].search([('day_fixed_id', '=', line.id)])
            for line_day in day_off_year:
                if day_off_year:
                    conver_date = line_day.date + relativedelta(years=1)
                    date = "{}".format(conver_date)
                    day_str = datetime.datetime.strptime(date, '%Y-%m-%d')
                    day_of_weeken = day_str.strftime('%A')
                    year = day_str.strftime('%Y')
                    line_day.update({
                        'date': day_str,
                        'rank': day_of_weeken,
                        'year': year
                    })



