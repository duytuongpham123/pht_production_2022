from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class HrLeave(models.Model):
    _inherit = "hr.leave"

    session_day = fields.Char("Số buổi", compute="_compute_number_of_days", store=True)
    hours = fields.Char("Số giờ", compute="_compute_number_of_days", store=True)

    @api.depends('date_from', 'date_to', 'employee_id')
    def _compute_number_of_days(self):
        res = super(HrLeave, self)._compute_number_of_days()
        for holiday in self:
            if holiday.date_from and holiday.date_to:
                holiday.session_day = holiday.number_of_days * 2
                holiday.hours = holiday.number_of_days * 8
            else:
                holiday.session_day = 0
                holiday.hours = 0
        return res

    def test(self):
        print(1111)



