odoo.define('pdt_work_entry.attendance_work_entry_custom', function (require) {
  "use strict";
  var ListController = require('web.ListController');
  var rpc = require('web.rpc');
  
  ListController.include({
      renderButtons: function ($node) {
          this._super.apply(this, arguments);            
          if (this.modelName == 'hr.work.entry') {
            var your_btn = this.$buttons.find('button.o_list_button_custom_import');
            your_btn.on('click', this.proxy('_action_generate_attandance'));
        }
      },
      _action_generate_attandance: function(event){
        var self = this;
        var answer = window.confirm("Bạn có chắc muốn tạo Work Entry?");
        if (answer) {
            rpc.query({
            model: 'hr.work.entry',
            method: 'generate_attendance_to_work_entries',
            args: [],
            }).then(function (res) {
                location.reload();
            });
        }
    },
  });
});