from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class HrLeave(models.Model):
    _inherit = "hr.leave"

    work_entries = fields.Many2one('hr.work.entry', string='Work Entries')