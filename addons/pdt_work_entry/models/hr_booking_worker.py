from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class HrBookingWorker(models.Model):
    _name = "booking.worker"

    name = fields.Char('Name')
    date_from = fields.Date('Date from')
    date_to = fields.Date('Date to')
    line_ids = fields.One2many('booking.worker.line', 'booking_worker_id', string="Line Booking Worker")

class HrBookingWorkerLine(models.Model):
    _name = "booking.worker.line"

    booking_worker_id = fields.Many2one('booking.worker')
    employee_id = fields.Many2one('hr.employee', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    type_booking = fields.Selection([
        ('full_day', 'Cả ngày'),
        ('sessions', 'Theo buổi'),
        ('shift', 'Theo ca')
    ], required=True)
    types_resource_calendar = fields.Many2one('hr.type.resource.calendar', 'Resource Calendar')
    resource_calendar = fields.Many2one('resource.calendar', 'Resource Calendar')

    @api.constrains('start_date', 'employee_id', 'booking_worker_id')
    def set_unique_booking_worker(self):
        for rec in self:
            check = self.search_count([
                ('start_date', '=', rec.start_date),
                ('booking_worker_id', '=', rec.booking_worker_id.id),
                ('employee_id', '=', rec.employee_id.id),
            ])
            if check > 1:
                raise UserError('Bạn đã chọn trùng ngày {}'.format(rec.start_date))




