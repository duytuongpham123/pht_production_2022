from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class TypeResourceCalendar(models.Model):
    _name = "hr.type.resource.calendar"

    name = fields.Char('Name')
    code = fields.Char('Code')
    type_booking = fields.Selection([
        ('sessions', 'Theo buổi'),
        ('shift', 'Theo ca')
    ], required=True)
    shift_type_id = fields.Many2one('shift.type', 'Time')