# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from . import res_setting
from . import hr_contract
# from . import hr_work_entry
from . import hr_employee
from . import hr_leave
from . import hr_work_entry_new
from . import resource_calendar
from . import hr_booking_worker
from . import hr_type_resource_calendar