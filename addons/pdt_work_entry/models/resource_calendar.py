from odoo import api, models, fields

class ResourceCalendarInherit(models.Model):
    _inherit = "resource.calendar"

    type_date = fields.Selection([
        ('in_day', 'Cả ngày'),
        ('sessions', 'Theo buổi')
    ], default='in_day')
