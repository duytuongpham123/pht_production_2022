from odoo import api, models, fields, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from collections import defaultdict
from operator import itemgetter
from itertools import zip_longest
import calendar
from dateutil.relativedelta import relativedelta
import logging
log = logging.getLogger("my-logger")

class HrWorkEntry(models.Model):
    _inherit = "hr.work.entry"
    duration = fields.Float(string="Period")

    @api.onchange('date_stop', 'date_start')
    def _compute_duration(self):
        for work_entry in self:
            work_entry.duration = work_entry._get_duration(work_entry.date_start, work_entry.date_stop)

    def code_att_work_type(self):
        models_att_w_type = self.env['hr.work.entry.type']
        value = {
            'type_att_nomal': models_att_w_type.search([('code', '=', 'WORK100')]),
            'type_att_come_late': models_att_w_type.search([('code', '=', 'ATTCOMELATE')]),
            'type_att_leave_early': models_att_w_type.search([('code', '=', 'ATTLEAVEEARLY')]),
            'type_att_late_early': models_att_w_type.search([('code', '=', 'ATTCLLE')]),
            'type_extra_hours': models_att_w_type.search([('code', '=', 'EXH')]),
            'type_extra_hours_come_late': models_att_w_type.search([('code', '=', '	EXHCL')]),
            'type_extra_hours_leave_early': models_att_w_type.search([('code', '=', 'EXHLE')]),
            'type_extra_hours_late_early': models_att_w_type.search([('code', '=', 'EXHCLLE')]),
        }
        return value

    def setting_time(self):
        param_early_time = self.env['ir.config_parameter'].sudo().get_param('pdt_work_entry.early_time')
        param_late_time = self.env['ir.config_parameter'].sudo().get_param('pdt_work_entry.late_time')
        # Work Entry
        work_entry = self.env['hr.work.entry']
        type_att_work_entry = self.code_att_work_type()
        value = {
            'param_early_time': param_early_time,
            'param_late_time': param_late_time,
            'work_entry': work_entry,
            'type_att_work_entry': type_att_work_entry,
        }
        return value

    def generate_attendance_work_entry_morning(self, work_time, arr_line):
        setting_time = self.setting_time()
        work_entry = setting_time.get('work_entry')
        type_att_work_entry = setting_time.get('type_att_work_entry')
        if work_time.day_period == 'morning':
            flag = 'morning'
            conver_arr = arr_line.get('check_in_hour').split("/")
            time_late_setting = work_time.hour_from + float(setting_time.get('param_late_time')) / 60
            time_early_setting = work_time.hour_to - float(setting_time.get('param_early_time')) / 60

            # Attendance(Normal)
            if work_time.hour_from >= float(conver_arr[0]):
                for key, line in enumerate(arr_line.get('check_out_hour').split("/")):
                    if float(line) >= work_time.hour_to:
                        if 10 < float(line) < 12:
                            start_time = self.convert_float_to_time(work_time.hour_from)
                            end_time = self.convert_float_to_time(work_time.hour_to)
                            self.create_attendance_morning_work_entry(work_entry, arr_line,
                                type_att_work_entry.get('type_att_nomal').id, end_time, flag, start_time)

                    # Attendance(Leave Early)
                    elif float(line) < work_time.hour_to:
                        if 10 < float(line) < 12:
                            # check checkout morning > end time working time
                            if float(line) > time_early_setting:
                                time_float = self.convert_float_to_time(line)
                                start_time = self.convert_float_to_time(work_time.hour_from)
                                self.create_attendance_morning_work_entry(work_entry, arr_line,
                                    type_att_work_entry.get('type_att_leave_early').id, time_float, flag, start_time)
            # Attendance(Come Late)
            if float(conver_arr[0]) > work_time.hour_from:
                if float(conver_arr[0]) <= time_late_setting:
                    for line in arr_line.get('check_out_hour').split("/"):
                        if float(line) >= work_time.hour_to:
                            if 10 < float(line) < 12:
                                end_time = self.convert_float_to_time(work_time.hour_to)
                                start_time = self.convert_float_to_time(conver_arr[0])
                                self.create_attendance_morning_work_entry(work_entry, arr_line,
                                    type_att_work_entry.get('type_att_come_late').id, end_time, flag, start_time)
                        # Attendance(Come Late & Leave Early)
                        if float(line) < work_time.hour_to:
                            if 10 < float(line) < 12:
                                if float(line) > time_early_setting:
                                    end_time = self.convert_float_to_time(line)
                                    start_time = self.convert_float_to_time(conver_arr[0])
                                    self.create_attendance_morning_work_entry(work_entry, arr_line,
                                        type_att_work_entry.get('type_att_late_early').id, end_time, flag, start_time)


    def generate_attendance_work_entry_afternoon(self, work_time, arr_line):
        setting_time = self.setting_time()
        work_entry = setting_time.get('work_entry')
        type_att_work_entry = setting_time.get('type_att_work_entry')
        if work_time.day_period == 'afternoon':
            # Attendance(Normal)
            flag = 'afternoon'
            conver_arr_check_in = arr_line.get('check_in_hour').split("/")
            conver_arr_check_out = arr_line.get('check_out_hour').split("/")
            time_late_setting = work_time.hour_from + float(setting_time.get('param_late_time')) / 60
            time_early_setting = work_time.hour_to - float(setting_time.get('param_early_time')) / 60
            for line in conver_arr_check_in:
                if 12.5 < float(line) <= work_time.hour_from:
                    for line_co in conver_arr_check_out:
                        if float(line_co) >= work_time.hour_to:
                            if float(line_co) < 18:
                                start_time = self.convert_float_to_time(work_time.hour_from)
                                end_time = self.convert_float_to_time(work_time.hour_to)
                                self.create_attendance_afternoon_work_entry(work_entry, arr_line,
                                      type_att_work_entry.get('type_att_nomal').id,
                                      end_time, flag, start_time)
                        # Attendance(Leave Early)
                        elif float(line_co) < work_time.hour_to:
                            if float(line_co) > 16 and float(line_co) > time_early_setting:
                                time_float = self.convert_float_to_time(line_co)
                                start_time = self.convert_float_to_time(work_time.hour_from)
                                self.create_attendance_afternoon_work_entry(work_entry, arr_line,
                                      type_att_work_entry.get('type_att_leave_early').id,
                                      time_float, flag, start_time)
                # Attendance(Come Late)
                if float(line) > work_time.hour_from:
                    if float(line) <= time_late_setting:
                        for line_co in conver_arr_check_out:
                            if float(line_co) >= work_time.hour_to:
                                if 16 < float(line_co) < 18:
                                    end_time = self.convert_float_to_time(work_time.hour_to)
                                    start_time = self.convert_float_to_time(line)
                                    self.create_attendance_afternoon_work_entry(work_entry, arr_line,
                                          type_att_work_entry.get('type_att_come_late').id,
                                          end_time, flag, start_time)
                            # Attendance(Come Late & early)
                            if float(line_co) < work_time.hour_to:
                                if 16 < float(line_co) < 18:
                                    if float(line_co) > time_early_setting:
                                        end_time = self.convert_float_to_time(line_co)
                                        start_time = self.convert_float_to_time(line)
                                        self.create_attendance_afternoon_work_entry(work_entry, arr_line,
                                              type_att_work_entry.get('type_att_late_early').id,
                                              end_time, flag, start_time)

    # Shift Request Day Time
    def generate_attendance_work_entry_shift_request_daytime(self, out_shift_request):
        setting_time = self.setting_time()
        work_entry = setting_time.get('work_entry')
        type_att_work_entry = setting_time.get('type_att_work_entry')
        for line in out_shift_request:
            shift_request = self.env['shift.request'].search([('state', '=', 'approved')])
            for check in shift_request:
                if line.get('employee') == check.employee_id.id:
                    # Get line request
                    for line_request in check.shift_request_line_ids:
                        get_date = datetime.strptime(str(line.get('date')), '%Y-%m-%d')
                        if line_request.start_date == get_date.date():
                            start = check.shift_type_id.start_time
                            end = check.shift_type_id.end_time
                            conver_arr_check_in = line.get('check_in_hour').split("/")
                            conver_arr_check_out = line.get('check_out_hour').split("/")
                            time_late_setting = start + float(setting_time.get('param_late_time')) / 60
                            time_early_setting = end - float(setting_time.get('param_early_time')) / 60
                            # Attendance normal
                            if float(conver_arr_check_in[0]) <= start:
                                for check_out in conver_arr_check_out:
                                    if 16 < float(check_out) < 18 and float(check_out) >= end:
                                        start_time = start
                                        end_time = end
                                        self.create_attendance_shift_request(work_entry, line,
                                                type_att_work_entry.get('type_att_nomal').id,
                                                start_time, 'shift_request', float(end_time))
                                    # Attendance leave early
                                    if 16 < float(check_out) < 18 and float(check_out) < end:
                                        if float(check_out) > time_early_setting:
                                            start_time = start
                                            end_time = float(check_out)
                                            self.create_attendance_shift_request(work_entry, line,
                                                 type_att_work_entry.get('type_att_leave_early').id,
                                                 start_time, 'shift_request', end_time)
                            # Attendance come late
                            if float(conver_arr_check_in[0]) > start:
                                if float(conver_arr_check_in[0]) <= time_late_setting:
                                    for check_out in conver_arr_check_out:
                                        if 16 < float(check_out) < 18:
                                            if float(check_out) >= end:
                                                start_time = float(conver_arr_check_in[0])
                                                end_time = end
                                                self.create_attendance_shift_request(work_entry, line,
                                                     type_att_work_entry.get('type_att_come_late').id,
                                                     start_time, 'shift_request', float(end_time))
                            # Attendance late & early
                            if start <= float(conver_arr_check_in[0]) <= time_late_setting:
                                for check_out in conver_arr_check_out:
                                    if time_early_setting <= float(check_out) <= end:
                                        start_time = float(conver_arr_check_in[0])
                                        end_time = float(check_out)
                                        self.create_attendance_shift_request(work_entry, line,
                                             type_att_work_entry.get('type_att_late_early').id,
                                             start_time, 'shift_request', end_time)

    # Shift Request Night
    def generate_attendance_work_entry_shift_request_night(self, out_shift_request):
        setting_time = self.setting_time()
        work_entry = setting_time.get('work_entry')
        type_att_work_entry = setting_time.get('type_att_work_entry')
        shift_request = self.env['shift.request'].search([('state', '=', 'approved')]).filtered(lambda x: x.shift_type_id.code == 'ON')
        arr = {}
        for ot_line in shift_request.mapped('shift_request_line_ids'):
            key = (ot_line.id)
            time_late_setting = ot_line.shift_request_id.shift_type_id.start_time + float(setting_time.get('param_late_time')) / 60
            time_early_setting = ot_line.shift_request_id.shift_type_id.end_time - float(setting_time.get('param_early_time')) / 60
            for line in sorted(out_shift_request, key=lambda k: (k['check_in'])):
                # check in
                if str(ot_line.start_date) == line.get('date'):
                    check_in = line.get('check_in_hour').split("/")
                    for ce_in in check_in:
                        # Attendance normal
                        if 18 < float(ce_in) <= ot_line.shift_request_id.shift_type_id.start_time:
                            start_7 = str(ot_line.start_date) + " " + self.convert_float_to_time(ot_line.shift_request_id.shift_type_id.start_time)
                            get_date_start = datetime.strptime(start_7, '%Y-%m-%d %H:%M') - timedelta(hours=7, minutes=0)
                            if key not in arr:
                                hr_employee = self.env['hr.employee'].search([('id', '=', int(line.get('employee')))])
                                if hr_employee:
                                    name_we = 'WET/SRON/{}/{}/{}'.format(hr_employee.id, get_date_start, hr_employee.code)
                                    arr.update({key: {
                                        'name': name_we,
                                        'employee_id': hr_employee.id,
                                        'date_start': get_date_start,
                                        'state': 'draft',
                                        'contract_id': hr_employee.contract_id.id,
                                        'work_entry_type_id': type_att_work_entry.get('type_att_nomal').id
                                    }})
                        # Attendance come late
                        if 18 < float(ce_in) > ot_line.shift_request_id.shift_type_id.start_time:
                            if float(ce_in) <= time_late_setting:
                                if key not in arr:
                                    hr_employee = self.env['hr.employee'].search([('id', '=', int(line.get('employee')))])
                                    convert_date = line.get('date') + " " + self.convert_float_to_time(ce_in)
                                    if hr_employee:
                                        get_date_start = datetime.strptime(str(convert_date), '%Y-%m-%d %H:%M') - timedelta(hours=7, minutes=0)
                                        name_we = 'WET/SRON/{}/{}/{}'.format(hr_employee.id, get_date_start, hr_employee.code)
                                        arr.update({key: {
                                            'name': name_we,
                                            'employee_id': hr_employee.id,
                                            'date_start': get_date_start,
                                            'state': 'draft',
                                            'contract_id': hr_employee.contract_id.id,
                                            'work_entry_type_id': self.code_att_work_type().get('type_att_come_late').id
                                        }})
                # check out
                if str(ot_line.end_date) == line.get('date'):
                    check_out = line.get('check_out_hour').split("/")
                    for out in check_out:
                        if float(out) >= ot_line.shift_request_id.shift_type_id.end_time:
                            if key in arr:
                                end_7 = str(ot_line.end_date) + " " + self.convert_float_to_time(ot_line.shift_request_id.shift_type_id.end_time)
                                get_date_stop = datetime.strptime(end_7, '%Y-%m-%d %H:%M') - timedelta(hours=7, minutes=0)
                                # arr[key]['check_out'] = ot_line.shift_request_id.shift_type_id.end_time
                                arr[key]['date_stop'] = get_date_stop
                                break
                        if float(out) < ot_line.shift_request_id.shift_type_id.end_time:
                            if float(out) > time_early_setting:
                                if key in arr:
                                    # Attendance leave early
                                    end_7 = str(ot_line.end_date) + " " + self.convert_float_to_time(float(out))
                                    get_date_stop = datetime.strptime(end_7, '%Y-%m-%d %H:%M')
                                    arr[key]['date_stop'] = get_date_stop - timedelta(hours=7, minutes=0)
                                    for y in arr[key]:
                                        if y == 'work_entry_type_id' and arr[key][y] == self.code_att_work_type().get('type_att_come_late').id:
                                            arr[key]['work_entry_type_id'] = self.code_att_work_type().get('type_att_late_early').id
                                        elif y == 'work_entry_type_id' and arr[key][y] == type_att_work_entry.get('type_att_nomal').id:
                                            arr[key]['work_entry_type_id'] = self.code_att_work_type().get('type_att_leave_early').id
                                    break
        if arr:
            for i in arr.values():
                if i.get('date_stop'):
                    check = self.env['hr.work.entry'].search([('name', '=', i.get('name'))])
                    if not check:
                        self.env['hr.work.entry'].create(i)


    def generate_attendance_ot(self, out_1):
        setting_time = self.setting_time()
        work_entry = setting_time.get('work_entry')
        type_att_work_entry = setting_time.get('type_att_work_entry')
        over_time = self.env['hr.overtime'].search([('states', '=', 'approved')])
        for line in out_1:
            for check_in_ot in line.get('check_in_hour').split("/"):
                for ot in over_time:
                    if line.get('employee') == ot.emploeee_id.id:
                        for ot_ids in ot.detail_overtime_ids:
                            from_hours = datetime.strptime(str(ot_ids.from_hours), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                            if line.get('date') == str(from_hours.date()):
                                to_hours = datetime.strptime(str(ot_ids.to_hours), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                                get_from_hour = from_hours.hour + from_hours.minute / 60
                                get_to_hour = to_hours.hour + to_hours.minute / 60
                                time_late_setting = get_from_hour + float(setting_time.get('param_late_time')) / 60
                                time_early_setting = get_to_hour - float(setting_time.get('param_early_time')) / 60
                                # Attendance(Normal)
                                # OT shift request daytime
                                if 17.5 < float(check_in_ot) <= get_from_hour:
                                    for check_out_ot in line.get('check_out_hour').split("/"):
                                        if float(check_out_ot) >= get_to_hour:
                                            start_time = get_from_hour
                                            end_time = get_to_hour
                                            self.create_attendance_OT_work_entry(work_entry, line,
                                                    type_att_work_entry.get('type_extra_hours').id,
                                                    start_time, "OT", end_time, ot_ids.rate_salary)
                                        # Attendance (Leave & Early)
                                        elif float(check_out_ot) < get_to_hour and float(check_out_ot) > time_early_setting:
                                            start_time = get_from_hour
                                            end_time = float(check_out_ot)
                                            self.create_attendance_OT_work_entry(work_entry, line,
                                                type_att_work_entry.get('type_extra_hours_leave_early').id, start_time, "OT",
                                                end_time, ot_ids.rate_salary)
                                # OT shift requsest ON
                                # Attendance(Normal)
                                if 3.5 < float(check_in_ot) <= get_from_hour:
                                    for check_out_ot in line.get('check_out_hour').split("/"):
                                        if float(check_out_ot) >= get_to_hour:
                                            start_time = get_from_hour
                                            end_time = get_to_hour
                                            self.create_attendance_OT_work_entry(work_entry, line,
                                                type_att_work_entry.get('type_extra_hours').id,
                                                start_time, "OT", end_time, ot_ids.rate_salary)
                                        # Attendance (Leave & Early)
                                        elif float(check_out_ot) < get_to_hour and float(check_out_ot) > time_early_setting:
                                            start_time = get_from_hour
                                            end_time = float(check_out_ot)
                                            self.create_attendance_OT_work_entry(work_entry, line,
                                                 type_att_work_entry.get('type_extra_hours_leave_early').id,
                                                 start_time, "OT", end_time, ot_ids.rate_salary)

                                # Attendance(Come & Late)
                                if float(check_in_ot) > get_from_hour and float(check_in_ot) <= time_late_setting:
                                    for check_out_ot in line.get('check_out_hour').split("/"):
                                        if float(check_out_ot) >= get_to_hour:
                                            start_time = float(check_in_ot)
                                            end_time = get_to_hour
                                            self.create_attendance_OT_work_entry(work_entry, line,
                                                type_att_work_entry.get('type_extra_hours_come_late').id, start_time, "OT",
                                                end_time, ot_ids.rate_salary)
                                # Attendance(Come Late & early)
                                if get_from_hour <= float(check_in_ot) <= time_late_setting:
                                    for check_out_ot in line.get('check_out_hour').split("/"):
                                        if time_early_setting <= float(check_out_ot) <= get_to_hour:
                                            start_time = float(check_in_ot)
                                            end_time = float(check_out_ot)
                                            self.create_attendance_OT_work_entry(work_entry, line,
                                                 type_att_work_entry.get('type_extra_hours_late_early').id, start_time, "OT",
                                                 end_time, ot_ids.rate_salary)
    # Get Attendance log
    def get_attendance_log(self, arr_new):
        out = []
        for i in arr_new:
            if i.get('date') not in out:
                out.append({
                    'employee': i.get('employee'),
                    'employee_name': i.get('employee_name'),
                    'code': i.get('code'),
                    'date': i.get('date'),
                })
        out_1 = [dict(t) for t in {tuple(d.items()) for d in out}]
        for i in arr_new:
            for j in out_1:
                if i.get('date') == j.get('date') and i.get('code') == j.get('code') and i.get('employee') == j.get('employee'):
                    if 'check_in' in j:
                        j.update({
                            'check_in': j.get('check_in') + "/" + i.get('check_in'),
                            'check_out': j.get('check_out') + "/" + i.get('check_out'),
                            'check_in_hour': j.get('check_in_hour') + "/" + i.get('check_in_hour'),
                            'check_out_hour': j.get('check_out_hour') + "/" + i.get('check_out_hour'),
                        })
                    else:
                        j.update({
                            'check_in': i.get('check_in'),
                            'check_out': i.get('check_out'),
                            'check_in_hour': i.get('check_in_hour'),
                            'check_out_hour': i.get('check_out_hour'),
                        })
        return out_1

    # Get all empployee working time
    def get_employee_working_time(self, employee_id, models_contract,models_hr_overtime, models_attendance):
        arr_attendance = []
        for employee_chid in employee_id:
            contract_id = models_contract.search([('employee_id', '=', employee_chid.id), ('employee_id.check_over_time', '=', False), ('state', '=', 'open')]).filtered(lambda x: x.resource_calendar_id)
            overtime_id = models_hr_overtime.search([('emploeee_id', '=', employee_chid.id)])
            attendance_id = models_attendance.search([('employee_id', '=', employee_chid.id)])
            if contract_id:
                for line in attendance_id:
                    # Getemployee have contract
                    if line.employee_id.id == contract_id.employee_id.id:
                        if line.check_in and line.check_out:
                            format_hour = '%Y-%m-%d %H:%M:%S'
                            hours_check_in = datetime.strptime(str(line.check_in), format_hour)
                            hours_check_out = datetime.strptime(str(line.check_out), format_hour)
                            float_check_in = hours_check_in + timedelta(hours=7, minutes=0)
                            float_check_out = hours_check_out + timedelta(hours=7, minutes=0)
                            get_date = hours_check_in + timedelta(hours=7, minutes=0)
                            check_in_hour = str(float_check_in.time()).split(':')[0]
                            check_in_hour = float(check_in_hour) + float(str(float_check_in.time()).split(':')[1]) / 60
                            check_out_hour = str(float_check_out.time()).split(':')[0]
                            check_out_hour = float(check_out_hour) + float(str(float_check_out.time()).split(':')[1]) / 60
                            value = {
                                'employee': line.employee_id.id,
                                'employee_name': line.employee_id.name,
                                'code': line.employee_id.code,
                                'date': str(get_date.date()),
                                'check_in': str(float_check_in),
                                'check_out': str(float_check_out),
                                'check_in_hour': '{}'.format(check_in_hour),
                                'check_out_hour': '{}'.format(check_out_hour),
                            }
                            arr_attendance.append(value)
        return arr_attendance

    def change_cron_time(self):
        ir_cron = self.env.ref('pdt_work_entry.cron_generate_work_entries')
        nextcall = ir_cron.nextcall + relativedelta(months=1)
        last_next_date = nextcall.replace(day=calendar.monthrange(nextcall.year, int(nextcall.month))[1])
        ir_cron.write({
            'nextcall': last_next_date
        })

    def create_work_entries_by_time_of(self):
        # self.change_cron_time()
        from_date = datetime.strptime("01-%s-%s" % (datetime.now().month, datetime.now().year), '%d-%m-%Y')
        to_date = from_date.replace(
            day=calendar.monthrange(datetime.now().year, int(datetime.now().month))[1]) + timedelta(hours=23,
                                                                                                    minutes=58,
                                                                                                    seconds=58)
        time_off = self.env['hr.leave'].search([
            ('state', '=', 'validate'),
            ('work_entries', '=', False)
        ])
        work_entries_type = self.env['hr.work.entry.type'].search([])
        time_of_type = work_entries_type.mapped('leave_type_ids').ids
        time_off_half_date_am = time_off.filtered(lambda x: x.request_date_from_period == 'am' and x.request_unit_half and from_date.date() <= x.request_date_from <= to_date.date() and x.holiday_status_id.id in time_of_type)
        time_off_half_date_pm = time_off.filtered(lambda x: x.request_date_from_period == 'pm' and x.request_unit_half and from_date.date() <= x.request_date_from <= to_date.date() and x.holiday_status_id.id in time_of_type)
        time_off_custom_hour = time_off.filtered(lambda x: x.request_unit_hours and from_date.date() <= x.request_date_from <= to_date.date() and x.holiday_status_id.id in time_of_type)
        time_off_normal = time_off.filtered(lambda x: not x.request_unit_half and not x.request_unit_hours and x.request_date_from >= from_date.date() and x.request_date_to <= to_date.date() and x.holiday_status_id.id in time_of_type)
        for i in time_off_half_date_am:
            work_entries = self.env['hr.work.entry.type'].search([]).filtered(
                lambda x: i.holiday_status_id.id in x.leave_type_ids.ids)
            for emp in i.employee_ids:
                if not emp.check_over_time:
                    week_day = i.request_date_from.weekday()
                    resource_calendar = False
                    if emp.contract_id and emp.contract_id.resource_calendar_id:
                        resource_calendar = emp.contract_id.resource_calendar_id.attendance_ids.filtered(
                            lambda x: x.dayofweek == str(week_day) and x.day_period == 'morning')
                    elif emp.resource_calendar_id:
                        resource_calendar = emp.resource_calendar_id.attendance_ids.filtered(
                            lambda x: x.dayofweek == str(week_day) and x.day_period == 'morning')
                    if resource_calendar:
                        value = {
                            'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                            'employee_id': emp.id,
                            'work_entry_type_id': work_entries[0].id if work_entries else False,
                            'date_start': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((resource_calendar.hour_from - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                            'date_stop': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((resource_calendar.hour_to - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                        }
                        try:
                            work_entry = self.env['hr.work.entry'].create(value)
                            i.work_entries = work_entry.id
                        except UserError as e:
                            continue

                else:
                    shift_request_line = self.env['shift.request.line'].search([
                        ('shift_request_id.state', '=', 'approved'),
                        ('shift_request_id.employee_id', '=', emp.id),
                        ('shift_request_id.start_date', '<=', i.request_date_from),
                        ('shift_request_id.end_date', '>=', i.request_date_from),
                        ('shift_request_id.shift_type_id', '=', self.env.ref('pdt_shift_management.morning_shift').id)
                    ])
                    if shift_request_line:
                        from_time = (shift_request_line[0].shift_request_id.shift_type_id.end_time - shift_request_line[0].shift_request_id.shift_type_id.start_time) / 2
                        to_time = shift_request_line[0].shift_request_id.shift_type_id.end_time
                        value = {
                            'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                            'employee_id': emp.id,
                            'work_entry_type_id': work_entries[0].id if work_entries else False,
                            'date_start': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((from_time) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                            'date_stop': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((to_time - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                        }
                        try:
                            work_entry = self.env['hr.work.entry'].create(value)
                            i.work_entries = work_entry.id
                        except UserError as e:
                            continue

        for i in time_off_half_date_pm:
            work_entries = self.env['hr.work.entry.type'].search([]).filtered(
                lambda x: i.holiday_status_id.id in x.leave_type_ids.ids)
            for emp in i.employee_ids:
                if not emp.check_over_time:
                    week_day = i.request_date_from.weekday()
                    resource_calendar = False
                    if emp.contract_id and emp.contract_id.resource_calendar_id:
                        resource_calendar = emp.contract_id.resource_calendar_id.attendance_ids.filtered(
                            lambda x: x.dayofweek == str(week_day) and x.day_period == 'afternoon')
                    elif emp.resource_calendar_id:
                        resource_calendar = emp.resource_calendar_id.attendance_ids.filtered(
                            lambda x: x.dayofweek == str(week_day) and x.day_period == 'afternoon')
                    if resource_calendar:
                        value = {
                            'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                            'employee_id': emp.id,
                            'work_entry_type_id': work_entries[0].id if work_entries else False,
                            'date_start': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((resource_calendar.hour_from - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                            'date_stop': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((resource_calendar.hour_to - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                        }
                        try:
                            work_entry = self.env['hr.work.entry'].create(value)
                            i.work_entries = work_entry.id
                        except UserError as e:
                            continue

                else:
                    shift_request_line = self.env['shift.request.line'].search([
                        ('shift_request_id.state', '=', 'approved'),
                        ('shift_request_id.employee_id', '=', emp.id),
                        ('shift_request_id.start_date', '<=', i.request_date_from),
                        ('shift_request_id.end_date', '>=', i.request_date_from),
                        ('shift_request_id.shift_type_id', '=', self.env.ref('pdt_shift_management.evening_shift').id)
                    ])
                    if shift_request_line:
                        from_time = shift_request_line[0].shift_request_id.shift_type_id.start_time
                        to_time = (shift_request_line[0].shift_request_id.shift_type_id.end_time - shift_request_line[0].shift_request_id.shift_type_id.start_time) / 2
                        value = {
                            'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                            'employee_id': emp.id,
                            'work_entry_type_id': work_entries[0].id if work_entries else False,
                            'date_start': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((from_time - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                            'date_stop': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                *divmod((to_time - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                        }
                        try:
                            work_entry = self.env['hr.work.entry'].create(value)
                            i.work_entries = work_entry.id
                        except UserError as e:
                            continue
        for i in time_off_custom_hour:
            work_entries = self.env['hr.work.entry.type'].search([]).filtered(
                lambda x: i.holiday_status_id.id in x.leave_type_ids.ids)
            for emp in i.employee_ids:
                value = {
                    'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                    'employee_id': emp.id,
                    'work_entry_type_id': work_entries[0].id if work_entries else False,
                    'date_start': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                        *divmod((i.request_hour_from - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                    'date_stop': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                        *divmod((i.request_hour_to - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                }
                try:
                    work_entry = self.env['hr.work.entry'].create(value)
                    i.work_entries = work_entry.id
                except UserError as e:
                    continue
        for i in time_off_normal:
            work_entries = self.env['hr.work.entry.type'].search([]).filtered(
                lambda x: i.holiday_status_id.id in x.leave_type_ids.ids)
            for emp in i.employee_ids:
                if not emp.check_over_time:
                    if i.request_date_from != i.request_date_to:
                        for idate in range((datetime.strptime(str(i.request_date_to), '%Y-%m-%d') - datetime.strptime(
                                str(i.request_date_from), '%Y-%m-%d')).days + 1):
                            date_count = i.request_date_from + timedelta(days=idate)
                            week_day = date_count.weekday()
                            resource_calendar_morning = False
                            resource_calendar_afternoon = False
                            if emp.contract_id and emp.contract_id.resource_calendar_id:
                                resource_calendar_morning = emp.contract_id.resource_calendar_id.attendance_ids.filtered(
                                    lambda x: x.dayofweek == str(week_day) and x.day_period in ['morning'])
                                resource_calendar_afternoon = emp.contract_id.resource_calendar_id.attendance_ids.filtered(
                                    lambda x: x.dayofweek == str(week_day) and x.day_period in ['afternoon'])
                            elif emp.resource_calendar_id:
                                resource_calendar_morning = emp.resource_calendar_id.attendance_ids.filtered(
                                    lambda x: x.dayofweek == str(week_day) and x.day_period in ['morning'])
                                resource_calendar_afternoon = emp.resource_calendar_id.attendance_ids.filtered(
                                    lambda x: x.dayofweek == str(week_day) and x.day_period in ['afternoon'])
                            if resource_calendar_morning:
                                value = {
                                    'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                                    'employee_id': emp.id,
                                    'work_entry_type_id': work_entries[0].id if work_entries else False,
                                    'date_start': datetime.strptime(str(date_count) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                        *divmod((resource_calendar_morning.hour_from - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                    'date_stop': datetime.strptime(str(date_count) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                        *divmod((resource_calendar_morning.hour_to - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                }
                                try:
                                    work_entry = self.env['hr.work.entry'].create(value)
                                    i.work_entries = work_entry.id
                                except UserError as e:
                                    continue
                            if resource_calendar_afternoon:
                                value = {
                                    'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                                    'employee_id': emp.id,
                                    'work_entry_type_id': work_entries[0].id if work_entries else False,
                                    'date_start': datetime.strptime(str(date_count) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                        *divmod((resource_calendar_afternoon.hour_from - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                    'date_stop': datetime.strptime(str(date_count) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                        *divmod((resource_calendar_afternoon.hour_to - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                }
                                try:
                                    work_entry = self.env['hr.work.entry'].create(value)
                                    i.work_entries = work_entry.id
                                except UserError as e:
                                    continue

                    else:
                        week_day = i.request_date_from.weekday()
                        resource_calendar_morning = False
                        resource_calendar_afternoon = False
                        if emp.contract_id and emp.contract_id.resource_calendar_id:
                            resource_calendar_morning = emp.contract_id.resource_calendar_id.attendance_ids.filtered(
                                lambda x: x.dayofweek == str(week_day) and x.day_period in ['morning'])
                            resource_calendar_afternoon = emp.contract_id.resource_calendar_id.attendance_ids.filtered(
                                lambda x: x.dayofweek == str(week_day) and x.day_period in ['afternoon'])
                        elif emp.resource_calendar_id:
                            resource_calendar_morning = emp.resource_calendar_id.attendance_ids.filtered(
                                lambda x: x.dayofweek == str(week_day) and x.day_period in ['morning'])
                            resource_calendar_afternoon = emp.resource_calendar_id.attendance_ids.filtered(
                                lambda x: x.dayofweek == str(week_day) and x.day_period in ['afternoon'])
                        if resource_calendar_morning:
                            value = {
                                'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                                'employee_id': emp.id,
                                'work_entry_type_id': work_entries[0].id if work_entries else False,
                                'date_start': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                    *divmod((resource_calendar_morning.hour_from - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                'date_stop': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                    *divmod((resource_calendar_morning.hour_to - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                            }
                            try:
                                work_entry = self.env['hr.work.entry'].create(value)
                                i.work_entries = work_entry.id
                            except UserError as e:
                                continue
                        if resource_calendar_afternoon:
                            value = {
                                'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                                'employee_id': emp.id,
                                'work_entry_type_id': work_entries[0].id if work_entries else False,
                                'date_start': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                    *divmod((resource_calendar_afternoon.hour_from - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                'date_stop': datetime.strptime(str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                    *divmod((resource_calendar_afternoon.hour_to - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                            }
                            try:
                                work_entry = self.env['hr.work.entry'].create(value)
                                i.work_entries = work_entry.id
                            except UserError as e:
                                continue

                else:
                    if i.request_date_from != i.request_date_to:
                        for idate in range((datetime.strptime(str(i.request_date_to), '%Y-%m-%d') - datetime.strptime(
                                str(i.request_date_from), '%Y-%m-%d')).days + 1):
                            date_count = i.request_date_from + timedelta(days=idate)
                            shift_request_line = self.env['shift.request.line'].search([
                                ('shift_request_id.state', '=', 'approved'),
                                ('shift_request_id.employee_id', '=', emp.id),
                                ('shift_request_id.start_date', '<=', date_count),
                                ('shift_request_id.end_date', '>=', date_count)
                            ])
                            if shift_request_line:
                                from_time = shift_request_line[0].shift_request_id.shift_type_id.start_time
                                to_time = shift_request_line[0].shift_request_id.shift_type_id.end_time
                                value = {
                                    'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                                    'employee_id': emp.id,
                                    'work_entry_type_id': work_entries[0].id if work_entries else False,
                                    'date_start': datetime.strptime(
                                        str(date_count) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                            *divmod((from_time - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                    'date_stop': datetime.strptime(
                                        str(date_count) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                            *divmod((to_time - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                }
                                try:
                                    work_entry = self.env['hr.work.entry'].create(value)
                                    i.work_entries = work_entry.id
                                except UserError as e:
                                    continue

                    else:
                        shift_request_line = self.env['shift.request.line'].search([
                            ('shift_request_id.state', '=', 'approved'),
                            ('shift_request_id.employee_id', '=', emp.id),
                            ('shift_request_id.start_date', '<=', i.request_date_from),
                            ('shift_request_id.end_date', '>=', i.request_date_to)
                        ])
                        if shift_request_line:
                            from_time = shift_request_line[0].shift_request_id.shift_type_id.start_time
                            to_time = shift_request_line[0].shift_request_id.shift_type_id.end_time
                            value = {
                                'name': 'Time off of %s with type leave %s' % (emp.name, i.holiday_status_id.name),
                                'employee_id': emp.id,
                                'work_entry_type_id': work_entries[0].id if work_entries else False,
                                'date_start': datetime.strptime(
                                    str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                        *divmod((from_time - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                                'date_stop': datetime.strptime(
                                    str(i.request_date_from) + ' {0:02.0f}:{1:02.0f}:00'.format(
                                        *divmod((to_time - 7) * 60, 60)), '%Y-%m-%d %H:%M:%S'),
                            }
                            try:
                                work_entry = self.env['hr.work.entry'].create(value)
                                i.work_entries = work_entry.id
                            except UserError as e:
                                continue
    @api.model
    def generate_attendance_to_work_entries(self):
        self.create_work_entries_by_time_of()
        models_employee = self.env['hr.employee']
        models_contract = self.env['hr.contract']
        models_hr_overtime = self.env['hr.overtime']
        models_attendance = self.env['hr.attendance']
        # Employee Working Time
        employee_id = models_employee.search([('check_over_time', '=', False)])
        employee_shif_request = models_employee.search([('check_over_time', '=', True)])
        arr_new = []
        arr_new1 = []
        if employee_id:
            arr_attendance = self.get_employee_working_time(employee_id,
                                    models_contract, models_hr_overtime, models_attendance)
            arr_new = arr_attendance[::-1]
        if employee_shif_request:
            arr_attendance_ot = self.get_employee_working_time(employee_shif_request,
                                    models_contract, models_hr_overtime, models_attendance)
            arr_new1 = arr_attendance_ot[::-1]
        out_1 = self.get_attendance_log(arr_new)
        out_shift_request = self.get_attendance_log(arr_new1)
        # Check OT working time
        if out_1:
            self.generate_attendance_ot(out_1)
        # Check OT shift request
        if out_shift_request:
            self.generate_attendance_ot(out_shift_request)
        # Check shift request day time
        self.generate_attendance_work_entry_shift_request_daytime(out_shift_request)
        # Check shift request night
        self.generate_attendance_work_entry_shift_request_night(out_shift_request)
        log.info("Hello, world11111111111111111111111111111111111111")
        for work_time in employee_id.resource_calendar_id.attendance_ids[:2]:
            for arr_line in out_1:
                # Check morning
                self.generate_attendance_work_entry_morning(work_time, arr_line)
                # Check afternoon
                self.generate_attendance_work_entry_afternoon(work_time, arr_line)

    @api.model
    def create_attendance_morning_work_entry(self, work_entry, arr_line, type_att_work_entry, end_time, flag, start_time):
        if flag == 'morning':
            hours_check_out = arr_line.get('date') + " " + end_time + ":00"
            hours_in_out = arr_line.get('date') + " " + str(start_time) + ":00"
            date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            date_start = datetime.strptime(str(hours_in_out), '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            # print(1111, arr_line.get('employee_name'), date_start)
            check = work_entry.search([]).mapped('name')
            check_name = 'WET/MN/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'), arr_line.get('code'))
            value = {
                'name': 'WET/MN/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'), arr_line.get('code')),
                'employee_id': arr_line.get('employee'),
                'work_entry_type_id': type_att_work_entry,
                'date_start':  date_start,
                'date_stop': date_stop,
                'state': 'draft'

            }
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    @api.model
    def create_attendance_afternoon_work_entry(self, work_entry, arr_line, type_att_work_entry, end_time, flag, start_time):
        if flag == 'afternoon':
            hours_check_in = arr_line.get('date') + " " + start_time + ":00"
            hours_check_out = arr_line.get('date') + " " + end_time + ":00"
            date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            check = work_entry.search([]).mapped('name')
            check_name = 'WET/AT/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'), arr_line.get('code'))
            value = {
                'name': 'WET/AT/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'), arr_line.get('code')),
                'employee_id': arr_line.get('employee'),
                'work_entry_type_id': type_att_work_entry,
                'date_start': date_start,
                'date_stop': date_stop,
                'state': 'draft'
            }
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    @api.model
    def create_attendance_OT_work_entry(self, work_entry, arr_line, type_att_work_entry, time_float, flag,start, rate_salary):
        if flag == 'OT':
            from_hours = self.convert_float_to_time(time_float)
            to_hours = self.convert_float_to_time(start)
            hours_check_in = arr_line.get('date') + " " + from_hours + ":00"
            hours_check_out = arr_line.get('date') + " " + to_hours + ":00"
            date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            duration = ((24 + date_stop.minute / 60) - (date_start.hour + date_start.minute / 60)) * rate_salary \
                        if date_stop.hour == 0 \
                        else ((date_stop.hour + date_stop.minute / 60) -
                              (date_start.hour + date_start.minute / 60)) * rate_salary
            result_duration = duration - ((24 + date_stop.minute / 60) -
                                    (date_start.hour + date_start.minute / 60)) \
                                    if date_stop.hour == 0 \
                                    else duration - ((date_stop.hour + date_stop.minute / 60) -
                                    (date_start.hour + date_start.minute / 60))
            minutes = result_duration * 60
            hours, minutes = divmod(minutes, 60)
            check = work_entry.search([]).mapped('name')
            check_name = 'WET/OT/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'), arr_line.get('code'))
            value = {
                'name': 'WET/OT/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'), arr_line.get('code')),
                'employee_id': arr_line.get('employee'),
                'work_entry_type_id': type_att_work_entry,
                'date_start': date_start,
                'date_stop': date_stop + timedelta(hours=hours, minutes=minutes),
                'state': 'draft',
            }
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)
    @api.model
    def create_attendance_shift_request(self, work_entry, arr_line, type_att_work_entry, time_float, flag ,start):
        if flag == "shift_request":
            from_hours = self.convert_float_to_time(time_float)
            to_hours = self.convert_float_to_time(start)
            hours_check_in = arr_line.get('date') + " " + from_hours + ":00"
            hours_check_out = arr_line.get('date') + " " + to_hours + ":00"
            date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            check = work_entry.search([]).mapped('name')
            check_name = 'WET/SR/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'),
                                                  arr_line.get('code'))
            value = {
                'name': 'WET/SR/{}/{}-{}'.format(arr_line.get('date'), arr_line.get('employee_name'),
                                                 arr_line.get('code')),
                'employee_id': arr_line.get('employee'),
                'work_entry_type_id': type_att_work_entry,
                'date_start': date_start,
                'date_stop': date_stop,
                'state': 'draft',
            }
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    def convert_float_to_time(self, line):
        td = timedelta(hours=float(line))
        dt = datetime.min + td
        time_float = "{:%H:%M}".format(dt)
        return time_float




















