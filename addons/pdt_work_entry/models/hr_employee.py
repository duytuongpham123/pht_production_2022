from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    check_over_time = fields.Boolean()
    employee_position = fields.Selection([
        ('office', 'Văn phòng'),
        ('worker', 'Công nhân')
    ], default='office')
    type_date = fields.Selection([
        ('in_day', 'Cả ngày'),
        ('sessions', 'Theo buổi')
    ], default='in_day')

    @api.onchange('type_date')
    def set_type_date_employee(self):
        if self.type_date:
            models = self.env['resource.calendar']
            work_time = models.search([('type_date', '=', self.type_date)], limit=1)
            self.resource_calendar_id = work_time.id












