from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    late_time = fields.Float('Số giờ được phép đi muộn')
    early_time = fields.Float('Số giờ được phép về sớm')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            late_time=self.env['ir.config_parameter'].sudo().get_param(
                'pdt_work_entry.late_time'),
            early_time=self.env['ir.config_parameter'].sudo().get_param(
                'pdt_work_entry.early_time'),
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()
        late_time = self.late_time
        early_time = self.early_time
        param.set_param('pdt_work_entry.late_time', late_time)
        param.set_param('pdt_work_entry.early_time', early_time)








