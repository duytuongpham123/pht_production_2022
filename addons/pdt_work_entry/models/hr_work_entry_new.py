from odoo import api, models, fields, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from collections import defaultdict
from operator import itemgetter
from itertools import zip_longest
import calendar
from dateutil.relativedelta import relativedelta
import logging
from collections import defaultdict
log = logging.getLogger("my-logger")
import itertools

class HrWorkEntry(models.Model):
    _inherit = "hr.work.entry"
    duration = fields.Float(string="Period")
    pht_duration = fields.Float(string="Period")
    date_attendance_start = fields.Datetime('Attendance Start')
    date_attendance_end = fields.Datetime('Attendance End')

    @api.onchange('date_stop', 'date_start')
    def _compute_duration(self):
        for work_entry in self:
            work_entry.duration = work_entry._get_duration(work_entry.date_start, work_entry.date_stop)

    def code_att_work_type(self):
        models = self.env['hr.work.entry.type']
        value = {
            'type_att_nomal': models.search([('code', '=', 'WORK100')]),
            'type_att_come_late': models.search([('code', '=', 'ATTCOMELATE')]),
            'type_att_leave_early': models.search([('code', '=', 'ATTLEAVEEARLY')]),
            'type_att_late_early': models.search([('code', '=', 'ATTCLLE')]),
            'type_extra_hours': models.search([('code', '=', 'EXH')]),
            'type_extra_hours_come_late': models.search([('code', '=', 'EXHCL')]),
            'type_extra_hours_leave_early': models.search([('code', '=', 'EXHLE')]),
            'type_extra_hours_late_early': models.search([('code', '=', 'EXHCLLE')]),
            'type_unpaid_leave_normal': models.search([('code', '=', 'LEAVE90')]),
            'type_unpaid_leave_years': models.search([('code', '=', 'LEAVE120')]),
            'type_unpaid_leave_100': models.search([('code', '=', 'LEAVE100')]),
            'type_unpaid_leave_special': models.search([('code', '=', 'LEAVE90SP')]),
            'type_leave_holiday': models.search([('code', '=', 'LEAVES_HOLIDAY')]),
            'type_extra_holiday_hours': models.search([('code', '=', 'EXTRA_HOLIDAY')])
        }
        return value

    # nhân viên làm full ngày
    def set_work_entries_employee_office_full_day(self):
        employee_full_day = self.env['hr.employee'].search([
            ('type_date', '=', 'in_day'),
            ('resource_calendar_id.type_date', '=', 'in_day'),
            ('contract_id.state', '=', 'open'),
            ('employee_position', '=', 'office')
        ])
        arr_date = []
        arr_date_check = []
        for emp in employee_full_day:
            hr_attendance = self.env['hr.attendance'].search([('employee_id', '=', emp.id)])
            for attendance in hr_attendance:
                if attendance.check_in and attendance.check_out:
                    check_in = datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                    check_out = datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                    flot_check_in = check_in.hour + check_in.minute/60.0
                    flot_check_out = check_out.hour + check_out.minute / 60.0
                    word_time = emp.resource_calendar_id.attendance_ids
                    hour_from = word_time[0].hour_from
                    hour_to = word_time[1].hour_to
                    time_off = word_time[1].hour_from - word_time[0].hour_to
                    if not (flot_check_in < hour_from and flot_check_out < hour_from) or not (flot_check_in > hour_to and flot_check_out > hour_to):
                        key = "{}-{}".format(attendance.employee_id.id, str(check_in).split(' ')[0])
                        if check_in.weekday() < 6:
                            arr_date.append({key: {
                                'date': str(check_in).split(' ')[0],
                                'employee_id': attendance.employee_id.id,
                                'work_entry_type_id': self.code_att_work_type().get('type_att_nomal').id,
                                'time_off': time_off,
                                'hour_from': self.convert_float_to_time(hour_from),
                                'hour_to': self.convert_float_to_time(hour_to),
                                'check_in': datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S'),
                                'check_out': datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S')
                            }})
                            arr_date_check.append(key)
        # res_date = [i for n, i in enumerate(arr_date) if i not in arr_date[n + 1:]]
        res_date = self.set_time_worker(arr_date, arr_date_check)
        return res_date

    # nhân viên làm theo buổi
    def set_work_entries_employee_office_sessions(self):
        employee_sessions = self.env['hr.employee'].search([
            ('type_date', '=', 'sessions'),
            ('resource_calendar_id.type_date', '=', 'sessions'),
            ('contract_id.state', '=', 'open'),
            ('employee_position', '=', 'office')
        ])
        arr_date_morning = []
        arr_date_afternoon = []
        arr_date_morning_check = []
        arr_date_afternoon_check = []
        for emp in employee_sessions:
            hr_attendance = self.env['hr.attendance'].search([('employee_id', '=', emp.id)])
            for attendance in hr_attendance:
                if attendance.check_in and attendance.check_out:
                    check_in = datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                    check_out = datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                    flot_check_in = check_in.hour + check_in.minute / 60.0
                    flot_check_out = check_out.hour + check_out.minute / 60.0
                    word_time = emp.resource_calendar_id.attendance_ids
                    hour_from_morning = word_time[0].hour_from
                    hour_to_morning = word_time[0].hour_to
                    hour_from_afternoon = word_time[1].hour_from
                    hour_to_afternoon = word_time[1].hour_to
                    # time_off = word_time[1].hour_from - word_time[0].hour_to
                    if not (flot_check_in < hour_from_morning and flot_check_out < hour_from_morning) or not (
                            flot_check_in > hour_to_morning and flot_check_out > hour_to_morning):
                        if flot_check_out <= hour_to_morning or hour_to_morning <= flot_check_out < hour_from_afternoon:
                            if check_in.weekday() < 6:
                                key = "{} - {}".format(attendance.employee_id.id, str(check_in).split(' ')[0])
                                arr_date_morning.append({key: {
                                    'date': str(check_in).split(' ')[0],
                                    'employee_id': attendance.employee_id.id,
                                    'work_entry_type_id': self.code_att_work_type().get('type_att_nomal').id,
                                    'hour_from': self.convert_float_to_time(hour_from_morning),
                                    'hour_to': self.convert_float_to_time(hour_to_morning),
                                    'check_in': datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S'),
                                    'check_out': datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S'),
                                    'str_check_in': str(datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)),
                                    'str_check_out': str(datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0))
                                }})
                                arr_date_morning_check.append(key)
                    if not (flot_check_in < hour_from_afternoon and flot_check_out < hour_from_afternoon) or not (
                            flot_check_in > hour_to_afternoon and flot_check_out > hour_to_afternoon):
                        if hour_to_morning < flot_check_in:
                            if check_in.weekday() < 6:
                                key = "{} - {}".format(attendance.employee_id.id, str(check_in).split(' ')[0])
                                arr_date_afternoon.append({key: {
                                    'date': str(check_in).split(' ')[0],
                                    'employee_id': attendance.employee_id.id,
                                    'work_entry_type_id': self.code_att_work_type().get('type_att_nomal').id,
                                    'hour_from': self.convert_float_to_time(hour_from_afternoon),
                                    'hour_to': self.convert_float_to_time(hour_to_afternoon),
                                    'check_in': datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S'),
                                    'check_out': datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S'),
                                    'str_check_in': str(
                                        datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7,
                                                                                                                     minutes=0)),
                                    'str_check_out': str(
                                        datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7,
                                                                                                                      minutes=0))
                                }})
                                arr_date_afternoon_check.append(key)
        # morning = [i for n, i in enumerate(arr_date_morning) if i not in arr_date_morning[n + 1:]]
        morning = self.set_time_worker(arr_date_morning, arr_date_morning_check)
        # afternoon = [i for n, i in enumerate(arr_date_afternoon) if i not in arr_date_afternoon[n + 1:]]
        afternoon = self.set_time_worker(arr_date_afternoon, arr_date_afternoon_check)
        return morning, afternoon

    def setting_time(self):
        param_early_time = self.env['ir.config_parameter'].sudo().get_param('pdt_work_entry.early_time')
        param_late_time = self.env['ir.config_parameter'].sudo().get_param('pdt_work_entry.late_time')
        return param_early_time, param_late_time

    def set_time_worker(self, arr, arr_check):
        arr_res_full = []
        dd = defaultdict(list)
        for d in arr:
            for key, value in d.items():
                dd[key].append(value)
        arr_check = list(dict.fromkeys(arr_check))
        for item in arr_check:
            arr_res_full.append(sorted(dd[item], key=lambda d: d['check_in']))
        return arr_res_full

    def convert_time_work(self, attendance):
        if attendance.check_in and attendance.check_out:
            check_in = datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
            check_out = datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
            check_in_full = check_in.hour + check_in.minute / 60.0
            check_out_full = check_out.hour + check_out.minute / 60.0
            return check_in, check_out, check_in_full, check_out_full

    def set_session_worker(self, arr_res_session_morning, flag):
        result_session = []
        for item in arr_res_session_morning:
            str_check_in = "{} {}".format(item[0]['check_in'].split(' ')[0], self.convert_float_to_time(item[0]['hour_from']))
            str_check_out = "{} {}".format(item[-1]['check_out'].split(' ')[0], self.convert_float_to_time(item[0]['hour_to']))
            value = {
                'date': item[0]['check_in'].split(' ')[0],
                'employee_id': item[0]['employee_id'],
            }
            if flag == 'morning':
                late = item[0]['time_late_morning']
                early = item[0]['time_early_morning']
            else:
                late = item[0]['time_late_afternoon']
                early = item[0]['time_early_afternoon']
            if item[0]['check_in_full'] > late and item[-1]['check_out_full'] >= early:
                value.update({
                    'check_in': item[0]['check_in'],
                    'check_out': str_check_out,
                    'flag': 'late',
                    'work_entry_type_id': self.code_att_work_type().get('type_att_come_late').id,
                    'view_check_in': item[0]['check_in'],
                    'view_check_out': item[-1]['check_out'],
                })
                result_session.append(value)
            if item[0]['check_in_full'] <= late and item[-1]['check_out_full'] < early:
                value.update({
                    'check_in': str_check_in,
                    'check_out': item[-1]['check_out'],
                    'flag': 'early',
                    'work_entry_type_id': self.code_att_work_type().get('type_att_leave_early').id,
                    'view_check_in': item[0]['check_in'],
                    'view_check_out': item[-1]['check_out'],
                })
                result_session.append(value)
            if item[0]['check_in_full'] <= late and item[-1]['check_out_full'] >= early:
                value.update({
                    'flag': 'session',
                    'hour_from': self.convert_float_to_time(item[0]['hour_from']),
                    'hour_to': self.convert_float_to_time(item[0]['hour_to']),
                    'work_entry_type_id': self.code_att_work_type().get('type_att_nomal').id,
                    'view_check_in': item[0]['check_in'],
                    'view_check_out': item[-1]['check_out'],
                })
                result_session.append(value)
            if item[0]['check_in_full'] > late and item[-1]['check_out_full'] < early:
                value.update({
                    'check_in': item[0]['check_in'],
                    'check_out': item[-1]['check_out'],
                    'flag': 'not_session',
                    'work_entry_type_id': self.code_att_work_type().get('type_att_late_early').id,
                    'view_check_in': item[0]['check_in'],
                    'view_check_out': item[-1]['check_out'],
                })
                result_session.append(value)
        return result_session

    def full_day_worker(self, arr, flag_shift):
        result_full = []
        for child in arr:
            str_check_in = "{} {}".format(child[0]['check_in'].split(' ')[0], self.convert_float_to_time(child[0]['hour_from']))
            str_check_out = "{} {}".format(child[-1]['check_out'].split(' ')[0], self.convert_float_to_time(child[0]['hour_to']))
            value = {
                'date': child[0]['check_in'].split(' ')[0],
                'date_to': child[-1]['check_out'].split(' ')[0],
                'employee_id': child[0]['employee_id']
            }
            # đi trễ
            if child[0]['check_in_full'] > child[0]['time_late'] and child[-1]['check_out_full'] >= child[0]['time_early']:
                value.update({
                    'check_in': child[0]['check_in'],
                    'check_out': str_check_out,
                    'flag': 'late',
                    # 'time_off': child[0]['time_off'],
                    'work_entry_type_id': self.code_att_work_type().get('type_att_come_late').id,
                    # 'view_check_in': child[0]['check_in'],
                    'view_check_out': child[-1]['check_out'],
                })
                view_check_in = "{} {}".format(child[0]['check_in'].split(' ')[0], self.convert_float_to_time(child[0]['hour_time_off_to']))
                if flag_shift in ['shift_full', 'full_day_session']:
                    if child[0]['hour_time_off_from'] < child[0]['check_in_full'] < child[0]['hour_time_off_to']:
                        value.update({'time_off': 0})
                    if child[0]['check_in_full'] <= child[0]['hour_time_off_from']:
                        value.update({'time_off': child[0]['time_off']})
                    if child[0]['check_in_full'] >= child[0]['hour_time_off_to']:
                        value.update({'time_off': 0})
                    # kiểm tra lây check out nằm trong khoảng nghỉ trưa
                    if child[0]['hour_time_off_from'] <= child[-1]['check_in_full'] <= child[0]['hour_time_off_to']:
                        value.update({'view_check_in': view_check_in})
                    else:
                        value.update({'view_check_in': child[-1]['check_in']})
                else:
                    value.update({
                        'time_off': child[0]['time_off'],
                        'view_check_in': child[0]['check_in']
                    })
                result_full.append(value)
            # về sớm
            if child[0]['check_in_full'] <= child[0]['time_late'] and child[-1]['check_out_full'] < child[0]['time_early']:
                value.update({
                    'check_in': str_check_in,
                    'check_out': child[-1]['check_out'],
                    'flag': 'early',
                    # 'time_off': child[0]['time_off'],
                    'work_entry_type_id': self.code_att_work_type().get('type_att_leave_early').id,
                    'view_check_in': child[0]['check_in'],
                    # 'view_check_out': child[-1]['check_out'],
                })
                view_check_out = "{} {}".format(child[0]['check_in'].split(' ')[0], self.convert_float_to_time(child[0]['hour_time_off_from']))
                if flag_shift in ['shift_full', 'full_day_session']:
                    # kiểm tra nghỉ trưa
                    if child[-1]['check_out_full'] >= child[0]['hour_time_off_to']:
                        value.update({'time_off': child[0]['time_off']})
                    else:
                        value.update({'time_off': 0})
                    # kiểm tra lây check out nằm trong khoảng nghỉ trưa
                    if child[0]['hour_time_off_from'] <= child[-1]['check_out_full'] <= child[0]['hour_time_off_to']:
                        value.update({'view_check_out': view_check_out})
                    else:
                        value.update({'view_check_out': child[-1]['check_out']})
                else:
                    value.update({
                        'time_off': child[0]['time_off'],
                        'view_check_out': child[-1]['check_out']
                    })
                result_full.append(value)
            # đúng giờ
            if child[0]['check_in_full'] <= child[0]['time_late'] and child[-1]['check_out_full'] >= child[0]['time_early']:
                value.update({
                    'flag': 'full',
                    'hour_from': self.convert_float_to_time(child[0]['hour_from']),
                    'hour_to': self.convert_float_to_time(child[0]['hour_to']),
                    'time_off': child[0]['time_off'],
                    'work_entry_type_id': self.code_att_work_type().get('type_att_nomal').id,
                    'view_check_in': child[0]['check_in'],
                    'view_check_out': child[-1]['check_out'],
                })
                result_full.append(value)
            # đi trễ và về sớm
            if child[0]['check_in_full'] > child[0]['time_late'] and child[-1]['check_out_full'] < child[0]['time_early']:
                view_check_in = "{} {}".format(child[0]['check_in'].split(' ')[0], self.convert_float_to_time(child[0]['hour_time_off_to']))
                view_check_out = "{} {}".format(child[0]['check_in'].split(' ')[0], self.convert_float_to_time(child[0]['hour_time_off_from']))
                value.update({
                    'check_in': child[0]['check_in'],
                    'check_out': child[-1]['check_out'],
                    'flag': 'not_full',
                    # 'time_off': child[0]['time_off'],
                    'work_entry_type_id': self.code_att_work_type().get('type_att_late_early').id,
                    # 'view_check_in': child[0]['check_in'],
                    # 'view_check_out': child[-1]['check_out'],
                })
                if flag_shift in ['shift_full', 'full_day_session']:
                    # kiểm tra nghỉ trưa
                    if child[-1]['check_out_full'] >= child[0]['hour_time_off_to']:
                        if child[0]['hour_time_off_from'] < child[0]['check_in_full'] < child[0]['hour_time_off_to']:
                            value.update({'time_off': 0})
                        if child[0]['check_in_full'] <= child[0]['hour_time_off_from']:
                            value.update({'time_off': child[0]['time_off']})
                        if child[0]['check_in_full'] >= child[0]['hour_time_off_to']:
                            value.update({'time_off': 0})
                    else:
                        value.update({'time_off': 0})
                    # kiểm tra lây check out nằm trong khoảng nghỉ trưa
                    if child[0]['hour_time_off_from'] <= child[-1]['check_out_full'] <= child[0]['hour_time_off_to']:
                        value.update({'view_check_out': view_check_out})
                    elif child[0]['hour_time_off_from'] <= child[-1]['check_in_full'] <= child[0]['hour_time_off_to']:
                        value.update({'view_check_in': view_check_in})
                    else:
                        value.update({
                            'view_check_in': child[0]['check_in'],
                            'view_check_out': child[-1]['check_out'],
                        })
                else:
                    value.update({
                        'time_off': child[0]['time_off'],
                        'view_check_out': child[-1]['check_out']
                    })
                result_full.append(value)
            # trường hợp đặc biệt của ca đêm => đi làm trong ngày và về trong ngày

            if 'flag_overnight' in child[0]:
                if child[0]['flag_overnight']:
                    # case 1 : ca đêm : đi trễ về sớm trong ngày 1
                    if child[0]['check_in_full'] > child[0]['time_late'] and child[-1]['check_out_full'] <= 24:
                        value.update({
                            'check_in': child[0]['check_in'],
                            'check_out': child[-1]['check_out'],
                            'flag': 'not_full',
                            # 'time_off': child[0]['time_off'],
                            'work_entry_type_id': self.code_att_work_type().get('type_att_late_early').id,
                            'view_check_in': child[0]['check_in'],
                            'view_check_out': child[-1]['check_out'],
                        })
                        result_full.append(value)
                    # case 2: ca đêm về sớm ngày 1
                    if child[0]['check_in_full'] <= child[0]['time_late'] and child[-1]['check_out_full'] <= 24:
                        view_check_in = "{} {}".format(child[0]['check_in'].split(' ')[0], self.convert_float_to_time(child[0]['hour_from']))
                        value.update({
                            'check_in': str_check_in,
                            'check_out': child[-1]['check_out'],
                            'flag': 'early',
                            # 'time_off': child[0]['time_off'],
                            'work_entry_type_id': self.code_att_work_type().get('type_att_leave_early').id,
                            'view_check_in': view_check_in,
                            'view_check_out': child[-1]['check_out'],
                        })
        return result_full

    def set_time_work_date_worker(self):
        setting_time = self.setting_time()
        worker = self.env['hr.employee'].search([
            ('contract_id.state', '=', 'open'),
            ('employee_position', '=', 'worker'),
            ('contract_id.state', '=', 'open'),
        ])
        arr_full, arr_full_check, arr_session_morning, arr_session_morning_check, arr_session_afternoon, arr_session_afternoon_check = [], [], [], [], [], []
        arr_shift_full, arr_shift_full_check = [], []
        arr_shift_over_night , arr_shift_over_night_check = [], []
        arr_1, arr_2, arr_3, arr_4 = [], [], [], []
        for wk in worker:
            hr_attendance = self.env['hr.attendance'].search([('employee_id', '=', wk.id)])
            for attendance in hr_attendance:
                attendance_cv = datetime.strptime(str(attendance.check_in), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                attendance_out = datetime.strptime(str(attendance.check_out), '%Y-%m-%d %H:%M:%S') + timedelta(hours=7, minutes=0)
                book_worker = self.env['booking.worker'].search([]).filtered(lambda x: x.date_from <= attendance_cv.date() <= x.date_to)
                if book_worker:
                    full_day = book_worker.line_ids.filtered(lambda x: x.start_date <= attendance_cv.date() <= x.end_date and x.type_booking == 'full_day' and x.employee_id.id == attendance.employee_id.id)
                    sessions = book_worker.line_ids.filtered(lambda x: x.start_date <= attendance_cv.date() <= x.end_date and x.type_booking == 'sessions' and x.employee_id.id == attendance.employee_id.id)
                    shift = book_worker.line_ids.filtered(lambda x: x.start_date <= attendance_cv.date() <= x.end_date and x.type_booking == 'shift' and x.employee_id.id == attendance.employee_id.id)
                    # cả ngày
                    if full_day:
                        for item in full_day:
                            time_late = item.resource_calendar.attendance_ids[0].hour_from + float(setting_time[0]) / 60
                            time_early = item.resource_calendar.attendance_ids[1].hour_to - float(setting_time[1]) / 60
                            time_work = self.convert_time_work(attendance)
                            time_off = item.resource_calendar.attendance_ids[1].hour_from - item.resource_calendar.attendance_ids[0].hour_to
                            if not (time_work[2] < time_late and time_work[3] < time_late) or not (time_work[2] > time_early and time_work[3] > time_early):
                                key = "{} - {}".format(str(attendance_cv.date()), wk.id)
                                str_check_out = "{} {}".format(str(time_work[1]).split(' ')[0],
                                                               self.convert_float_to_time(item.resource_calendar.attendance_ids[1].hour_to))
                                if time_work[2] < item.resource_calendar.attendance_ids[1].hour_to:
                                    if time_work[3] <= item.resource_calendar.attendance_ids[1].hour_to:
                                        ce = str(time_work[1])
                                    else:
                                        ce = str_check_out
                                    arr_full.append({key: {
                                        'check_in': str(time_work[0]),
                                        'check_out': ce,
                                        'check_in_full': time_work[2],
                                        'check_out_full': time_work[3],
                                        'time_late': time_late,
                                        'time_early': time_early,
                                        'employee_id': wk.id,
                                        'hour_from': item.resource_calendar.attendance_ids[0].hour_from,
                                        'hour_to': item.resource_calendar.attendance_ids[1].hour_to,
                                        'time_off': time_off,
                                        'hour_time_off_from': item.resource_calendar.attendance_ids[0].hour_to,
                                        'hour_time_off_to': item.resource_calendar.attendance_ids[1].hour_from
                                    }})
                                    arr_full_check.append(key)
                    # theo buổi
                    if sessions:
                        for item in sessions:
                            time_late_morning = item.resource_calendar.attendance_ids[0].hour_from + float(setting_time[0]) / 60
                            time_early_morning = item.resource_calendar.attendance_ids[0].hour_to - float(setting_time[1]) / 60
                            time_late_afternoon = item.resource_calendar.attendance_ids[1].hour_from + float(setting_time[0]) / 60
                            time_early_afternoon = item.resource_calendar.attendance_ids[1].hour_to - float(setting_time[1]) / 60
                            time_work = self.convert_time_work(attendance)
                            value = {
                                'check_in': str(time_work[0]),
                                'check_out': str(time_work[1]),
                                'check_in_full': time_work[2],
                                'check_out_full': time_work[3],
                                'time_early_morning': time_early_morning,
                                'time_late_morning': time_late_morning,
                                'time_early_afternoon': time_early_afternoon,
                                'time_late_afternoon': time_late_afternoon,
                                'employee_id': wk.id
                            }
                            # sáng
                            if item.types_resource_calendar.code == 'morning' or item.types_resource_calendar.code == 'full_day':
                                if not (time_work[2] < time_late_morning and time_work[3] < time_late_morning) or not (time_work[2] > time_early_morning and time_work[3] > time_early_morning):
                                    if time_work[3] <= time_early_morning or time_early_morning <= time_work[3] < time_late_afternoon:
                                        key = "{} - {}".format(str(attendance_cv.date()), wk.id)
                                        value.update({
                                            'hour_from': item.resource_calendar.attendance_ids[0].hour_from,
                                            'hour_to': item.resource_calendar.attendance_ids[0].hour_to
                                        })
                                        arr_session_morning.append({key: value})
                                        arr_session_morning_check.append(key)
                            # chiều
                            if item.types_resource_calendar.code == 'afternoon' or item.types_resource_calendar.code == 'full_day':
                                if not (time_work[2] < time_late_afternoon and time_work[3] < time_late_afternoon) or not (
                                        time_work[2] > time_early_afternoon and time_work[3] > time_early_afternoon):
                                    if time_early_morning < time_work[2]:
                                        key = "{} - {}".format(str(attendance_cv.date()), wk.id)
                                        str_check_out = "{} {}".format(str(time_work[1]).split(' ')[0], self.convert_float_to_time(item.resource_calendar.attendance_ids[1].hour_to))
                                        if time_work[2] < item.resource_calendar.attendance_ids[1].hour_to:
                                            if time_work[3] <= item.resource_calendar.attendance_ids[1].hour_to:
                                                ce = str(time_work[1])
                                            else:
                                                ce = str_check_out
                                            value.update({
                                                'check_out': ce,
                                                'hour_from': item.resource_calendar.attendance_ids[1].hour_from,
                                                'hour_to': item.resource_calendar.attendance_ids[1].hour_to
                                            })
                                            arr_session_afternoon.append({key: value})
                                            arr_session_afternoon_check.append(key)
                    # theo ca
                    if shift:
                        for item in shift:
                            time_work = self.convert_time_work(attendance)
                            value_shift = {
                                'check_in': str(time_work[0]),
                                'check_out': str(time_work[1]),
                                'check_in_full': time_work[2],
                                'check_out_full': time_work[3],
                                'employee_id': wk.id,
                                'hour_from': item.types_resource_calendar.shift_type_id.start_time,
                                'hour_to': item.types_resource_calendar.shift_type_id.end_time,
                                'time_off': item.types_resource_calendar.shift_type_id.time_off,
                                'hour_time_off_from': item.types_resource_calendar.shift_type_id.time_off_from,
                                'hour_time_off_to': item.types_resource_calendar.shift_type_id.time_off_to
                            }
                            if item.types_resource_calendar.code == 'day_shift':
                                time_late_full_day = item.types_resource_calendar.shift_type_id.start_time + float(
                                    setting_time[0]) / 60
                                time_early_full_day = item.types_resource_calendar.shift_type_id.end_time - float(
                                    setting_time[1]) / 60
                                if not (time_work[2] < time_late_full_day and time_work[3] < time_late_full_day) or not (time_work[2] > time_early_full_day and time_work[3] > time_early_full_day):
                                    key = "{} - {}".format(str(attendance_cv.date()), wk.id)
                                    str_check_out = "{} {}".format(str(time_work[1]).split(' ')[0], self.convert_float_to_time(item.types_resource_calendar.shift_type_id.end_time))
                                    if time_work[2] < item.types_resource_calendar.shift_type_id.end_time:
                                        if time_work[3] <= item.types_resource_calendar.shift_type_id.end_time:
                                            ce = str(time_work[1])
                                        else:
                                            ce = str_check_out
                                        value_shift.update({
                                            'check_out': ce,
                                            'time_late': time_late_full_day,
                                            'time_early': time_early_full_day,
                                        })
                                        arr_shift_full.append({key: value_shift})
                                        arr_shift_full_check.append(key)
                            if item.types_resource_calendar.code == 'night_shift':
                                time_late_full_day = item.types_resource_calendar.shift_type_id.start_time + float(
                                    setting_time[0]) / 60
                                time_early_full_day = item.types_resource_calendar.shift_type_id.end_time - float(
                                    setting_time[1]) / 60
                                if item.start_date == attendance_cv.date():
                                    time_work = self.convert_time_work(attendance)
                                    str_check_out = "{} {}".format(str(time_work[1]).split(' ')[0], self.convert_float_to_time(item.types_resource_calendar.shift_type_id.end_time))

                                    if item.types_resource_calendar.shift_type_id.end_time <= time_work[2] <= item.types_resource_calendar.shift_type_id.start_time:
                                        key = "{} - {} - {}".format(item.employee_id.id, item.start_date, item.end_date)
                                        if time_work[3] <= item.types_resource_calendar.shift_type_id.end_time:
                                            ce = str(time_work[1])
                                        else:
                                            ce = str_check_out
                                        value_shift.update({
                                            'check_in': str(attendance_cv),
                                            'check_out': ce,
                                            'time_late': time_late_full_day,
                                            'time_early': time_early_full_day,
                                            'flag_overnight': False
                                        })
                                        arr_1.append({key: value_shift})
                                        arr_shift_over_night_check.append(key)
                                if item.end_date == attendance_out.date():
                                    # print(item.end_date)
                                    time_work = self.convert_time_work(attendance)
                                    str_check_out = "{} {}".format(str(time_work[1]).split(' ')[0], self.convert_float_to_time(item.types_resource_calendar.shift_type_id.end_time))

                                    if item.types_resource_calendar.shift_type_id.end_time <= time_work[3] <= item.types_resource_calendar.shift_type_id.start_time:
                                        key = "{} - {} - {}".format(item.employee_id.id, item.start_date, item.end_date)
                                        if time_work[3] <= item.types_resource_calendar.shift_type_id.end_time:
                                            ce = str(time_work[1])
                                        else:
                                            ce = str_check_out
                                        value_shift.update({
                                            'check_in': str(attendance_cv),
                                            'check_out': ce,
                                            'time_late': time_late_full_day,
                                            'time_early': time_early_full_day,
                                            'flag_overnight': False
                                        })
                                        arr_2.append({key: value_shift})
                                        arr_shift_over_night_check.append(key)
                                    if time_work[3] < item.types_resource_calendar.shift_type_id.end_time:
                                        key = "{} - {} - {}".format(item.employee_id.id, item.start_date, item.end_date)
                                        if time_work[3] <= item.types_resource_calendar.shift_type_id.end_time:
                                            ce = str(time_work[1])
                                        else:
                                            ce = str_check_out
                                        value_shift.update({
                                            'check_in': str(attendance_cv),
                                            'check_out': ce,
                                            'time_late': time_late_full_day,
                                            'time_early': time_early_full_day,
                                            'flag_overnight': False
                                        })
                                        arr_3.append({key: value_shift})
                                        arr_shift_over_night_check.append(key)

                                # trường hợp ca đêm chỉ làm trong ngày là về
                                if item.start_date == attendance_cv.date() and item.start_date == attendance_out.date():
                                    time_work = self.convert_time_work(attendance)
                                    str_check_out = "{} {}".format(str(time_work[1]).split(' ')[0], self.convert_float_to_time(
                                        item.types_resource_calendar.shift_type_id.end_time))
                                    key = "{} - {} - {}".format(item.employee_id.id, item.start_date, item.start_date)
                                    # if time_work[3] <= item.types_resource_calendar.shift_type_id.end_time:
                                    #     ce = str(time_work[1])
                                    # else:
                                    #     ce = str_check_out
                                    value_shift.update({
                                        'check_in': str(attendance_cv),
                                        'check_out': str(time_work[1]),
                                        'time_late': time_late_full_day,
                                        'time_early': time_early_full_day,
                                        'flag_overnight': True
                                    })
                                    arr_4.append({key: value_shift})
                                    arr_shift_over_night_check.append(key)

        arr_res_full = self.set_time_worker(arr_full, arr_full_check)
        arr_res_session_morning = self.set_time_worker(arr_session_morning, arr_session_morning_check)
        arr_res_session_afternoon = self.set_time_worker(arr_session_afternoon, arr_session_afternoon_check)
        arr_shift_full = self.set_time_worker(arr_shift_full, arr_shift_full_check)
        # arr_res_shift_overnight = self.set_time_worker(arr_shift_over_night, arr_shift_over_night_check)
        #  Theo ngày
        result_full = self.full_day_worker(arr_res_full, 'full_day_session')
        # print(result_full)
        # Theo buổi
        session_morning_worker = self.set_session_worker(arr_res_session_morning, 'morning')
        session_afternoon_worker = self.set_session_worker(arr_res_session_afternoon, 'afternoon')
        # Theo ca ngày
        result_shift_full = self.full_day_worker(arr_shift_full, 'shift_full')
        # Theo ca tối
        # print('arr1', arr_1 + arr_2)
        arr_shift_overnight = self.set_time_worker(arr_1 + arr_2 + arr_3 + arr_4, arr_shift_over_night_check)
        result_shift_overnight = self.full_day_worker(arr_shift_overnight, '')
        return session_morning_worker, session_afternoon_worker, result_full, result_shift_full, result_shift_overnight

    def set_duration_time(self,date_start, date_stop, item):
        duration = ((24 + date_stop.minute / 60) - (date_start.hour + date_start.minute / 60)) - item \
            if date_stop.hour == 0 \
            else ((date_stop.hour + date_stop.minute / 60) -
                  (date_start.hour + date_start.minute / 60)) - item
        result_duration = duration - ((24 + date_stop.minute / 60) -
                                      (date_start.hour + date_start.minute / 60)) \
            if date_stop.hour == 0 \
            else duration - ((date_stop.hour + date_stop.minute / 60) -
                             (date_start.hour + date_start.minute / 60))
        return result_duration

    def create_work_entries_full_day(self):
        emp_full_date = self.set_work_entries_employee_office_full_day()
        work_entry = self.env['hr.work.entry']
        for item in emp_full_date:
            emp_name = self.env['hr.employee'].browse(item[0]['employee_id']).name
            hours_check_in = item[0]['date'] + " " + item[0]['hour_from']
            hours_check_out = item[0]['date'] + " " + item[0]['hour_to']
            date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            result_duration = self.set_duration_time(date_start, date_stop, item[0]['time_off'])
            minutes = result_duration * 60
            hours, minutes = divmod(minutes, 60)
            check = work_entry.search([]).mapped('name')
            check_name = 'WET/FULL/{}/{}'.format(item[0]['date'], emp_name)
            pht_duration = work_entry._get_duration(date_start, date_stop + timedelta(hours=hours, minutes=minutes))
            value = {
                'name': 'WET/FULL/{}/{}'.format(item[0]['date'], emp_name),
                'employee_id': item[0]['employee_id'],
                'work_entry_type_id': item[0]['work_entry_type_id'],
                'date_start': date_start,
                'date_stop': date_stop,
                'pht_duration': pht_duration,
                'state': 'draft',
                'date_attendance_start': item[0]['check_in'],
                'date_attendance_end': item[-1]['check_out']
            }
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    def create_work_entries_office_sessions_morning(self):
        emp_session = self.set_work_entries_employee_office_sessions()[0]
        if emp_session:
            self.create_work_entries_office_sessions(emp_session, 'MORNING')

    def create_work_entries_office_sessions_afternoon(self):
        emp_session = self.set_work_entries_employee_office_sessions()[1]
        if emp_session:
            self.create_work_entries_office_sessions(emp_session, 'AFTERNOON')

    def create_work_entries_office_sessions(self, emp_session, flag):
        work_entry = self.env['hr.work.entry']
        for item in emp_session:
            check_name = ''
            emp_name = self.env['hr.employee'].browse(item[0]['employee_id']).name
            hours_check_in = item[0]['date'] + " " + "{}".format(item[0]['hour_from'])
            hours_check_out = item[0]['date'] + " " + "{}".format(item[0]['hour_to'])
            date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
            check = work_entry.search([]).mapped('name')
            if flag == 'MORNING':
                check_name = 'WET/OFFICE/MORNING/{}/{}'.format(item[0]['date'], emp_name)
            if flag == 'AFTERNOON':
                check_name = 'WET/OFFICE/AFTERNOON/{}/{}'.format(item[0]['date'], emp_name)
            pht_duration = work_entry._get_duration(date_start, date_stop)
            value = {
                'name': check_name,
                'employee_id': item[0]['employee_id'],
                'work_entry_type_id': item[0]['work_entry_type_id'],
                'date_start': date_start,
                'date_stop': date_stop,
                'pht_duration': pht_duration,
                'state': 'draft',
                'date_attendance_start': item[0]['check_in'],
                'date_attendance_end': item[-1]['check_out']
            }
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    def create_work_entries_worker_full_day(self):
        emp_full_day = self.set_time_work_date_worker()[2]
        work_entry = self.env['hr.work.entry']
        for item in emp_full_day:
            emp_name = self.env['hr.employee'].browse(item['employee_id']).name
            check = work_entry.search([]).mapped('name')
            check_name = ''
            value = {
                'name': 'WET/WORKER/FULLDAY/{}/{}'.format(item['date'], emp_name),
                'employee_id': item['employee_id'],
                'work_entry_type_id': item['work_entry_type_id'],
                'state': 'draft'
            }
            if item['flag'] == 'full':
                hours_check_in = item['date'] + " " + "{}".format(item['hour_from'])
                hours_check_out = item['date'] + " " + "{}".format(item['hour_to'])
                date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_start = datetime.strptime(item['view_check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_end = datetime.strptime(item['view_check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)

                result_duration = self.set_duration_time(date_attendance_start, date_attendance_end, item['time_off'])
                minutes = result_duration * 60
                hours, minutes = divmod(minutes, 60)
                check_name = 'WET/WORKER/FULLDAY/{}/{}'.format(item['date'], emp_name)
                # pht_duration = work_entry._get_duration(date_attendance_start, date_attendance_end + timedelta(hours=hours, minutes=minutes))
                value.update({
                    'date_start': date_start,
                    'date_stop': date_stop + timedelta(hours=hours, minutes=minutes),
                    'date_attendance_start': date_attendance_start,
                    'date_attendance_end': date_attendance_end,
                    'pht_duration': 8
                })
            if item['flag'] == 'late' or item['flag'] == 'early' or item['flag'] == 'not_full':
                date_start = datetime.strptime(item['check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_stop = datetime.strptime(item['check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_start = datetime.strptime(item['view_check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_end = datetime.strptime(item['view_check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                result_duration = self.set_duration_time(date_attendance_start, date_attendance_end, item['time_off'])
                minutes = result_duration * 60
                hours, minutes = divmod(minutes, 60)
                check_name = 'WET/WORKER/FULLDAY/{}/{}'.format(item['date'], emp_name)
                pht_duration = work_entry._get_duration(date_attendance_start, date_attendance_end + timedelta(hours=hours, minutes=minutes))
                value.update({
                    'date_start': date_start,
                    'date_stop': date_stop + timedelta(hours=hours, minutes=minutes),
                    'date_attendance_start': date_attendance_start,
                    'date_attendance_end': date_attendance_end,
                    'pht_duration': pht_duration
                })
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    def create_work_entries_worker_sessions(self, emp_session, flag_session):
        emp_session = emp_session
        work_entry = self.env['hr.work.entry']
        for item in emp_session:
            emp_name = self.env['hr.employee'].browse(item['employee_id']).name
            check = work_entry.search([]).mapped('name')
            check_name = ''
            value = {
                'name': 'WET/WORKER/MORNING/{}/{}'.format(item['date'], emp_name)
                if flag_session == 'morning' else 'WET/WORKER/AFTERNOON/{}/{}'.format(item['date'], emp_name),
                'employee_id': item['employee_id'],
                'work_entry_type_id': item['work_entry_type_id'],
                'state': 'draft'
            }
            if item['flag'] == 'late' or item['flag'] == 'early' or item['flag'] == 'not_session':
                date_start = datetime.strptime(item['check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_stop = datetime.strptime(item['check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_start = datetime.strptime(item['view_check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_end = datetime.strptime(item['view_check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                check_name = 'WET/WORKER/MORNING/{}/{}'.format(item['date'], emp_name) \
                    if flag_session == 'morning' else 'WET/WORKER/AFTERNOON/{}/{}'.format(item['date'], emp_name)
                pht_duration = work_entry._get_duration(date_attendance_start, date_attendance_end)
                value.update({
                    'date_start': date_start,
                    'date_stop': date_stop,
                    'date_attendance_start': date_attendance_start,
                    'date_attendance_end': date_attendance_end,
                    'pht_duration': pht_duration
                })
            if item['flag'] == 'session':
                hours_check_in = item['date'] + " " + "{}".format(item['hour_from'])
                hours_check_out = item['date'] + " " + "{}".format(item['hour_to'])
                date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_start = datetime.strptime(item['view_check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_end = datetime.strptime(item['view_check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                check_name = 'WET/WORKER/MORNING/{}/{}'.format(item['date'], emp_name) \
                    if flag_session == 'morning' else 'WET/WORKER/AFTERNOON/{}/{}'.format(item['date'], emp_name)
                # pht_duration = work_entry._get_duration(date_attendance_start, date_attendance_end)
                value.update({
                    'date_start': date_start,
                    'date_stop': date_stop,
                    'date_attendance_start': date_attendance_start,
                    'date_attendance_end': date_attendance_end,
                    'pht_duration': 4
                })
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    def create_work_entries_worker_shift_full(self):
        emp_full_day = self.set_time_work_date_worker()[3]
        if emp_full_day:
            self.create_work_entries_worker_shift_all(emp_full_day, 'FULL')

    def create_work_entries_worker_shift_overnight(self):
        emp_overnight = self.set_time_work_date_worker()[4]
        if emp_overnight:
            self.create_work_entries_worker_shift_all(emp_overnight, 'OVERNIGHT')

    def create_work_entries_worker_shift_all(self, emp_full_all, flag):
        work_entry = self.env['hr.work.entry']
        for item in emp_full_all:
            emp_name = self.env['hr.employee'].browse(item['employee_id']).name
            check = work_entry.search([]).mapped('name')
            value = {
                'name': 'WET/WORKER/SHIFT/FULLDAY/{}/{}'.format(item['date'], emp_name) if flag == 'FULL' else 'WET/WORKER/SHIFT/OVERNIGHT/{}/{}'.format(item['date'], emp_name),
                'employee_id': item['employee_id'],
                'work_entry_type_id': item['work_entry_type_id'],
                'state': 'draft'
            }
            if item['flag'] == 'full':
                hours_check_in = item['date'] + " " + "{}".format(item['hour_from'])
                hours_check_out = item['date'] + " " + "{}".format(item['hour_to']) if flag == 'FULL' else item['date_to'] + " " + "{}".format(item['hour_to'])
                date_start = datetime.strptime(hours_check_in, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_stop = datetime.strptime(hours_check_out, '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_start = datetime.strptime(item['view_check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_end = datetime.strptime(item['view_check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                result_duration = self.set_duration_time(date_attendance_start, date_attendance_end, item['time_off'])
                minutes = result_duration * 60
                hours, minutes = divmod(minutes, 60)
                check_name = 'WET/WORKER/SHIFT/FULLDAY/{}/{}'.format(item['date'], emp_name) if flag == 'FULL' else 'WET/WORKER/SHIFT/OVERNIGHT/{}/{}'.format(item['date'], emp_name)
                pht_duration = work_entry._get_duration(date_attendance_start, date_attendance_end + timedelta(hours=hours, minutes=minutes))
                value.update({
                    'date_start': date_start,
                    'date_stop': date_stop + timedelta(hours=hours, minutes=minutes),
                    'date_attendance_start': date_attendance_start,
                    'date_attendance_end': date_attendance_end,
                    'pht_duration': 8
                })
            else:
                date_start = datetime.strptime(item['check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_stop = datetime.strptime(item['check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_start = datetime.strptime(item['view_check_in'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                date_attendance_end = datetime.strptime(item['view_check_out'], '%Y-%m-%d %H:%M:%S') - timedelta(hours=7, minutes=0)
                result_duration = self.set_duration_time(date_attendance_start, date_attendance_end, item['time_off'])
                minutes = result_duration * 60
                hours, minutes = divmod(minutes, 60)
                check_name = 'WET/WORKER/SHIFT/FULLDAY/{}/{}'.format(item['date'], emp_name) if flag == 'FULL' else 'WET/WORKER/SHIFT/OVERNIGHT/{}/{}'.format(item['date'], emp_name)
                pht_duration = work_entry._get_duration(date_attendance_start, date_attendance_end + timedelta(hours=hours, minutes=minutes))
                value.update({
                    'date_start': date_start,
                    'date_stop': date_stop + timedelta(hours=hours, minutes=minutes),
                    'date_attendance_start': date_attendance_start,
                    'date_attendance_end': date_attendance_end,
                    'pht_duration': pht_duration
                })
            if check:
                if check_name not in check:
                    work_entry.create(value)
            else:
                work_entry.create(value)

    def create_ot_work_entries(self):
        work_entry = self.env['hr.work.entry']
        employee = self.env['hr.employee'].search([
            ('contract_id.state', '=', 'open')
        ])
        overtime = self.env['hr.overtime'].search([]).filtered(lambda x: x.states == 'approved')
        flag_time = timedelta(hours=7, minutes=0)
        f_date = '%Y-%m-%d %H:%M:%S'
        holiday = self.env['day.off.year'].search([])
        for ot in overtime:
            res_ot = ot.detail_overtime_ids.filtered(lambda x: x.employee_id.id in employee.ids)
            for line in res_ot:
                if line.date_ot not in holiday.mapped('date'):
                    str_check_in = "{} {}".format(line.date_ot,self.convert_float_to_time(line.from_hours))
                    str_check_out = "{} {}".format(line.date_ot, self.convert_float_to_time(line.to_hours))
                    date_start = datetime.strptime(str_check_in, f_date) - flag_time
                    date_stop = datetime.strptime(str_check_out, f_date) - flag_time
                    check = work_entry.search([]).mapped('name')
                    check_name = 'WET/OT/{}/{}/{}'.format(line.id,line.date_ot, line.employee_id.name)
                    duration = ((24 + date_stop.minute / 60) - (date_start.hour + date_start.minute / 60)) * line.rate_salary \
                        if date_stop.hour == 0 \
                        else ((date_stop.hour + date_stop.minute / 60) -
                              (date_start.hour + date_start.minute / 60)) * line.rate_salary
                    result_duration = duration - ((24 + date_stop.minute / 60) -
                                                  (date_start.hour + date_start.minute / 60)) \
                        if date_stop.hour == 0 \
                        else duration - ((date_stop.hour + date_stop.minute / 60) -
                                         (date_start.hour + date_start.minute / 60))
                    minutes = result_duration * 60
                    hours, minutes = divmod(minutes, 60)
                    pht_duration = work_entry._get_duration(date_start, date_stop + timedelta(hours=hours, minutes=minutes))
                    value = {
                        'name': 'WET/OT/{}/{}/{}'.format(line.id,line.date_ot, line.employee_id.name),
                        'employee_id': line.employee_id.id,
                        'work_entry_type_id': line.overtime_type_id.work_entry_type_id.id,
                        'state': 'draft',
                        'date_start': date_start,
                        'date_stop': date_stop,
                        'date_attendance_start': date_start,
                        'date_attendance_end': date_stop,
                        'pht_duration': pht_duration
                    }
                    if check:
                        if check_name not in check:
                            work_entry.create(value)
                    else:
                        work_entry.create(value)
                else:
                    str_check_in = "{} {}".format(line.date_ot, self.convert_float_to_time(line.from_hours))
                    str_check_out = "{} {}".format(line.date_ot, self.convert_float_to_time(line.to_hours))
                    date_start = datetime.strptime(str_check_in, f_date) - flag_time
                    date_stop = datetime.strptime(str_check_out, f_date) - flag_time
                    check = work_entry.search([]).mapped('name')
                    check_name = 'WET/OT_HOLIDAY/{}/{}/{}'.format(line.id, line.date_ot, line.employee_id.name)
                    duration = ((24 + date_stop.minute / 60) - (date_start.hour + date_start.minute / 60)) * line.rate_salary \
                        if date_stop.hour == 0 \
                        else ((date_stop.hour + date_stop.minute / 60) -
                              (date_start.hour + date_start.minute / 60)) * line.rate_salary
                    result_duration = duration - ((24 + date_stop.minute / 60) -
                                                  (date_start.hour + date_start.minute / 60)) \
                        if date_stop.hour == 0 \
                        else duration - ((date_stop.hour + date_stop.minute / 60) -
                                         (date_start.hour + date_start.minute / 60))
                    minutes = result_duration * 60
                    hours, minutes = divmod(minutes, 60)
                    pht_duration = work_entry._get_duration(date_start, date_stop + timedelta(hours=hours, minutes=minutes))
                    value = {
                        'name': 'WET/OT_HOLIDAY/{}/{}/{}'.format(line.id, line.date_ot, line.employee_id.name),
                        'employee_id': line.employee_id.id,
                        'work_entry_type_id': line.overtime_type_id.work_entry_type_id.id,
                        'state': 'draft',
                        'date_start': date_start,
                        'date_stop': date_stop,
                        'date_attendance_start': date_start,
                        'date_attendance_end': date_stop,
                        'pht_duration': pht_duration
                    }
                    if check:
                        if check_name not in check:
                            work_entry.create(value)
                    else:
                        work_entry.create(value)

    def create_leaves_employee(self):
        # check leaves
        model_le = self.env['hr.leave'].sudo()
        hr_leave = model_le.search([])\
            .filtered(lambda x: x.state == 'validate' and x.employee_id.contract_id.state == 'open')
        work_entry = self.env['hr.work.entry']
        check = work_entry.search([]).mapped('name')
        for leave in hr_leave:
            # leave full day
            value = {
                'employee_id': leave.employee_id.id,
                'state': 'draft'
            }
            if not leave.request_unit_half and not leave.request_unit_hours:
                # trong ngày
                leave_in = datetime.strptime(str(leave.request_date_from) + " " + "7:00:00", '%Y-%m-%d %H:%M:%S') \
                           - timedelta(hours=7, minutes=0)
                leave_out = datetime.strptime(str(leave.request_date_to) + " " + "17:00:00", '%Y-%m-%d %H:%M:%S') \
                            - timedelta(hours=7, minutes=0)
                if leave_in.date() == leave_out.date():
                    result_duration = self.set_duration_time(leave_in, leave_out, 1.5)
                    minutes = result_duration * 60
                    hours, minutes = divmod(minutes, 60)
                    if leave.holiday_status_id.work_entry_type_id.code == 'LEAVES_HOLIDAY':
                        check_name = 'WET/LEAVE_HOLIDAY/FULL/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                        type_leave = self.code_att_work_type().get('type_leave_holiday').id
                    else:
                        check_name = 'WET/UNPAID/FULL/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                        type_leave = self.code_att_work_type().get('type_unpaid_leave_normal').id
                    value.update({
                        'name': check_name,
                        'date_start': leave_in,
                        'date_stop': leave_out,
                        'date_attendance_start': leave_in,
                        'date_attendance_end': leave_out,
                        'pht_duration': 8,
                        'work_entry_type_id': type_leave,
                    })
                    if check:
                        if check_name not in check:
                            new_wet = work_entry.create(value)
                    else:
                        new_wet = work_entry.create(value)
                # nhiều ngày
                else:
                    result_duration = self.set_duration_time(leave_in, leave_out, 1.5)
                    minutes = result_duration * 60
                    hours, minutes = divmod(minutes, 60)
                    if leave.holiday_status_id.work_entry_type_id.code == 'LEAVES_HOLIDAY':
                        check_name = 'WET/LEAVE_HOLIDAY/FULL/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                        type_leave = self.code_att_work_type().get('type_leave_holiday').id
                    else:
                        check_name = 'WET/UNPAID/FULL/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                        type_leave = self.code_att_work_type().get('type_unpaid_leave_normal').id
                    value.update({
                        'name': check_name,
                        'date_start': leave_in,
                        'date_stop': leave_out,
                        'date_attendance_start': leave_in,
                        'date_attendance_end': leave_out,
                        'pht_duration': [int(s) for s in leave.duration_display.split() if s.isdigit()][0],
                        'work_entry_type_id': type_leave,
                    })
                    if check:
                        if check_name not in check:
                            new_wet = work_entry.create(value)
                    else:
                        new_wet = work_entry.create(value)

            else:
                if leave.request_unit_half:
                    if leave.request_date_from_period == 'am':
                        leave_in = datetime.strptime(str(leave.request_date_from) + " " + "7:00:00", '%Y-%m-%d %H:%M:%S') \
                                   - timedelta(hours=7, minutes=0)
                        leave_out = datetime.strptime(str(leave.request_date_to) + " " + "11:00:00", '%Y-%m-%d %H:%M:%S') \
                                    - timedelta(hours=7, minutes=0)
                        result_duration = self.set_duration_time(leave_in, leave_out, 0)
                        minutes = result_duration * 60
                        hours, minutes = divmod(minutes, 60)
                        if leave.holiday_status_id.work_entry_type_id.code == 'LEAVES_HOLIDAY':
                            check_name = 'WET/LEAVE_HOLIDAY/FULL/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                            type_leave = self.code_att_work_type().get('type_leave_holiday').id
                        else:
                            check_name = 'WET/UNPAID/SESSION/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                            type_leave = self.code_att_work_type().get('type_unpaid_leave_normal').id
                        value.update({
                            'name': check_name,
                            'date_start': leave_in,
                            'date_stop': leave_out + timedelta(hours=hours, minutes=minutes),
                            'date_attendance_start': leave_in,
                            'date_attendance_end': leave_out,
                            'pht_duration': 4,
                            'work_entry_type_id': type_leave,
                        })
                        if check:
                            if check_name not in check:
                                new_wet = work_entry.create(value)
                        else:
                            new_wet = work_entry.create(value)
                    else:
                        leave_in = datetime.strptime(str(leave.request_date_from) + " " + "13:00:00", '%Y-%m-%d %H:%M:%S') \
                                   - timedelta(hours=7, minutes=0)
                        leave_out = datetime.strptime(str(leave.request_date_to) + " " + "17:00:00", '%Y-%m-%d %H:%M:%S') \
                                    - timedelta(hours=7, minutes=0)
                        result_duration = self.set_duration_time(leave_in, leave_out, 0)
                        minutes = result_duration * 60
                        hours, minutes = divmod(minutes, 60)
                        if leave.holiday_status_id.work_entry_type_id.code == 'LEAVES_HOLIDAY':
                            check_name = 'WET/LEAVE_HOLIDAY/FULL/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                            type_leave = self.code_att_work_type().get('type_leave_holiday').id
                        else:
                            check_name = 'WET/UNPAID/SESSION/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                            type_leave = self.code_att_work_type().get('type_unpaid_leave_normal').id
                        value.update({
                            'name': check_name,
                            'date_start': leave_in,
                            'date_stop': leave_out + timedelta(hours=hours, minutes=minutes),
                            'date_attendance_start': leave_in,
                            'date_attendance_end': leave_out,
                            'pht_duration': 4,
                            'work_entry_type_id': type_leave,
                        })
                        if check:
                            if check_name not in check:
                                new_wet = work_entry.create(value)
                        else:
                            new_wet = work_entry.create(value)
                if leave.request_unit_hours:
                    txt_in = self.convert_float_to_time(float(leave.request_hour_from))
                    txt_out = self.convert_float_to_time(float(leave.request_hour_to))
                    leave_in = datetime.strptime(str(leave.request_date_from) + " " + txt_in, '%Y-%m-%d %H:%M:%S') \
                               - timedelta(hours=7, minutes=0)
                    leave_out = datetime.strptime(str(leave.request_date_to) + " " + txt_out, '%Y-%m-%d %H:%M:%S') \
                                - timedelta(hours=7, minutes=0)
                    result_duration = self.set_duration_time(leave_in, leave_out, 0)
                    minutes = result_duration * 60
                    hours, minutes = divmod(minutes, 60)
                    pht_duration = work_entry._get_duration(leave_in, leave_out + timedelta(hours=hours, minutes=minutes))
                    if leave.holiday_status_id.work_entry_type_id.code == 'LEAVES_HOLIDAY':
                        check_name = 'WET/LEAVE_HOLIDAY/FULL/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                        type_leave = self.code_att_work_type().get('type_leave_holiday').id
                    else:
                        check_name = 'WET/UNPAID/HOURS/{}/{}/{}'.format(str(leave.request_date_from), str(leave.request_date_to), leave.employee_id.name)
                        type_leave = self.code_att_work_type().get('type_unpaid_leave_normal').id
                    value.update({
                        'name': check_name,
                        'date_start': leave_in,
                        'date_stop': leave_out + timedelta(hours=hours, minutes=minutes),
                        'date_attendance_start': leave_in,
                        'date_attendance_end': leave_out,
                        'pht_duration': pht_duration,
                        'work_entry_type_id': type_leave,
                    })
                    if check:
                        if check_name not in check:
                            new_wet = work_entry.create(value)
                    else:
                        new_wet = work_entry.create(value)

    @api.model
    def generate_attendance_to_work_entries(self):
        # tính công nhân viên văn phòng full ngày
        self.create_work_entries_full_day()
        # tính công nhân viên văn phòng theo buổi
        self.create_work_entries_office_sessions_morning()
        self.create_work_entries_office_sessions_afternoon()
        # tính công cho công nhân
        self.create_work_entries_worker_full_day()
        self.create_work_entries_worker_sessions(self.set_time_work_date_worker()[0], 'morning')
        self.create_work_entries_worker_sessions(self.set_time_work_date_worker()[1], 'afternoon')
        # tính công cho công nhân theo ca ngày và ca đêm
        self.create_work_entries_worker_shift_full()
        self.create_work_entries_worker_shift_overnight()
        # tính ot cho nhân viên
        self.create_ot_work_entries()
        # tính ngày nghỉ thường
        self.create_leaves_employee()

    def convert_float_to_time(self, line):
        td = timedelta(hours=float(line))
        dt = datetime.min + td
        time_float = "{:%H:%M}:00".format(dt)
        return time_float

    def _mark_conflicting_work_entries(self, start, stop):
        """
        Set `state` to `conflict` for overlapping work entries
        between two dates.
        If `self.ids` is truthy then check conflicts with the corresponding work entries.
        Return True if overlapping work entries were detected.
        """
        # Use the postgresql range type `tsrange` which is a range of timestamp
        # It supports the intersection operator (&&) useful to detect overlap.
        # use '()' to exlude the lower and upper bounds of the range.
        # Filter on date_start and date_stop (both indexed) in the EXISTS clause to
        # limit the resulting set size and fasten the query.
        self.flush(['date_start', 'date_stop', 'employee_id', 'active'])
        query = """
            SELECT b1.id,
                   b2.id
              FROM hr_work_entry b1
              JOIN hr_work_entry b2
                ON b1.employee_id = b2.employee_id
               AND b1.id <> b2.id
             WHERE b1.date_start <= %(stop)s
               AND b1.date_stop >= %(start)s
               AND b1.active = TRUE
               AND b2.active = TRUE
               AND tsrange(b1.date_start, b1.date_stop, '()') && tsrange(b2.date_start, b2.date_stop, '()')
               AND {}
        """.format("b2.id IN %(ids)s" if self.ids else "b2.date_start <= %(stop)s AND b2.date_stop >= %(start)s")
        self.env.cr.execute(query, {"stop": stop, "start": start, "ids": tuple(self.ids)})
        conflicts = set(itertools.chain.from_iterable(self.env.cr.fetchall()))
        if conflicts:
            wet_leave = self.env['hr.work.entry'].sudo().browse(conflicts).\
                filtered(lambda x: x.work_entry_type_id.code == 'LEAVE90')
            if len(wet_leave.ids) >= 1:
                wet_not_leave = self.env['hr.work.entry'].sudo().browse(conflicts).filtered(
                    lambda x: x.work_entry_type_id.code != 'LEAVE90')
                # gom tất cả các check in trong ngày đó
                arr_wet = []
                for item in wet_not_leave:
                    date_attendance_start = datetime.strptime(str(item.date_attendance_start), '%Y-%m-%d %H:%M:%S') \
                               + timedelta(hours=7, minutes=0)
                    date_attendance_end = datetime.strptime(str(item.date_attendance_end), '%Y-%m-%d %H:%M:%S') \
                               + timedelta(hours=7, minutes=0)
                    arr_wet.append(date_attendance_start)
                    arr_wet.append(date_attendance_end)
                new_arr = sorted(arr_wet)
                leave_special = self.env['hr.work.entry.type'].search([('code', '=', 'LEAVE90SP')], limit=1)
                for item_leave in wet_leave:
                    leave_start = datetime.strptime(str(item_leave.date_attendance_start), '%Y-%m-%d %H:%M:%S') \
                                            + timedelta(hours=7, minutes=0)
                    leave_end = datetime.strptime(str(item_leave.date_attendance_end), '%Y-%m-%d %H:%M:%S') \
                                          + timedelta(hours=7, minutes=0)
                    if (leave_start > new_arr[0] and leave_end > new_arr[0]) \
                            and (leave_start < new_arr[-1] and leave_end < new_arr[-1]):
                        item_leave.work_entry_type_id = leave_special.id
                        check = self.env['hr.work.entry'].search([]).mapped('name')
                        txt_name = 'WET/UNPAID/HOURS/{}/{}/{}'.format(leave_start.date(), leave_end.date(), item_leave.employee_id.name)
                        if txt_name not in check:
                            item_leave.name = txt_name
        self.browse(conflicts).write({
            'state': 'conflict',
        })
        return bool(conflicts)
