# Copyright 2018-2020 Akretion France (http://www.akretion.com)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "PDT WORK ENTRY",
    "version": "14.0.1.0.0",
    "category": "CONTRACT MANAGEMENT",
    "license": "AGPL-3",
    "summary": "Technical module to generate PDF invoices with " "embedded XML file",
    "author": "Akretion,Onestein,Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://github.com/OCA/edi",
    "depends": ["base", "hr", "base_setup", "hr_attendance", 'pdt_shift_management', 'pdt_hr_day_off', 'resource', 'hr_payroll'],
    "data": [
        "views/res_setting.xml",
        "views/hr_employee.xml",
        "views/hr_leave_view.xml",
        "views/resource_calendar.xml",
        "views/hr_booking_worker.xml",
        "views/type_resource_calendar.xml",
        # "data/work_entry_type.xml",
        "data/cron.xml",
        # "views/template.xml",
        "views/hr_work_entry.xml",
        "security/ir.model.access.csv",
        ],
    # 'qweb': ['static/src/xml/attendance_work_entry.xml'],
    'assets': {
        'web.assets_backend': [
            '/pdt_work_entry/static/src/js/attendance_work_entry.js',
        ],
        'web.assets_qweb': [
            '/pdt_work_entry/static/src/xml/attendance_work_entry.xml',
        ],
    },
    "installable": True,
}
