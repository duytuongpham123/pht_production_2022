# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Field Partner',
    'version': '15.0.0',
    'category': 'Sales',
    'summary': 'PDT',
    "description": """
       Pham Duy Tuong
    """,
    'author': 'PDT',
    'depends': ['base'],
    'data': [
        'views/res_partner.xml'
    ],
    'demo': [],
    # 'assets': {
    #     'web.assets_backend': [
    #     ],
    #     'web.assets_qweb': [
    #
    #     ],
    # },
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
