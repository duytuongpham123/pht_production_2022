# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import defaultdict
from datetime import datetime, date, time
from dateutil.relativedelta import relativedelta
import pytz

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools import format_date


class InheritHrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'
    _description = 'Generate payslips for all selected employees'

    def get_allowance_ids(self, payslips):
        for child in payslips:
            if child.contract_id:
                if not child.hr_allowance_ids:
                    arr = []
                    for line in child.contract_id.hr_allowance_ids:
                        format_date = '%Y-%m-%d'
                        time_from_date = datetime.strptime('{}'.format(line.from_date), format_date)
                        time_to_date = datetime.strptime('{}'.format(line.to_date), format_date)
                        time_date_to = datetime.strptime('{}'.format(child.date_to), format_date)
                        time_date_from = datetime.strptime('{}'.format(child.date_from), format_date)
                        if time_from_date <= time_date_to and time_to_date >= time_date_from:
                            value = {
                                'employee_id': line.employee_id.id,
                                'allowance_id': line.allowance_id.id,
                                'money': line.money,
                                'from_date': line.from_date,
                                'to_date': line.to_date,
                                'type_salary': line.type_salary,
                                'payslip_id': child.id
                            }
                            arr.append(value)
                    self.env['hr.allowance.payslip'].create(arr)

    def get_salary_contribution(self, payslips):
        for child in payslips:
            if child.contract_id:
                arr = []
                salary_contribution = self.env['salary.contribution'].search([
                    ('contract_id', '=', child.contract_id.id),
                    ('state', 'in', ['confirmed', 'restore'])
                ])
                if not child.salary_contribution_ids:
                    for line in salary_contribution:
                        if line.contribution_type_id.code == 'BHXH':
                            child.social_insurance_employee = line.employee_rate * line.insurance_salary / 100
                            child.social_insurance_company = line.company_rate * line.insurance_salary / 100
                        elif line.contribution_type_id.code == 'BHYT':
                            child.health_insurance_employee = line.employee_rate * line.insurance_salary / 100
                            child.health_insurance_company = line.company_rate * line.insurance_salary / 100
                        elif line.contribution_type_id.code == 'BHTN':
                            child.unemployment_insurance_employee = line.employee_rate * line.insurance_salary / 100
                            child.unemployment_insurance_company = line.company_rate * line.insurance_salary / 100
                        elif line.contribution_type_id.code == 'KPCD':
                            child.union_funds_employee = line.employee_rate * line.insurance_salary / 100
                            child.union_funds_company = line.company_rate * line.insurance_salary / 100
                        child.total_cost_employee = child.social_insurance_employee + child.health_insurance_employee \
                                                   + child.unemployment_insurance_employee + child.union_funds_employee
                        child.total_cost_company = child.social_insurance_company + child.health_insurance_company \
                                                  + child.unemployment_insurance_company + child.union_funds_company
                        # get list data contribution
                        value = {
                            'contribution_type_id': line.contribution_type_id.id,
                            'insurance_salary': line.insurance_salary,
                            'employee_rate': line.employee_rate,
                            'company_rate': line.company_rate,
                            'to_date': line.to_date,
                            'from_date': line.from_date,
                            'state': dict(line._fields['state'].selection).get(line.state),
                            'payslip_id': child.id
                        }
                        arr.append(value)
                    self.env['salary.contribution.payslip'].create(arr)

    def compute_sheet(self):
        self.ensure_one()
        if not self.env.context.get('active_id'):
            from_date = fields.Date.to_date(self.env.context.get('default_date_start'))
            end_date = fields.Date.to_date(self.env.context.get('default_date_end'))
            today = fields.date.today()
            first_day = today + relativedelta(day=1)
            last_day = today + relativedelta(day=31)
            if from_date == first_day and end_date == last_day:
                batch_name = from_date.strftime('%B %Y')
            else:
                batch_name = _('From %s to %s', format_date(self.env, from_date), format_date(self.env, end_date))
            payslip_run = self.env['hr.payslip.run'].create({
                'name': batch_name,
                'date_start': from_date,
                'date_end': end_date,
            })
        else:
            payslip_run = self.env['hr.payslip.run'].browse(self.env.context.get('active_id'))

        employees = self.with_context(active_test=False).employee_ids
        if not employees:
            raise UserError(_("You must select employee(s) to generate payslip(s)."))

        #Prevent a payslip_run from having multiple payslips for the same employee
        employees -= payslip_run.slip_ids.employee_id
        success_result = {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.payslip.run',
            'views': [[False, 'form']],
            'res_id': payslip_run.id,
        }
        if not employees:
            return success_result

        payslips = self.env['hr.payslip']
        Payslip = self.env['hr.payslip']

        contracts = employees._get_contracts(
            payslip_run.date_start, payslip_run.date_end, states=['open', 'close']
        ).filtered(lambda c: c.active)
        contracts._generate_work_entries(payslip_run.date_start, payslip_run.date_end)
        work_entries = self.env['hr.work.entry'].search([
            ('date_start', '<=', payslip_run.date_end),
            ('date_stop', '>=', payslip_run.date_start),
            ('employee_id', 'in', employees.ids),
        ])
        self._check_undefined_slots(work_entries, payslip_run)

        if(self.structure_id.type_id.default_struct_id == self.structure_id):
            work_entries = work_entries.filtered(lambda work_entry: work_entry.state != 'validated')
            if work_entries._check_if_error():
                return {
                    'type': 'ir.actions.client',
                    'tag': 'display_notification',
                    'params': {
                        'title': _('Some work entries could not be validated.'),
                        'sticky': False,
                    }
                }
        default_values = Payslip.default_get(Payslip.fields_get())
        payslips_vals = []
        for contract in contracts:
            values = dict(default_values, **{
                'name': _('New Payslip'),
                'employee_id': contract.employee_id.id,
                'credit_note': payslip_run.credit_note,
                'payslip_run_id': payslip_run.id,
                'date_from': payslip_run.date_start,
                'date_to': payslip_run.date_end,
                'contract_id': contract.id,
                'struct_id': self.structure_id.id or contract.structure_type_id.default_struct_id.id,
            })
            payslips_vals.append(values)
        payslips = Payslip.with_context(tracking_disable=True).create(payslips_vals)
        payslips._compute_name()
        payslips.compute_sheet()
        self.get_allowance_ids(payslips)
        self.get_salary_contribution(payslips)
        payslip_run.state = 'verify'

        return success_result
