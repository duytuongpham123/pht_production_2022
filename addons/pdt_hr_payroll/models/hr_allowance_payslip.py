from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class HrAllowancePayslip(models.Model):
    _name = "hr.allowance.payslip"

    employee_id = fields.Many2one('hr.employee', 'Nhân viên')
    allowance_id = fields.Many2one('allowance.all', 'Phụ cấp')
    money = fields.Float('Số tiền')
    from_date = fields.Date('Từ ngày')
    to_date = fields.Date('Đến ngày')
    type_salary = fields.Char('Cách tính lương')
    payslip_id = fields.Many2one('hr.payslip')