# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from . import hr_payslip
from . import hr_allowance_payslip
from . import salary_contribution_payslip
from . import hr_payslip_employees
from . import annual_leave_allowance