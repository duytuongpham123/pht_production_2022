from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError

class SalaryContributionPayslip(models.Model):
    _name = "salary.contribution.payslip"

    contribution_type_id = fields.Many2one('contribution.type')
    insurance_salary = fields.Float('Mức đóng góp')
    employee_rate = fields.Float('Tỉ lệ nhân viên đóng')
    company_rate = fields.Float('Tỉ lệ công ty đóng')
    to_date = fields.Date('Từ ngày')
    from_date = fields.Date('Đến ngày')
    state = fields.Char('Trạng thái')
    payslip_id = fields.Many2one('hr.payslip')