from odoo import api, models, fields, _
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, date, timedelta


class HrAnnualLeaveAllowance(models.Model):
    _name = "hr.annual.leave.allowance"

    employee_id = fields.Many2one('hr.employee', string='Employee')
    allowance_leave = fields.Integer(string='Allowance Leaves')
    date_import = fields.Date(string='Date')
    code = fields.Char(string='Code')

    @api.model
    def create(self, vals_list):
        if 'employee_id' not in vals_list:
            check = self.env['hr.employee'].search([
                ('code', '=', vals_list['code'])
            ])
            if check:
                vals_list['employee_id'] = check.id
            else:
                raise UserError(_("Mã code %s không tồn tại!") % vals_list['code'])
        return super(HrAnnualLeaveAllowance, self).create(vals_list)

    @api.constrains('employee_id')
    def _check_employee_id(self):
        for record in self:
            check = self.search([
                ('id', '!=', record.id),
                ('employee_id', '=', record.employee_id.id)
            ])
            for i in check:
                if i.date_import.month == record.date_import.month and i.date_import.year == record.date_import.year:
                   raise UserError(_("You can not create more than 1 record for Employee %s for 1 month in year") % record.employee_id.name)
