from odoo import api, models, fields, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
from odoo.exceptions import UserError, ValidationError, RedirectWarning

class HrPayslip(models.Model):
    _inherit = "hr.payslip"

    hr_allowance_ids = fields.One2many('hr.allowance.payslip', 'payslip_id', readonly=True)
    social_insurance_employee = fields.Float('BHXH')
    health_insurance_employee = fields.Float('BHYT')
    unemployment_insurance_employee = fields.Float('BHTN')
    union_funds_employee = fields.Float('Kinh phí công đoàn')
    total_cost_employee = fields.Float('Tổng cộng')
    social_insurance_company = fields.Float('BHXH')
    health_insurance_company = fields.Float('BHYT')
    unemployment_insurance_company = fields.Float('BHTN')
    union_funds_company = fields.Float('Kinh phí công đoàn')
    total_cost_company = fields.Float('Tổng cộng')
    salary_contribution_ids = fields.One2many('salary.contribution.payslip', 'payslip_id', readonly=True)
    other_income_ids = fields.Many2many('other.income', 'payslip_income_rel', 'payslip_id', 'income_id', string='Income')
    other_deductions_ids = fields.Many2many('other.income', 'payslip_deduct_rel', 'payslip_id', 'deduct_id', string='Deduct')
    seniority_month = fields.Float('Seniority (month)', compute='compute_month', store=True)
    work_month_in_year = fields.Float('Working months in year', compute='compute_month_year', store=True)
    cut_off_date = fields.Integer('Cut off Date')
    total_allowance = fields.Float('Total Allowance', compute='compute_allowance', store=False)
    base_salary = fields.Float('Base Salary', readonly=1)
    flag = fields.Boolean(compute='compute_flag_13', store=True)
    debt_last_month = fields.Integer(string='Debt Last Month')
    advance_payment_ids = fields.One2many('salary.management', 'payslip_id')
    annual_leaves = fields.Float(string='Annual Leaves', compute='_compute_annual_leave', store=True)

    @api.depends('employee_id', 'contract_id', 'struct_id', 'date_from', 'date_to')
    def _compute_worked_days_line_ids(self):
        res = super(HrPayslip, self)._compute_worked_days_line_ids()
        print(res)
        return res

    @api.depends('employee_id', 'date_from', 'date_to')
    def _compute_annual_leave(self):
        for record in self:
            if record.employee_id and record.date_from and record.date_to:
                annual_leave = self.env['hr.annual.leave.allowance'].search([
                    ('employee_id', '=', record.employee_id.id),
                    ('date_import', '>=', record.date_from),
                    ('date_import', '<=', record.date_to)
                ])
                record.annual_leaves = sum(i.allowance_leave for i in annual_leave)
            else:
                record.annual_leaves = 0

    def get_advance_payment(self):
        for record in self:
            salary_management = self.env['salary.management'].search([
                ('employee_id', '=', record.employee_id.id),
                ('state', '=', 'advanced'),
                ('advance_date', '>=', record.date_from),
                ('advance_date', '<=', record.date_to)
            ])
            for i in salary_management:
                i.write({
                    'payslip_id': record.id
                })

    @api.depends('struct_id')
    def compute_flag_13(self):
        for line in self:
            if line.struct_id.code == 'SALARY_13_PHT':
                line.flag = True
            else:
                line.flag = False

    @api.constrains('date_from', 'date_to', 'struct_id')
    def constrains_date(self):
        if self.struct_id.code == 'SALARY_13_PHT':
            format_date = '%Y-%m-%d'
            time1 = datetime.strptime(str(self.date_from), format_date)
            time2 = datetime.strptime(str(self.date_to), format_date)
            if time1.day != 1 or time1.month != 1:
                raise UserError('Vui lòng  chọn đúng ngày bắt đầu')
            if time2.day != 31 or time2.month != 12:
                raise UserError('Vui lòng  chọn đúng ngày kêt thúc')

    @api.depends('contract_id','date_to', 'struct_id')
    def compute_month(self):
        for line in self:
            if line.struct_id.code == 'SALARY_13_PHT' and line.contract_id and line.date_to:
                format_date = '%Y-%m-%d'
                time1 = datetime.strptime(str(line.contract_id.date_start), format_date)
                time2 = datetime.strptime(str(line.date_to), format_date)
                num_months = (time2.year - time1.year) * 12 + (time2.month - time1.month)
                line.seniority_month = num_months
            else:
                line.seniority_month = 0

    @api.depends('employee_id')
    def compute_month_year(self):
        for child in self:
            hr_payslip = self.env['hr.payslip'].search([
                ('employee_id', '=', child.employee_id.id),
                ('state', '=', 'paid'),
                ('struct_id.code', '!=', 'SALARY_13_PHT')
            ])
            arr = []
            arr_contract = []
            for line in hr_payslip:
                format_date = '%Y-%m-%d'
                currentDateTime = datetime.now()
                date = currentDateTime.date()
                time1 = datetime.strptime(str(line.date_from), format_date)
                time_contract = datetime.strptime(str(line.contract_id.date_start), format_date)
                # if time1.year == date.year:
                if time_contract.day > 5:
                    arr_contract.append(time_contract.month)
                arr.append(time1.month)
            arr_new = [value for value in arr if value not in list(dict.fromkeys(arr_contract))]
            child.work_month_in_year = len(arr_new)

    def compute_allowance(self):
        for child in self:
            if child.contract_id.state == 'open':
                arr = []
                format_date = '%Y-%m-%d'
                currentDateTime = datetime.now()
                date = currentDateTime.date()
                for line in child.contract_id.hr_allowance_ids:
                    time12 = datetime.strptime('{}-12-1'.format(date.year), format_date)
                    time_to_date = datetime.strptime(str(line.to_date), format_date)
                    if time12 < time_to_date:
                        arr.append(line.money)
                if child.contract_id.salary_13 == True:
                    child.total_allowance = sum(arr)
                else:
                    child.total_allowance = 0
                if not child.contract_id.date_end:
                    child.base_salary = child.contract_id.wage
                else:
                    last_current_date = datetime.strptime('{}-12-31'.format(date.year), format_date)
                    end_date_contract = datetime.strptime(str(child.contract_id.date_end), format_date)
                    if end_date_contract >= last_current_date:
                        child.base_salary = child.contract_id.wage
                    else:
                        child.base_salary = 0
            else:
                child.total_allowance = 0

    @api.onchange('other_income_ids')
    def income(self):
        if self.other_income_ids:
            other = self.env['other.income'].search([('id', 'in', self.other_income_ids.ids)])
            for line_other in other:
                sql = """
                    select * from payslip_income_rel where income_id = """+ str(line_other.id) +"""
                """
                self.env.cr.execute(sql)
                res_all = self.env.cr.fetchall()
                for line in res_all:
                    if line[0]:
                        check = self.env['hr.payslip'].search([('id', '=', line[0])])
                        raise UserError('Đã tồn tại ở {}'.format(check.name))

    @api.onchange('other_deductions_ids')
    def deduct(self):
        if self.other_deductions_ids:
            other = self.env['other.income'].search([('id', 'in', self.other_deductions_ids.ids)])
            for line_deduct in other:
                sql = """
                        select * from payslip_deduct_rel where deduct_id = """ + str(line_deduct.id) + """
                    """
                self.env.cr.execute(sql)
                res_all = self.env.cr.fetchall()
                for line in res_all:
                    if line[0]:
                        check = self.env['hr.payslip'].search([('id', '=', line[0])])
                        raise UserError('Đã tồn tại ở {}'.format(check.name))

    def del_allowance_ids(self):
        if self.hr_allowance_ids:
            self.hr_allowance_ids.unlink()

    def del_contribution(self):
        if self.salary_contribution_ids:
            self.salary_contribution_ids.unlink()

    def del_income_and_deductions(self):
        if self.other_income_ids or self.other_deductions_ids:
            self.other_income_ids.unlink()
            self.other_deductions_ids.unlink()

    def get_allowance_ids(self):
        if self.contract_id:
            self.del_allowance_ids()
            if not self.hr_allowance_ids:
                arr = []
                for line in self.contract_id.hr_allowance_ids:
                    format_date = '%Y-%m-%d'
                    time_from_date = datetime.strptime('{}'.format(line.from_date), format_date)
                    time_to_date = datetime.strptime('{}'.format(line.to_date), format_date)
                    time_date_to = datetime.strptime('{}'.format(self.date_to), format_date)
                    time_date_from = datetime.strptime('{}'.format(self.date_from), format_date)
                    if time_from_date <= time_date_to and time_to_date >= time_date_from:
                        value = {
                            'employee_id': line.employee_id.id,
                            'allowance_id': line.allowance_id.id,
                            'money': line.money,
                            'from_date': line.from_date,
                            'to_date': line.to_date,
                            'type_salary': line.type_salary,
                            'payslip_id': self._origin.id
                        }
                        arr.append(value)
                self.env['hr.allowance.payslip'].create(arr)

    def get_salary_contribution(self):
        if self.contract_id:
            arr = []
            self.del_contribution()
            salary_contribution = self.env['salary.contribution'].search([
                ('contract_id', '=', self.contract_id.id),
                ('state', 'in', ['confirmed', 'restore'])
            ])
            if not self.salary_contribution_ids:
                for line in salary_contribution:
                    if line.contribution_type_id.code == 'BHXH':
                        self.social_insurance_employee = line.employee_rate * line.insurance_salary / 100
                        self.social_insurance_company = line.company_rate * line.insurance_salary / 100
                    elif line.contribution_type_id.code == 'BHYT':
                        self.health_insurance_employee = line.employee_rate * line.insurance_salary / 100
                        self.health_insurance_company = line.company_rate * line.insurance_salary / 100
                    elif line.contribution_type_id.code == 'BHTN':
                        self.unemployment_insurance_employee = line.employee_rate * line.insurance_salary / 100
                        self.unemployment_insurance_company = line.company_rate * line.insurance_salary / 100
                    elif line.contribution_type_id.code == 'KPCD':
                        self.union_funds_employee = line.employee_rate * line.insurance_salary / 100
                        self.union_funds_company = line.company_rate * line.insurance_salary / 100
                    self.total_cost_employee = self.social_insurance_employee + self.health_insurance_employee \
                                               + self.unemployment_insurance_employee + self.union_funds_employee
                    self.total_cost_company = self.social_insurance_company + self.health_insurance_company \
                                              + self.unemployment_insurance_company + self.union_funds_company
                    # get list data contribution
                    value = {
                        'contribution_type_id': line.contribution_type_id.id,
                        'insurance_salary': line.insurance_salary,
                        'employee_rate': line.employee_rate,
                        'company_rate': line.company_rate,
                        'to_date': line.to_date,
                        'from_date': line.from_date,
                        'state': dict(line._fields['state'].selection).get(line.state),
                        'payslip_id': self.id
                    }
                    arr.append(value)
                self.env['salary.contribution.payslip'].create(arr)

    @api.depends('employee_id', 'contract_id', 'struct_id', 'date_from', 'date_to')
    def _compute_worked_days_line_ids(self):
        res = super(HrPayslip, self)._compute_worked_days_line_ids()
        for rec in self:
            other_input = self.env['hr.payslip.input.type'].search([]).filtered(lambda x: rec.struct_id.id in x.struct_ids.ids)
            rec.input_line_ids = [(5, 0, 0)] + [(0, 0, {'input_type_id': item.id, 'name': item.name}) for item in other_input]
        return res


class SalaryManagement(models.Model):
    _inherit = "salary.management"

    payslip_id = fields.Many2one('hr.payslip')


class HrPayslipWorkedDays(models.Model):
    _inherit = 'hr.payslip.worked_days'

    @api.depends('is_paid', 'number_of_hours', 'payslip_id', 'payslip_id.normal_wage', 'payslip_id.sum_worked_hours',
                 'number_of_days')
    def _compute_amount(self):
        res = super(HrPayslipWorkedDays, self)._compute_amount()
        for worked_days in self.filtered(lambda wd: not wd.payslip_id.edited):
            if worked_days.payslip_id.contract_id.structure_type_id.id == self.env.ref('hr_contract.structure_type_worker').id:
                worked_days.amount = worked_days.number_of_hours * worked_days.payslip_id.contract_id.hourly_wage
        return res

class HrPayslipInput(models.Model):
    _inherit = "hr.payslip.input"

    input_money = fields.Float('Money', default=0.0)

class HrPayslipLine(models.Model):
    _inherit = 'hr.payslip.line'

    amount = fields.Float(digits=(12, 2))
    total = fields.Float(compute='_compute_total', string='Total', store=True, digits=(12, 2))




