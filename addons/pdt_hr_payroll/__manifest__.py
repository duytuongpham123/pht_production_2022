# Copyright 2018-2020 Akretion France (http://www.akretion.com)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "PDT HR PAYROLL",
    "version": "14.0.1.0.0",
    "category": "PDT PAYROLL",
    "license": "AGPL-3",
    "summary": "Technical module to generate PDF invoices with " "embedded XML file",
    "author": "Akretion,Onestein,Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://github.com/OCA/edi",
    "depends": ["base", "hr", "hr_contract", "hr_payroll", "pdt_contract_management", "pdt_salary_management"],
    "data": [
        "views/hr_payslip.xml",
        "views/annual_leave_allowance_view.xml",
        "data/hr_salary_rule_pht.xml",
        "data/hr_salary_worker_rule.xml",
        "data/hr_salary_worker_specical_rule.xml",
        "security/ir.model.access.csv",
        ],
    "installable": True,
}
