# -*- coding: utf-8 -*-
{
    'name': "Import Shift Request",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'pdt_shift_management'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'wizard/form_import.xml',
    ],
    'assets': {
        'web.assets_backend': [
            '/import_shift_request/static/src/js/import.js',
        ],
        'web.assets_qweb': [
            '/import_shift_request/static/src/xml/import.xml'
        ],
    },
    # 'qweb': ['static/src/xml/import.xml'],
    # only loaded in demonstration mode
   
}
