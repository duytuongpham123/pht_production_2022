odoo.define('import_shift_request.Import', function (require) {
  "use strict";
  var ListController = require('web.ListController');
  var rpc = require('web.rpc');
  
  ListController.include({
      renderButtons: function ($node) {
          this._super.apply(this, arguments);            
          if (this.modelName == 'shift.request') {
            var your_btn = this.$buttons.find('button.o_list_button_custom_import_shift_request');
            your_btn.on('click', this.proxy('_action_import_shift_request'));
        }
      },
      _action_import_shift_request: function(event){
        var self = this;
        rpc.query({
            model: 'shift.request',
            method: 'action_call_import',
            args: [],
        }).then(function (res) {
            self.do_action(
                {
                    name: ("Import Ca"),
                    type: 'ir.actions.act_window',
                    res_model: 'import.shift.request',
                    view_mode: 'form',
                    view_type: 'form',
                    target: 'new',
                    binding_type: 'action',
                    views: [[false, 'form']]
                },
            )
        });

    },
  });
});