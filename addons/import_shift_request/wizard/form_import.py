# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError

import os
from xlrd import open_workbook
import base64
import openpyxl
from io import BytesIO
from datetime import datetime, timedelta

class ImportShiftRequest(models.TransientModel):
    _name = 'import.shift.request'

    file = fields.Binary("File")


    def mess_notification(self):
        view = self.env.ref('sh_message.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context['message'] = "Bạn đã import thành công"
        return {
            'name': _('Successfull'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'context': context,
            'nodestroy' : True,
        }

    @api.onchange('file')
    def _onchange_file(self):
        if self.file != False:
            return {'file': self.file}

    def button_import(self):
        result = self._onchange_file().get('file')
        if result:
            try:
                wb = openpyxl.load_workbook(filename=BytesIO(base64.b64decode(result)), read_only=True)
            except Exception as e:
                raise ValidationError(e)
            ws = wb.active
            for index, record in enumerate(ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True)):
                listRecord = list(record)
                check = listRecord.copy()
                if check[0]:
                    employee = self.env['hr.employee'].search([('code', '=', check[0])])
                    shift_type = self.env['shift.type'].search([('code', '=', check[1])])
                    from_date = datetime.strptime(str(check[2]), '%Y-%m-%d %H:%M:%S')
                    to_date = datetime.strptime(str(check[3]), '%Y-%m-%d %H:%M:%S')
                    if employee and shift_type:
                        value = {
                            'employee_id': employee.id,
                            'shift_type_id': shift_type.id,
                            'start_date': from_date,
                            'end_date': to_date
                        }
                        shift_request = self.env['shift.request'].create(value)
                        if self.env['shift.request'].search_count([('employee_id', '=', employee.id)]) > 1:
                            get_last_data = self.env['shift.request'].search([('employee_id', '=', employee.id)], order="id desc")[1]
                            check_date = get_last_data.end_date - shift_request.start_date
                            if check_date.days > 0:
                                raise UserError('Ngày kết thúc {} > ngày bắt đầu {} lỗi dòng {}'.format(
                                    get_last_data.end_date, shift_request.start_date, index + 2))
                        if shift_request:
                            if shift_request.shift_type_id.id == self.env.ref('pdt_shift_management.morning_shift').id:
                                date = shift_request.end_date - shift_request.start_date
                                total_date = int(date.days) + 1
                                for i in range(total_date):
                                    get_date = datetime.strptime(str(shift_request.start_date), '%Y-%m-%d') + timedelta(days=i)
                                    value = {
                                        'name': 'Ca {}'.format(i + 1),
                                        'start_date': get_date.date(),
                                        'end_date': get_date.date(),
                                    }
                                    shift_request.shift_request_line_ids = [(0, 0, value)]
                            if shift_request.shift_type_id.id == self.env.ref('pdt_shift_management.evening_shift').id:
                                date = shift_request.end_date - shift_request.start_date
                                if int(date.days) % 2 != 0:
                                    total_date = int(date.days) + 1
                                    for i in range(total_date):
                                        get_start_date = datetime.strptime(str(shift_request.start_date), '%Y-%m-%d') \
                                                         + timedelta(days=i)
                                        get_stop_date = get_start_date + timedelta(days=1)
                                        value = {
                                            'name': 'Ca {}'.format(i + 1),
                                            'start_date': get_start_date.date(),
                                            'end_date': get_stop_date.date(),
                                        }
                                        shift_request.shift_request_line_ids = [(0, 0, value)]
                                else:
                                    raise UserError("Vui lòng chỉnh lại ngày kết thúc trên excel dòng {}".format(index + 2))
                    else:
                        raise UserError("Không có nhân viên trong hệ thống {}".format(index + 2))

