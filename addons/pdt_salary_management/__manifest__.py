# Copyright 2018-2020 Akretion France (http://www.akretion.com)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "PDT Salary Management",
    "version": "14.0.1.0.0",
    "category": "PDT Salary Management",
    "license": "AGPL-3",
    "summary": "PDT Salary Management",
    "author": "Pham Duy Tuong",
    "depends": ["base", "hr", "hr_contract", "hr_payroll", "pdt_contract_management", "hr_work_entry_contract_enterprise"],
    "data": [
        "views/salary_advance.xml",
        "views/salary_contribution.xml",
        "views/salary_donations.xml",
        "views/contribution_type.xml",
        "views/contribution_history.xml",
        "views/other_income.xml",
        "wizard/multi_salary_sign.xml",
        "wizard/change_close.xml",
        "wizard/change_rate.xml",
        "wizard/stop_contribution.xml",
        "wizard/close_contribution.xml",
        "wizard/restart_contribution.xml",
        "security/ir.model.access.csv",
        "security/pdt_security.xml",
        ],
    "installable": True,
}
