from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError


class ChangeClose(models.TransientModel):
    _name = "change.close"

    date = fields.Date('Ngày')
    insurance_salary = fields.Float('Lương bảo hiểm')
    salary_contribution_id = fields.Many2one('salary.contribution')

    def action_confirm(self):
        salary_contribution_id = self.env.context
        salary_contribution = self.env['salary.contribution'].browse(salary_contribution_id['active_id'])
        model_history = self.env['contribution.history'].search([
            ('salary_contribution_id', '=', salary_contribution.id)
        ])
        salary_contribution.update({
            'insurance_salary': self.insurance_salary,
            # 'to_date': self.date
        })
        value = {
            'employee_id': salary_contribution.employee_id.id,
            'status': salary_contribution.state,
            'employee_rate': salary_contribution.employee_rate,
            'company_rate': salary_contribution.company_rate,
            'salary': salary_contribution.insurance_salary,
            'start_date': self.date,
            'salary_contribution_id': salary_contribution.id
        }
        if model_history:
            history_contribution = model_history[-1]
            if history_contribution.start_date <= self.date:
                salary_contribution.contribution_history_id = [(0, 0, value)]
                check = salary_contribution.contribution_history_id
                if len(check) == 2:

                    check[0].update({
                        'end_date': check[-1].start_date - timedelta(days=1),
                        'status': 'close'
                    })
                else:
                    result_date = check[-1].start_date - timedelta(days=1)
                    # last_end_date = check.filtered(lambda x: x.end_date)
                    check[-2].update({
                        'status': 'close'
                    })
                    if check[-1].start_date > check[-2].start_date:
                        check[-2].end_date = result_date
                    if check[-1].start_date == check[-2].start_date:
                        check[-2].end_date = check[-2].start_date
            else:
                raise UserError('Ngày phải  lớn hơn hoặc bằng {}'.format(model_history[-1].start_date))
        else:
            salary_contribution.contribution_history_id = [(0, 0, value)]
