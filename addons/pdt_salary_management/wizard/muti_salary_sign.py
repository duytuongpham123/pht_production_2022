from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError


class MultiSalarySign(models.TransientModel):
    _name = "multi.salary.sign"

    start_date = fields.Date('Ngày bắt đầu')
    contribution_type_id = fields.Many2many('contribution.type')
    contract_ids = fields.Many2many('hr.contract')

    def confirm_sign(self):
        if self.contract_ids:
            models = self.env['salary.contribution']
            for line in self.contract_ids:
                check = models.search([], order='id asc')
                if not check:
                    name = 'CONSAl-0001'
                else:
                    last_num = check[-1]
                    convert = str(int(last_num.name.split('-')[1]) + 1)
                    name = 'CONSAl-{}'.format(convert.zfill(4))
                for line_type in self.contribution_type_id:
                    value = {
                        'name': name,
                        'employee_id': line.employee_id.id,
                        'contract_id':  line.id,
                        'contribution_type_id': line_type.id,
                        'employee_rate': line_type.employee_rate,
                        'company_rate': line_type.company_rate,
                        'num_sign': line_type.code,
                        'to_date': self.start_date,
                    }
                    salary_contribution = models.create(value)
                    if salary_contribution:
                        salary_contribution.state = 'confirmed'
                        val = {
                            'employee_id': salary_contribution.employee_id.id,
                            'status': salary_contribution.state,
                            'employee_rate': salary_contribution.employee_rate,
                            'company_rate': salary_contribution.company_rate,
                            'salary': salary_contribution.insurance_salary,
                            'start_date': salary_contribution.to_date,
                            'salary_contribution_id': salary_contribution.id
                        }
                        salary_contribution.contribution_history_id = [(0, 0, val)]
            action = self.env.ref('pdt_salary_management.view_salary_contribution').read()[0]
            return action