from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError


class SalaryContribution(models.Model):
    _name = "salary.contribution"

    name = fields.Char('Tên', readonly=1)
    employee_id = fields.Many2one('hr.employee', 'Nhân viên', required=True)
    contract_id = fields.Many2one('hr.contract',  'Hợp đồng', required=True)
    payslip_id = fields.Many2one('hr.payslip')
    insurance_salary = fields.Float('Lương đóng bảo hiểm', related='contract_id.salary_paid_insurance', store=True)
    employee_rate = fields.Float('Tỉ lệ nhân viên đóng %')
    company_rate = fields.Float('Tỉ lệ công ty đóng  %')
    contribution_type_id = fields.Many2one('contribution.type', 'Loại đóng góp', required=True)
    num_sign = fields.Char('Số đăng kí')
    to_date = fields.Date('Từ ngày', required=True)
    from_date = fields.Date('Đến ngày')
    contribution_history_id = fields.One2many('contribution.history', 'salary_contribution_id', 'Lịch sử đóng góp')
    state = fields.Selection(
        string='Trạng thái',
        selection=[
            ('draft', 'Dự thảo'),
            ('confirmed', 'Đã xác nhận'),
            ('stop', 'Tạm dừng'),
            ('restore', 'Khôi phục'),
            ('close', 'Đã đóng'),
            ('cancel', 'Đã hủy')]
        )

    @api.model
    def create(self, val):
        model = self.env['salary.contribution']
        check = model.search([], order='id asc')
        if not check:
            val['name'] = 'CONSAl-0001'
        else:
            last_num = check[-1]
            convert = str(int(last_num.name.split('-')[1]) + 1)
            val['name'] = 'CONSAl-{}'.format(convert.zfill(4))
        res = super(SalaryContribution, self).create(val)
        res.state = 'draft'
        current_contribution = model.search([
            ('employee_id', '=', res.employee_id.id),
            ('contract_id', '=', res.contract_id.id),
            ('contribution_type_id', '=', res.contribution_type_id.id)
        ], order='id asc')
        if len(current_contribution.mapped('id')) > 1:
            raise UserError('Bạn đã chọn loại đóng góp {} này ở {}'.format(current_contribution[0].contribution_type_id.code, current_contribution[0].name))
        return res

    @api.onchange('contribution_type_id')
    def _onchange_contribution_type(self):
        if self.contribution_type_id:
            self.employee_rate = self.contribution_type_id.employee_rate
            self.company_rate = self.contribution_type_id.company_rate

    def action_confirm(self):
        if self.state == 'draft':
            self.state = 'confirmed'
            value = {
                'employee_id': self.employee_id.id,
                'status': self.state,
                'employee_rate': self.employee_rate,
                'company_rate': self.company_rate,
                'salary': self.insurance_salary,
                'start_date': self.to_date,
                'salary_contribution_id': self.id
            }
            self.contribution_history_id = [(0, 0, value)]

    def action_change_close(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'change.close',
            'view_mode': 'form',
            'binding_view_types': 'form',
            'target': 'new',
            'context': self.id
        }

    def action_change_rate(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'change.rate',
            'view_mode': 'form',
            'binding_view_types': 'form',
            'target': 'new',
            'context': self.id
        }

    def action_stop(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'stop.contribution',
            'view_mode': 'form',
            'binding_view_types': 'form',
            'target': 'new',
            'context': self.id
        }

    def action_close(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'close.contribution',
            'view_mode': 'form',
            'binding_view_types': 'form',
            'target': 'new',
            'context': self.id
        }

    def action_restart(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'restart.contribution',
            'view_mode': 'form',
            'binding_view_types': 'form',
            'target': 'new',
            'context': self.id
        }

    def action_cancel(self):
        if self.contribution_history_id[-1].start_date <= datetime.date.today():
            self.state = 'cancel'
        else:
            raise UserError('Ngày bằng hoặc lớn hơn {}'.format(self.contribution_history_id[-1].start_date))


    def action_draft(self):
        self.state = 'draft'






