from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError


class ContributionType(models.Model):
    _name = "contribution.type"

    name = fields.Many2one('salary.donations', 'Khoản đóng góp', required=True)
    code = fields.Char('Mã', required=True)
    employee_rate = fields.Float('Tỉ lệ nhân viên đóng %')
    company_rate = fields.Float('Tỉ lệ công ty đóng %')
