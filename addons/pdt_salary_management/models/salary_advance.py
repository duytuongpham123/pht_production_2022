from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError


class SalaryManagement(models.Model):
    _name = "salary.management"

    name = fields.Char('Tên phiếu tạm ứng', readonly=1)
    employee_id = fields.Many2one('hr.employee', 'Nhân viên', required=True)
    responsible = fields.Many2one('hr.employee', 'Chịu trách nhiệm', required=True)
    request_date = fields.Date('Ngày yêu cầu', required=True, readonly=1)
    advance_date = fields.Date('Ngày tạm ứng',  required=True)
    money = fields.Float('Số tiền', required=True)
    reason = fields.Text('Lý do',  required=True)
    state = fields.Selection(
        string='Trạng thái',
        selection=[
            ('draft', 'Dự thảo'),
            ('pending', 'Chờ duyệt'),
            ('approved', 'Đã duyệt'),
            ('refused', 'Đã từ chối'),
            ('advanced', 'Đã tạm ứng')
        ]
    )

    @api.model
    def create(self, val):
        check = self.env['salary.management'].search([], order='id asc')
        if not check:
            val['name'] = 'TUL-0001'
        else:
            last_num = check[-1]
            convert = str(int(last_num.name.split('-')[1]) + 1)
            val['name'] = 'TUL-{}'.format(convert.zfill(4))
        val['state'] = 'draft'
        res = super(SalaryManagement, self).create(val)
        res.request_date = datetime.date.today()
        return res

    def action_request(self):
        if self.state == 'draft':
            self.state = 'pending'

    def action_approved(self):
        if self.state == 'pending':
            self.state = 'approved'

    def action_refused(self):
        if self.state == 'pending':
            self.state = 'refused'

    def action_advanced(self):
        if self.state == 'approved':
            self.state = 'advanced'

