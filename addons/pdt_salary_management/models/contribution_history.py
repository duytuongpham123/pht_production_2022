from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError


class ContributionHistory(models.Model):
    _name = "contribution.history"

    name = fields.Char('Tên', readonly=1)
    employee_id = fields.Many2one('hr.employee', 'Nhân viên')
    status = fields.Char('Ghi nhận đóng góp của nhân viên')
    salary = fields.Float('Mức lương đóng')
    start_date = fields.Date('Ngày bắt đầu')
    end_date = fields.Date('Ngày kết thúc')
    employee_rate = fields.Float('Tỉ lệ nhân viên đóng %')
    company_rate = fields.Float('Tỉ lệ công ty đóng %')
    salary_contribution_id = fields.Many2one('salary.contribution')
