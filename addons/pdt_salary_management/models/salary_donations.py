from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError


class SalaryDonations(models.Model):
    _name = "salary.donations"

    name = fields.Char('Tên', required=True)
    code = fields.Char('Mã')
    note = fields.Char('Ghi chú')
