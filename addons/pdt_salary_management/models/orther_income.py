from odoo import api, models, fields
from dateutil.relativedelta import relativedelta
import datetime
from odoo.exceptions import UserError, ValidationError


class OtherIncome(models.Model):
    _name = "other.income"

    name = fields.Char('Tên', required=True)
    employee_id = fields.Many2one('hr.employee', 'Nhân viên', required=True)
    money = fields.Float('Số tiền')
    description = fields.Text('Mô tả')
    date = fields.Date('Ngày ghi nhận', required=True)
    type_other = fields.Selection(
        string='Loại',
        selection=[
            ('income', 'Thu nhập'),
            ('deduct', 'Khấu trừ')
        ],
        required=True
    )

