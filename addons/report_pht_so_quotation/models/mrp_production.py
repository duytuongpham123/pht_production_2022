from odoo import _, models,fields, api
import os

class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    partner_last = fields.Many2many('res.partner', store=False, compute='get_last_partner')

    @api.depends('procurement_group_id')
    def get_last_partner(self):
        for rec in self:
            if rec.procurement_group_id:
                sale_order = rec.procurement_group_id.mrp_production_ids.move_dest_ids.group_id.sale_id
                arr = []
                for line in sale_order:
                    so = self.env['sale.order'].sudo().search([
                        ('x_po', '=', line.x_po),
                        ('get_company', '!=', 'CÔNG TY TNHH MỘT THÀNH VIÊN BAO BÌ PHƯỚC HIỆP THÀNH')
                    ])
                    for item in so:
                        arr.append(item.partner_id.id)
                rec.partner_last = [(6, 0, arr)]
            else:
                rec.partner_last = [(6, 0, [])]
