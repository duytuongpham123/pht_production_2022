from odoo import _, models,fields, api
import os
import calendar
from datetime import datetime, timedelta



class InheritCustomize(models.Model):
    _inherit = 'stock.picking'

    car_plate = fields.Selection(
        [
            ('Thảo(61C-167.19)', 'Thảo(61C-167.19)'),
            ('Thanh(61C-190.48)', 'Thanh(61C-190.48)'),
            ('Mão(61C-081.66)', 'Mão(61C-081.66)'),
            ('Trình(61L7-0241)', 'Trình(61L7-0241)'),
            ('Tâm(61C-361.72)', 'Tâm(61C-361.72)'),
            ('Phong(47C-17352)', 'Phong(47C-17352)'),
            ('Nhân(61C-310.75)', 'Nhân(61C-310.75)'),
            ('Trung(61C-319.25)', 'Trung(61C-319.25)'),
            ('Tí(61H-031.22)', 'Tí(61H-031.22)'),
            ('Phương(61H-008.11)', 'Phương(61H-008.11)'),
            ('Lâm(61H-013.26)', 'Lâm(61H-013.26)'),
            ('Phước(61H-028.78)', 'Phước(61H-028.78)'),
            ('Tuấn Anh (61C-451.32)', 'Tuấn Anh (61C-451.32)'),
            ('Luân (61C-451.02)', 'Luân (61C-451.02)'),
            ('Xe tới lấy', 'Xe tới lấy'),
        ],  'Số xe', default='Thảo(61C-167.19)', required=True)
    note = fields.Char('Ghi chú')

    car_plate_id = fields.Many2one('stock.car.plate', string="Số xe")
    # abbreviations_name = fields.Char('Tên viết tắt', related='partner_id.abbreviations_name')
    flag_abbreviations = fields.Char(compute='get_flag_abbreviations', store=False)

    def get_name_customer(self):
        for line in self.move_ids_without_package:
            for item in line.product_id.customer_product_line:
                if self.partner_id.id == item.customer.id:
                    return item.code_product_customer

    def get_po_name(self):
        sale = self.env['sale.order'].search([('name', '=', self.origin)])
        if sale:
            return sale.x_po, sale.client_order_ref
        return '', ''

    def get_attribute(self, line):
        if line.product_id.product_template_variant_value_ids.name:
            if '*' in line.product_id.product_template_variant_value_ids.name:
                result = ['']
                att = line.product_id.product_template_variant_value_ids.name.split('*')
                if len(att) < 5:
                    result = att + result * (5 - len(att))
                else:
                    result = att
                return result
            else:
                return [''] * 5
        else:
            return [''] * 5

    @api.depends('partner_id.abbreviations_name')
    def get_flag_abbreviations(self):
        for rec in self:
            cnv_date = rec.scheduled_date.date()
            flag_partner = ''
            if rec.partner_id.parent_id:
                flag_partner = rec.partner_id.parent_id.abbreviations_name
            else:
                flag_partner = rec.partner_id.abbreviations_name
            if rec.partner_id.abbreviations_name or rec.partner_id.parent_id.abbreviations_name:
                current_date = calendar.monthrange(cnv_date.year, cnv_date.month)
                date_from = datetime.strptime('{}-{}-{}'.format(cnv_date.year, cnv_date.month, 1), '%Y-%m-%d')
                date_to = datetime.strptime('{}-{}-{}'.format(cnv_date.year, cnv_date.month, current_date[1]), '%Y-%m-%d')
                stock = self.env['stock.picking'].search([
                    ('scheduled_date', '>=', date_from.date()),
                    ('scheduled_date', '<=', date_to.date())
                ], order='create_date asc')\
                    .filtered(lambda x: x.partner_id.abbreviations_name == flag_partner)
                if stock:
                    if len(stock.ids) == 1:
                        last_item = str(2).zfill(2)
                    else:
                        last_item = stock.ids.index(rec.id) + 1
                    rec.flag_abbreviations = '%s%s%s-%s' % (
                        flag_partner, cnv_date.strftime("%y"), cnv_date.strftime("%m"), str(last_item).zfill(2))
                else:
                    rec.flag_abbreviations = '%s%s%s-%s' % (
                        flag_partner, cnv_date.strftime("%y"), cnv_date.strftime("%m"), str(1).zfill(2))
            else:
                rec.flag_abbreviations = '%s%s%s-%s' % (
                    flag_partner, cnv_date.strftime("%y"), cnv_date.strftime("%m"), str(1).zfill(2))

    def get_number_stock(self):
        if self.scheduled_date:
            cnv_date = self.scheduled_date.date()
            stock = self.search([]).mapped('id')
            sequence = stock.index(self.id)
            return '%s%s%s-%s' % (self.abbreviations_name, cnv_date.strftime("%y"), cnv_date.strftime("%m"), sequence+1)

    def get_name_po(self):
        if self.origin:
            sale = self.env['sale.order'].sudo().search([('name', '=', self.origin)])
            if sale.x_po:
                return sale.x_po
            else:
                return sale.name

    def total_forecast_availability(self, flag):
        for rec in self:
            arr = []
            arr1 = []
            for line in rec.move_ids_without_package:
                arr.append(line.forecast_availability)
                arr1.append(line.quantity_done)
            if flag == 'forecast':
                return sum(arr)
            if flag == 'done':
                return sum(arr1)

    def get_name_partner_thang_long(self):
        for rec in self:
            if rec.partner_id.parent_id:
                return '%s (%s)' % (rec.partner_id.parent_id.name, rec.partner_id.name)
            return '%s' % rec.partner_id.name

    def get_code_product(self, record):
        for rec in self:
            for line2 in record.product_id.customer_product_line:
                if rec.partner_id.id == line2.customer.id:
                    return line2.code_product_customer

class StockMove(models.Model):
    _inherit = 'stock.move'

    note = fields.Char('Ghi chú')

class StockCarPlate(models.Model):
    _name = 'stock.car.plate'

    name = fields.Char('Số xe')


