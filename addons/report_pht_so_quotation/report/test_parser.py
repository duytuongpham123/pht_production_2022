from odoo import api, models, _
from odoo.tools.misc import formatLang, format_date
from datetime import date, datetime, timedelta
import logging
_logger = logging.getLogger(__name__)


class Parser(models.AbstractModel):
    _inherit = 'report.report_aeroo.abstract'
    _name = 'report.report_einv_and_doc_detail1'


    def _set_localcontext(self):
        localcontext = super(Parser, self)._set_localcontext()
        # localcontext.update({
        #     'test12341': self.test12341,
        # })
        return localcontext

    # def test12341(self):
    #     return 'TUowng'


