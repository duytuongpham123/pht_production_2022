# -*- coding: utf-8 -*-
# module template
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'PHT_Report_SO',
    "summary": "Custom report",
    "description": "Tùy chỉnh báo cáo",
    "version": "14.0.0.0.1",
    "category": "",
    "website": "",
    "author": "Tran Nga",
    # "license": "AGPL-3",
    'depends': [
            'base',
            'sale',
            "mrp",
            'report_aeroo'
            ],
    'data': [
            'views/show.xml',
            'views/template_report_pdf_iivi.xml',
            'views/template_report_pdf_th.xml',
            'views/template_report_pdf_wanek.xml',
            'views/sale_order_line.xml',
            'views/delivery_bill.xml',
            'views/delivery_bill_glimet.xml',
            'views/delivery_bill_glory.xml',
            'views/delivery_bill_in_xiang.xml',
            'views/delivery_notes.xml',
            'views/delivery_shing_mark.xml',
            'views/stock_picking_views.xml',
            'views/mrp_production.xml',
            'report/test.xml',
# 'report/test.xml',
            'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
}