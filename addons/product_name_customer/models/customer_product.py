from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class ProductsCustomer(models.Model):
    _name = "product.customer"

    product_id = fields.Many2one('product.product')
    product_tmpl_id = fields.Many2one('product.template')
    customer = fields.Many2one('res.partner', string="Tên khách hàng")
    name = fields.Char('Tên sản phẩm của khách hàng')
    code_product_customer = fields.Char("Mã sản phẩm của khách hàng")

class ProductProductCustomer(models.Model):
    _name = "product.product.customer"

    product_id = fields.Many2one('product.product')
    product_tmpl_id = fields.Many2one('product.template')
    customer = fields.Many2one('res.partner', string="Tên khách hàng")
    name = fields.Char('Tên sản phẩm của khách hàng')
    code_product_customer = fields.Char("Mã sản phẩm của khách hàng")


class ProductProducts(models.Model):
    _inherit = "product.product"

    customer_product_line = fields.One2many('product.product.customer', 'product_id', string="Sản phẩm của khách hàng")

class ProductTemplate(models.Model):
    _inherit = "product.template"

    customer_product_template_line = fields.One2many('product.customer', 'product_tmpl_id', string="Sản phẩm của khách hàng")

    def btn_get_name_code_product(self):
        for pr in self.customer_product_template_line:
            product = self.env['product.product'].browse(pr.product_id.id)
            if product:
                product.customer_product_line = [(0, 0, {
                    'customer': pr.customer.id,
                    'name': pr.name,
                    'code_product_customer': pr.code_product_customer
                })]

    @api.constrains('name')
    def _sql_constraints_name(self):
        for rec in self:
            arr = []
            self = self.sudo()
            self._cr.execute(""" SELECT name FROM product_template """)
            sql_product_template = self.env.cr.fetchall()
            for pr_tl in sql_product_template:
                arr.append(pr_tl[0])
            if self.sudo() and arr.count('{}'.format(rec.name)) > 1:
                raise ValidationError(_("Tên đã bị trùng"))

    # @api.constrains('customer_product_template_line')
    # def _sql_contraints_pr_name_customer(self):
    #     self = self.sudo()
    #     if self.customer_product_template_line:
    #         for pr_tl_name in self.customer_product_template_line:
    #             if self.customer_product_template_line.search_count([('product_id', '=', pr_tl_name.product_id.id), ('customer', '=', pr_tl_name.customer.id)]) > 1:
    #                 raise ValidationError(_('Lỗi trùng tên khách hàng'))
    #             dup_customer_product = self.customer_product_template_line.search_count([('name', '=', pr_tl_name.name)])
    #             if dup_customer_product > 1:
    #                 pr_name = self.customer_product_template_line.search([('name', '=', pr_tl_name.name)]).sudo()
    #                 for chill_name in pr_name:
    #                     raise ValidationError(_('Lỗi trùng tên sản phẩm khách hàng từ {}').format(chill_name[0].product_id.with_context(display_default_code=False).display_name))





