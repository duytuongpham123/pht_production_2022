# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    'name': 'PDT report contract',
    'version': '15.0.0',
    'category': 'Sales',
    'summary': 'PDT',
    "description": """
       Pham Duy Tuong
    """,
    'author': 'PDT',
    "currency": 'EUR',
    'depends': ['base', 'hr', 'hr_contract'],
    'data': [
        'views/menu_report.xml',
        'views/contract_principles.xml',
        'views/probationary_contract.xml',
        # 'security/branch_security.xml',
        # 'security/ir.model.access.csv',
        # 'views/reflect_branch.xml',
        # 'views/visit_branch.xml',
    ],
    'demo': [],
    # 'assets': {
    # },
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
