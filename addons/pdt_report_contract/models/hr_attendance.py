import time

from odoo import api, fields, models, _
import requests, json
import subprocess
import sys
import base64
from datetime import datetime
from odoo.exceptions import UserError

class HrAttendance(models.Model):
    _inherit = 'hr.attendance'

    @api.model
    def action_import_attendance(self):
        arr = []
        out = []
        for i in self.env['list.place'].search([]):
            data = {
                'token': self.env['ir.config_parameter'].sudo().get_param('camera_api.token'),
                'placeID': int(i.place_id),
                'from': int(datetime.strptime('1/10/2021 00:00:00', '%d/%m/%Y %H:%M:%S').timestamp() * 1000),
                'to': int(datetime.today().timestamp() * 1000),
                'devices': '<C21284M140>',
                'type': 0
            }
            list_person = requests.post('https://partner.hanet.ai/person/getCheckinByPlaceIdInTimestamp', data=data)
            data_person = list_person.json()['data']
            if data_person:
                arr = data_person
        if arr:
            for i in arr:
                if i.get('aliasID') not in out:
                    out.append({
                        'personName': i.get('personName'),
                        'aliasID': i.get('aliasID'),
                        'date': i.get('date')

                    })
            out_1 = [dict(t) for t in {tuple(d.items()) for d in out}]
            for i in arr:
                for j in out_1:
                    if i.get('aliasID') == j.get('aliasID') and i.get('date') == j.get('date'):
                        if 'checkinTime' in j:
                            j.update({
                                'checkinTime': str(j.get('checkinTime')) + "/" + str(i.get('checkinTime'))
                            })
                        else:
                            j.update({
                                'checkinTime': i.get('checkinTime')
                            })
            # get arr camera return
            value = {}
            for line in out_1:
                employee_id = self.env['hr.employee'].search([('alias_id', '=', line['aliasID'])])
                convert_arr = line['checkinTime'].split('/')
                if employee_id:
                    for index, child in enumerate(convert_arr):
                        num = index + 1
                        if num % 2 == 0:
                            value['check_out'] = datetime.fromtimestamp(int(child) / 1000)
                        else:
                            value = {
                                'employee_id': employee_id.id,
                                'check_in': datetime.fromtimestamp(int(child) / 1000),
                            }
                        if 'check_out' in value and value:
                            self.env['hr.attendance'].sudo().create(value)
                            value = {}
                        if num == len(convert_arr) and num %2 != 0:
                            self.env['hr.attendance'].sudo().create(value)


