odoo.define('camera_api.hr_employee', function (require) {
  "use strict";
  var ListController = require('web.ListController');
  var rpc = require('web.rpc');

  ListController.include({
      renderButtons: function ($node) {
          this._super.apply(this, arguments);
          if (this.modelName == 'hr.employee') {
            var your_btn = this.$buttons.find('button.o_list_button_custom_import1');
            your_btn.on('click', this.proxy('_action_list_employee'));

            var your_btn_2 = this.$buttons.find('button.o_list_button_custom_import2');
            your_btn_2.on('click', this.proxy('_check_data'));
        }
      },
      _action_list_employee: function(event){
        var self = this;
        rpc.query({
            model: 'hr.employee',
            method: 'refesh_list_employee',
            args: [],
        }).then(function (res) {
            console.log(1);
        });

    },
      _action_create_employee: function(event){
        var self = this;
//        return new Promise(resolve => {
//            fetch('https://jsonplaceholder.typicode.com/todos/1')
//              .then(response => response.json())
//              .then(json => resolve(json))
//          });
//        return new Promise(resolve => {
        rpc.query({
            model: 'hr.employee',
            method: 'create_list_employee',
            args: [],
            }).then(function (res) {
                const arr = [];
                const allPromise = [];

                const createLoadingElelement = () => {
                    var blurBackground = document.createElement('div');
                    blurBackground.classList.add('loading-background');
                    var loadingElement = document.createElement('div');

                    loadingElement.classList.add('lds-ring');
                    blurBackground.appendChild(loadingElement);
                    loadingElement.appendChild(document.createElement('div'))
                    document.body.appendChild(blurBackground);
                }

                const removeLoadingElement = () => {
                    const element = document.querySelector('.loading-background');
                    if (element) element.remove();
                }
                createLoadingElelement();

                $.each(res, function(i, item) {
                    let promiseSubmitForm = new Promise(resolve => {
                        let formData = new FormData()
                        formData.append("token", item.token)
                        formData.append("name", item.name)
                        formData.append("aliasID", item.aliasID)
                        formData.append("title", item.title)
                        formData.append("type", item.type)
                        formData.append("placeID", item.placeID)
                        let base64=item.file;
                        let buffer=Uint8Array.from(atob(base64), c => c.charCodeAt(0));
                        let blob=new Blob([buffer], { type: "image/jpg" });
                        let url=URL.createObjectURL(blob);
                        fetch(url)
                          .then(res => res.blob())
                          .then(blob => {
                            const file = new File([blob], "File name",{ type: "image/jpg" })
                            formData.append('file', file, `${item.name}.jpg`);
                            fetch('https://partner.hanet.ai/person/register', {
                               method: "POST",
                               body: formData,
                            }).then(res=>res.json()).then(respone=> {
                                console.log(respone)
                                if (respone.returnMessage === 'SUCCESS'){
                                    const alias_id = respone.data.aliasID
                                    rpc.query({
                                        model: 'hr.employee',
                                        method: 'update_check_api_camera',
                                        args: [alias_id],
                                    }).then(function (res) {
                                        resolve(true);
                                    });
                                }
                                else{
                                    arr.push({'alias': item.aliasID, 'returnMessage': respone.returnMessage});
                                    resolve(true);
                                }
                            });
                          })
                    })
                    allPromise.push(promiseSubmitForm);
                });
                Promise.all(allPromise).then((values) => {
                  rpc.query({
                        model: 'hr.employee',
                        method: 'create_list_employee1',
                        args: [arr],
                    }).then(function (res) {
                        removeLoadingElement();
                    }).catch((error) => {
                        removeLoadingElement();
                    });
                });


            });
//          });
        },
        _action_create_employee1: function(arr){
            return new Promise(resolve => {
                resolve(arr)
            });
        },
        _check_data:  function(){
            const test =  this._action_create_employee()
//            console.log(1111, test)
//            const test1 =  await this._action_create_employee1(test)
//            console.log(2222, test1)


        },
    });
});