odoo.define('camera_api.hr_attendance', function (require) {
  "use strict";
  var ListController = require('web.ListController');
  var rpc = require('web.rpc');

  ListController.include({
      renderButtons: function ($node) {
          this._super.apply(this, arguments);
          if (this.modelName == 'hr.attendance') {
            var your_btn = this.$buttons.find('button.o_list_button_custom_import_attendance');
            your_btn.on('click', this.proxy('_action_import_attendance'));
        }
      },
      _action_import_attendance: function(event){
        var self = this;
        rpc.query({
            model: 'hr.attendance',
            method: 'action_import_attendance',
            args: [],
        }).then(function (res) {
            console.log(1);
        });

    },
  });
});