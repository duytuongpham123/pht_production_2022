# Copyright 2018-2020 Akretion France (http://www.akretion.com)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "PDT CONTRACT MANAGEMENT",
    "version": "14.0.1.0.0",
    "category": "CONTRACT MANAGEMENT",
    "license": "AGPL-3",
    "summary": "Technical module to generate PDF invoices with " "embedded XML file",
    "author": "Akretion,Onestein,Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://github.com/OCA/edi",
    "depends": ["base", "hr", "hr_contract", "hr_payroll", "sale"],
    "data": [
        "views/contract_management.xml",
        "views/allowance.xml",
        "views/hr_allowance.xml",
        "views/contract_addendum.xml",
        "views/overtime_type.xml",
        "views/overtime.xml",
        "security/ir.model.access.csv",
        "security/pdt_security.xml",
        ],
    "installable": True,
}
