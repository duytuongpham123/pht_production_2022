# Copyright 2018-2020 Akretion France
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, models, fields


class Allowance(models.Model):
    _name = "allowance.all"

    name = fields.Char("Tên phụ cấp")
    code =  fields.Char("Mã phụ cấp")
    money = fields.Float("Tiền")
    
