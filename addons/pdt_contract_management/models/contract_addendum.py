
from odoo import api, models, fields


class ContractAddendum(models.Model):
    _name = "contract.addendum"

    contract_id = fields.Many2one('hr.contract', 'Hợp đồng')
    employee_id = fields.Many2one('hr.employee', 'Nhân viên')
    hr_department_id = fields.Many2one('hr.department', 'Phòng ban')
    company_id = fields.Many2one('res.company', 'Công ty')
    hr_job_id = fields.Many2one('hr.job', 'Chức vụ')
    salary_structure_type = fields.Many2one('hr.payroll.structure.type', 'Cấu trúc lương')
    date_start = fields.Date('Ngày bắt đầu', default=fields.Date.today(), required=True)
    date_first = fields.Date('Ngày bắt đầu', related="date_start")
    date_end = fields.Date('Ngày kết thúc', default=fields.Date.today())
    resource_calendar_id = fields.Many2one('resource.calendar')
    hr_responsible = fields.Many2one('res.users')