
from odoo import api, models, fields
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

class OverTime(models.Model):
    _name = "hr.overtime"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Tăng ca")
    emploeee_id = fields.Many2one("hr.employee","Người yêu cầu")
    code = fields.Char('Mã nhân viên')
    job_title = fields.Char('Chức vụ', related="emploeee_id.job_title", readonly=True)
    department = fields.Many2one("hr.department", "Phòng ban", readonly=True)
    content = fields.Char("Nội dung/ lí do")
    start_date = fields.Date("Ngày tạo", default=fields.Date.today())
    approved_id = fields.Many2one("hr.employee", "Người duyệt")
    file = fields.Binary("File đính kèm")
    states = fields.Selection(
        string='Trạng thái',
        selection=[('draft', 'Mới tạo'), 
            ('pending', 'Chờ duyệt'), 
            ('approved', 'Đã duyệt'), 
            ('cancel', 'Hủy')],
        default='draft'
    )
    flag = fields.Boolean()

    detail_overtime_ids = fields.One2many("hr.detail.overtime", 'hr_overtime_id')

    @api.onchange('emploeee_id')
    def get_data_employee(self):
        if self.emploeee_id:
            self.department = self.emploeee_id.department_id
            self.approved_id = self.emploeee_id.parent_id.id

    @api.model
    def create(self, vals):
        # employee_id = self.env['hr.employee'].search([('code', '=', vals['code'])])
        # for line in employee_id:
        #     if employee_id:
        #         vals['emploeee_id'] = line.id
        #         vals['department'] = line.department_id.id
        #         vals['approved_id'] = line.parent_id.id
        res = super(OverTime, self).create(vals)
        res.department = res.emploeee_id.department_id.id
        res.approved_id = res.emploeee_id.parent_id.id
        return res
    #
    # @api.onchange('emploeee_id')
    # def onchange_employee_id(self):
    #     if self.emploeee_id:
    #         self.code = self.emploeee_id.code

    def check_date(self, date1, date2):
        flag_time = timedelta(hours=7, minutes=0)
        f_date = '%Y-%m-%d %H:%M:%S'
        date_1 = datetime.strptime(str(date1), f_date) - flag_time
        date_2 = datetime.strptime(str(date2), f_date) - flag_time
        return date_1, date_2

    def action_send_request(self):
        if self.states:
            for item in self.detail_overtime_ids:
                leave = self.env['hr.leave'].search([]).filtered(lambda x: x.state == 'validate' and x.holiday_status_id.work_entry_type_id.code == 'LEAVES_HOLIDAY')
                for check in leave:
                    start = self.check_date(check.date_from, check.date_to)[0].date()
                    end = self.check_date(check.date_from, check.date_to)[1].date()
                    if start <= item.date_ot <= end:
                        raise UserError("Lỗi tạo trùng với ngày nghỉ {}".format(check.request_date_from))
            if self.approved_id and self.emploeee_id and self.department:
                self.states = 'pending'
                group = self.env['res.groups'].search([('id', '=', self.env.ref('pdt_contract_management.group_pdt_manager').id)])
                group.write({'users': [(4, self.approved_id.user_id.id)]})
            else:
                raise UserError('Vui lòng chọn đầy đủ thông tin')

    def back_to_draft(self):
        if self.states:
            self.states = 'draft'
    
    def action_approved(self):
        if self.states:
            self.states = 'approved'

    def action_cancel_request(self):
        if self.states:
            self.states = 'cancel'


class DetailOverTime(models.Model):
    _name = "hr.detail.overtime"

    hr_overtime_id = fields.Many2one("hr.overtime")
    employee_id = fields.Many2one('hr.employee', 'Nhân viên')
    date_ot = fields.Date('Ngày làm việc', required=True)
    from_hours = fields.Float("Từ giờ", required=True)
    to_hours = fields.Float("Đến giờ", required=True)
    total_hours = fields.Float("Tổng số giờ", compute='get_total_hours', store=True)
    overtime_type_id = fields.Many2one("hr.overtime.type", "Loại tăng ca", required=True)
    rate_salary = fields.Float("Tỉ lệ hưởng lương", related="overtime_type_id.rate_salary", readonly=True)
    note = fields.Char("Ghi chú")
    flag_from_hour = fields.Char()
    flag_to_hour = fields.Char()

    @api.depends('from_hours', 'to_hours')
    def get_total_hours(self):
        for line in self:
            if line.from_hours and line.to_hours:
                if line.to_hours > line.from_hours:
                    line.total_hours = line.to_hours - line.from_hours
                else:
                    raise UserError('Đến giờ nhỏ hơn từ giờ')

    @api.model
    def create(self, vals):
        if vals['flag_from_hour'] and vals['flag_to_hour']:
            if ':' in vals['flag_from_hour'] and ':' in vals['flag_to_hour']:
                from_h = vals['flag_from_hour'].split(':')
                from_t = vals['flag_to_hour'].split(':')
                if float(from_h[0]) > 23 or float(from_t[0]) > 23:
                    raise UserError('Số giờ nhập sai')
                if float(from_h[1]) > 59 or float(from_t[1]) > 59:
                    raise UserError('Số phút nhập sai')
                vals['from_hours'] = float(from_h[0]) + float(from_h[1]) / 60
                vals['to_hours'] = float(from_t[0]) + float(from_t[1]) / 60
            else:
                raise UserError('Bạn vui lòng nhập đúng định dạng')
        res = super(DetailOverTime, self).create(vals)
        return res

class Hrleave(models.Model):
    _inherit = 'hr.leave'

    def check_date(self, date1, date2):
        flag_time = timedelta(hours=7, minutes=0)
        f_date = '%Y-%m-%d %H:%M:%S'
        date_1 = datetime.strptime(str(date1), f_date) - flag_time
        date_2 = datetime.strptime(str(date2), f_date) - flag_time
        return date_1, date_2

    def action_approve(self):
        detail_ot = self.env['hr.detail.overtime'].search([('employee_id', 'in', self.employee_ids.ids)]).\
            filtered(lambda x: x.hr_overtime_id.states == 'approved')
        for item in detail_ot:
            start = self.check_date(self.date_from, self.date_to)[0].date()
            end = self.check_date(self.date_from, self.date_to)[1].date()
            if start <= item.date_ot <= end:
                raise UserError('Trùng với OT cho ngày lễ : {}'.format(item.date_ot))
        res = super(Hrleave, self).action_approve()
        return res







    
