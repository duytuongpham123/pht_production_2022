from odoo import api, models, fields


class OvertimeType(models.Model):
    _name = "hr.overtime.type"

    name = fields.Char("Tên")
    rate_salary = fields.Float("Tỉ lệ hưởng lương", default=1.0)
    work_entry_type_id = fields.Many2one('hr.work.entry.type', 'Work Entry Type')