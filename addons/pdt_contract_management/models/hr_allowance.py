from odoo import api, models, fields
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta, date


class HrAllowance(models.Model):
    _name = "hr.allowance"

    employee_id = fields.Many2one('hr.employee', 'Nhân viên')
    allowance_id = fields.Many2one('allowance.all', 'Phụ cấp')
    name = fields.Char(related='allowance_id.name', store=False)
    contract_id = fields.Many2one('hr.contract', 'Hợp đồng')
    payslip_id = fields.Many2one('hr.payslip')
    money = fields.Float('Tiền')
    from_date = fields.Date('Từ ngày', default=fields.Date.today(),
                            required=True)
    to_date = fields.Date('Đến ngày', default=fields.Date.today(),
                          required=True)
    type_salary = fields.Selection(
        string='Cách tính lương',
        selection=[('fixed_monthly', 'Cố định hàng tháng'),
                   ('actual_working_day', 'Theo ngày công thức tính')]
    )

    @api.constrains('employee_id', 'contract_id', 'allowance_id', 'from_date',
                    'to_date')
    def constrains_date_allowance(self):
        if self.contract_id and self.employee_id:
            allowance = self.search([
                ('employee_id', '=', self.employee_id.id),
                ('contract_id', '=', self.contract_id.id),
                ('allowance_id', '=', self.allowance_id.id)
            ])
            # get current year
            currentDateTime = datetime.now()
            date = currentDateTime.date()
            arr = []
            for line in allowance:
                format_date = '%Y-%m-%d'
                time = datetime.strptime('{}'.format(line.from_date),
                                         format_date)
                if time.year == date.year:
                    arr.append(line.id)
            if len(arr) > 1:
                raise UserError(
                    'Bạn đã tạo phụ cấp đó trong 1 khoảng thời gian')
