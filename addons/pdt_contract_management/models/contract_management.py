# Copyright 2018-2020 Akretion France
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, models, fields
from odoo.exceptions import UserError, ValidationError

class HrContract(models.Model):
    _inherit = "hr.contract"

    salary_paid_insurance = fields.Float("Lương đóng bảo hiểm")
    payment_type = fields.Selection(
        string='Phương thức thanh toán',
        selection=[('bank', 'Ngân hàng'), ('money', 'Tiền mặt')]
    )

    hr_allowance_ids = fields.One2many('hr.allowance', 'contract_id', "Tất cả phụ cấp của nhân viên")
    salary_13 = fields.Boolean('Pay for 13th month salary')

    @api.constrains('wage')
    def constrains_wage(self):
        if self.wage == 0:
            raise UserError('Bạn vui lòng nhập mức lương tháng cho nhân viên')





    
